var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var swig = require('swig');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var Sequelize = require('sequelize');
var sequelizeMiddleware = require('sequelize-middleware');
var SequelizeStore = require('connect-session-sequelize')(session.Store);
var app = express();
 
// debug mode
//app.use(logger('dev'));


// sequelize Middleware middleware
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/config/config')[env];
app.use(sequelizeMiddleware(config.database, config.username, config.password, config, path.join(__dirname, 'models')));


// initalize sequelizeMiddleware with session store
var sequelize = new Sequelize(config.database, config.username, config.password, config);
var sequelizeStore = new SequelizeStore({
    db: sequelize
});
app.use(cookieParser());
app.use(session({
    secret: 'googleaccounts',
    store: sequelizeStore
    //, cookie: {
    //    maxAge: 14400000
    //}
    //, proxy: true // if you do SSL outside of node.
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('view cache', false);
swig.setDefaults({cache: false});
require('./lib/swig/filters')(swig);

app.use(favicon(path.join(__dirname, 'public/img/', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// Routes
require('./controller/defaultRouterLoader')(app);
require('./controller/apiRouterLoader')(app);
require('./controller/adminRouterLoader')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// error handlers
// development error handler
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
        console.error(err, req.originalUrl);
    });
}

// production error handler
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
