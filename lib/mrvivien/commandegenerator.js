var Promise = require("bluebird");
var API = require('../api/engine'),
    engine = new API();
var VARIABLES = require('../../config/variables');
var Error = require('../../lib/error/error'),
    error = new Error();
var helper = require('../../lib/utils/common');
var _ = require('underscore')

var Commande = function (models, MV, sid) {
        var Party;

        // Modal to Commande Presentation Mapping
        var mapping_commande = {
            Theme: [
                {a: "title", b: " Vous avez choisi le Theme : "},
                {a: "name", b: "  "},
                {a: "description", b: ""}
            ],
            Drink: [
                {a: "name", b: " : "},
                {a: "description", b: "   "}
            ],
            Ice: [
                {a: "description", b: "  "}
            ],
            Service: [
                {a: "description", b: " "}
            ],
            ADDITIONAL: [
                {a: "description", b: " "}
            ],
            FORMULE: [
                {a: "name", b: " "}
            ],
            FORMULEITEM: [
                {a: "description", b: " "}
            ]
        };

        var setOfCommandes = {};
        var listOfHighLevelCommands = [];
        var toBeLoaded = [];

        // ----------------
        // helper functions
        var _actionsAfterLoad = {};
        // adding an object to actionsBeforeLoad
        var _addAfterLoadAction = function (model, target, value) {
            _actionsAfterLoad[toBeLoaded.length - 1] = {model: model, target: target, value: value};
        }
        // after load handler
        var _AfterLoad = function (r, next) {
            var processingQueue = [];
            var processToListMap = {};
            r.forEach(function (o, index) {
                var action = _actionsAfterLoad[index];
                if (action != undefined) {
                    var whereClause = {where: {}};
                    whereClause.where[action.target] = o[action.value];
                    processToListMap[processingQueue.length] = index;
                    processingQueue.push(models[action.model].findOne(whereClause));
                }
            });

            Promise.all(processingQueue).then(function (rProcessed) {
                rProcessed.forEach(function (o, index) {
                    r[processToListMap[index]] = o;
                });

                // callback
                next(r);
            });
        }

        // load all in toBeLoaded
        var _resolve_all_toBeLoaded = function (list, next) {
            Promise.all(toBeLoaded).then(function (r) {
                // send to Processing
                _AfterLoad(r, function (rProcessed) {
                    var k = 0, price;
                    // Fetching Prices
                    var pricesToBeLoaded = [], pricesToObjectMap = {};
                    rProcessed.forEach(function (o, i) {
                        if (helper.isObject(o)) {
                            if (o.PriceId != undefined) {
                                pricesToObjectMap[k] = pricesToBeLoaded.length;
                                pricesToBeLoaded.push(
                                    models.Price.findOne({where: {id: o.PriceId}})
                                );
                            }
                            k++;
                        } else if (helper.isArray(o)) {
                            o.forEach(function (a, j) {
                                if (a.PriceId != undefined) {
                                    pricesToObjectMap[k] = pricesToBeLoaded.length;
                                    pricesToBeLoaded.push(
                                        models.Price.findOne({where: {id: a.PriceId}})
                                    );
                                }
                                k++;
                            });
                        }
                    });

                    // Building Commands
                    Promise.all(pricesToBeLoaded).then(function (prices) {
                        var i = 0, o;
                        while (rProcessed.length > 0) {
                            o = rProcessed.shift();
                            if (helper.isObject(o)) {
                                if (pricesToObjectMap[i] != undefined) {
                                    price = prices[pricesToObjectMap[i]].price;
                                } else
                                    price = null;
                                list.push({o: o, p: price});
                                i++;
                            } else if (helper.isArray(o)) {
                                o.forEach(function (a) {
                                    if (pricesToObjectMap[i] != undefined) {
                                        price = prices[pricesToObjectMap[i]].price;
                                    } else
                                        price = null;
                                    list.push({o: a, p: price});
                                    i++;
                                });
                            }
                        }
                        // callback
                        next();
                    });
                });
            });
        }

        // appending command to final set of commands

        var _format_command_item = function (m, o, p, a, op) {

            if (mapping_commande[m]) {
                var text = "";
                mapping_commande[m].forEach(function (attr) {
                    if (attr.a == 'title')
                        text += attr.b

                    if (o[attr.a] != undefined)
                        text += o[attr.a] + attr.b

                    text += " ";
                });

                return {d: text, p: p, a: a, o: op};
            }
        }

        var _remove_given_indexes_from_list = function (list, indexes) {
            var newList = [];
            list.forEach(function (o, i) {
                if (indexes.indexOf(i) == -1) {
                    newList.push(o);
                }
            });
            return newList;
        }

        // -----------------
        var calculate_total_price = function (next) {
            var total = 0;

            // Theme
            if (setOfCommandes.Theme) {
                total += setOfCommandes.Theme.p;
            }
            // Drinks
            if (setOfCommandes.Drinks) {
                Object.keys(setOfCommandes.Drinks).forEach(function (key) {
                    if (setOfCommandes.Drinks[key].p) {
                        total += setOfCommandes.Drinks[key].p;
                    }
                    //console.log("Drink", total, setOfCommandes.Drinks[key]);
                });
            }
            // Services
            if (setOfCommandes.Services) {
                Object.keys(setOfCommandes.Services).forEach(function (typeService) {
                    setOfCommandes.Services[typeService].forEach(function (Service) {
                        if (Service.p) {
                            total += Service.p;
                        }
                        //console.log("Service", total, Service, Service.p);
                    });
                });
            }
            // Additional
            if (setOfCommandes.Additional) {
                Object.keys(setOfCommandes.Additional).forEach(function (typeAdditional) {
                    setOfCommandes.Additional[typeAdditional].forEach(function (Additional) {
                        if (Additional.p) {
                            total += Additional.p;
                        }
                        //console.log("Additional", total, Additional.p);
                    });
                });
            }
            setOfCommandes["Total"] = total;
            next();
        }

        var get_additional_options = function (next) {
            toBeLoaded = [];
            var indexesToRemove = [];
            var Categories = {};

            if (Party.Event && Party.Event.ifBio) {
                toBeLoaded.push(VARIABLES.get("Bio"));
            }

            _resolve_all_toBeLoaded(listOfHighLevelCommands, function () {

                listOfHighLevelCommands.forEach(function (o, index) {
                    indexesToRemove.push(index);

                    if (Categories[o.o.type] == undefined) {
                        Categories[o.o.type] = [
                            _format_command_item("ADDITIONAL", o.o, o.p, "")
                        ];

                    } else {
                        Categories[o.o.type].push(
                            _format_command_item("ADDITIONAL", o.o, o.p, "")
                        );
                    }
                });
                // refresh listOfHighLevelCommands
                listOfHighLevelCommands = _remove_given_indexes_from_list(listOfHighLevelCommands, indexesToRemove);
                setOfCommandes['Additional'] = Categories;
                next();
            })

        }

        var expand_services = function (next) {
            toBeLoaded = [];
            _actionsAfterLoad = [];
            var servicesPriceList = [], servicesOptionsMapping = {}, indexesToRemove = [];
            listOfHighLevelCommands.forEach(function (o, index) {
                if (o.o.toString() == "[object SequelizeInstance:Service]") {
                    indexesToRemove.push(index);

                    var Service = o.o;
                    var type = o.o.type;

                    // fetching services
                    if (Service.status == true) {
                        // TODO map service id To loaded service variables
                        if (Service.type == "fontaine" || Service.type == "bar" || Service.type == "verre") {
                            // TODO load drink that goes with fontaine
                            if (Service.type == "fontaine") {
                                servicesOptionsMapping[toBeLoaded.length] = index;
                                toBeLoaded.push(VARIABLES.get("service_fontaine", Service.meta));
                            } else if (Service.type == "bar") {
                                servicesOptionsMapping[toBeLoaded.length] = index;
                                toBeLoaded.push(VARIABLES.get("service_bar", Service.meta));
                            } else if (Service.type == "verre") {
                                servicesOptionsMapping[toBeLoaded.length] = index;
                                toBeLoaded.push(VARIABLES.get("service_glass", Service.meta));
                            }

                        } else if (Service.type == "animation") {
                            servicesOptionsMapping[toBeLoaded.length] = index;
                            toBeLoaded.push(VARIABLES.get("animation", Service.meta));

                        } else if (Service.type == "service-bar") {
                            // le bar

                            if (Service.meta == "texture_au_choix") {
                                servicesOptionsMapping[toBeLoaded.length] = index;
                                toBeLoaded.push(VARIABLES.get("texture_au_choix", Service.body.id));
                            } else if (Service.meta == "revetement_perso") {
                                servicesOptionsMapping[toBeLoaded.length] = index;
                                toBeLoaded.push(VARIABLES.get("revetement_texture", Service.body.texture.id));
                                servicesOptionsMapping[toBeLoaded.length] = index;
                                toBeLoaded.push(VARIABLES.get("revetement_coleur", Service.body.color.id));

                            } else if (Service.meta == "extension_bar_1") {
                                servicesOptionsMapping[toBeLoaded.length] = index;
                                toBeLoaded.push(VARIABLES.get("extension_bar_1"));

                            } else if (Service.meta == "extension_bar_2") {
                                servicesOptionsMapping[toBeLoaded.length] = index;
                                toBeLoaded.push(VARIABLES.get("extension_bar_2"));

                            } else if (Service.meta == "deux_bar") {
                                servicesOptionsMapping[toBeLoaded.length] = index;
                                toBeLoaded.push(VARIABLES.get("deux_bar"));

                            } else {
                                error.STDError([__dirname, __filename].join('/'), "expand_services",
                                    "unkown Service service-bar option", null);
                            }

                        } else if (Service.meta == "no_lift") {
                            servicesOptionsMapping[toBeLoaded.length] = index;
                            toBeLoaded.push(VARIABLES.get("no_lift"));

                        } else {
                            error.STDError([__dirname, __filename].join('/'), "expand_services", "unkown type", null);
                        }
                    }
                }
            });

            _resolve_all_toBeLoaded(servicesPriceList, function () {
                    var Categories = {};

                    var calculate_price_services = function (o, p, index) {
                        var total;
                        var count = 1, hours = 1;
                        var options = listOfHighLevelCommands[servicesOptionsMapping[index]].o.body;

                        var get_count_or_hour = function (o, k) {
                            if (k == "count")
                                count = parseInt(o[k]);
                            if (k == "hours")
                                hours = parseInt(o[k]);
                        }

                        if (helper.isObject(options)) {
                            // level 1 search for count or hours
                            Object.keys(options).forEach(function (key) {

                                if (helper.isObject(options[key])) {
                                    get_count_or_hour(options, key);
                                    // level 2 search
                                    Object.keys(options[key]).forEach(function (key2) {
                                        get_count_or_hour(options[key], key2);
                                    });
                                }
                            });
                        }

                        var a;
                        if (count > hours)
                            a = count;
                        else
                            a = hours;

                        total = p * count * hours;
                        return {o: o, p: total, a: a};
                    }

                    // seperating services by categories
                    servicesPriceList.forEach(function (o, index) {
                        var newO = calculate_price_services(o.o, o.p, index);
                        if (Categories[o.o.type] == undefined) {
                            Categories[o.o.type] = [];
                        }
                        var item = listOfHighLevelCommands[servicesOptionsMapping[index]].o;

                        Categories[o.o.type].push(
                            _format_command_item("Service", newO.o, newO.p, newO.a, {
                                id: newO.o.type,
                                serviceid: item.id,
                                type: "[object SequelizeInstance:Service]",
                                body: JSON.stringify(item.body)
                            })
                        );
                    });

                    // refresh listOfHighLevelCommands
                    listOfHighLevelCommands = _remove_given_indexes_from_list(listOfHighLevelCommands, indexesToRemove);
                    // add to global set of commandes
                    setOfCommandes["Services"] = Categories;

                    // callback
                    next();
                }
            );

        }

        var expand_drinks = function (next) {
            var Categories = {}, indexesToRemove = [];

            // seperating drinks by categories
            listOfHighLevelCommands.forEach(function (o, index) {
                if (o.o.toString() == "[object SequelizeInstance:Drink]") {
                    indexesToRemove.push(index);
                    // seperating into categories
                    if (Categories[o.o.BarDrink.category] == undefined) {
                        Categories[o.o.BarDrink.category] = {
                            name: null,
                            drinks: [{o: o.o, p: o.p}],
                            price: o.p * o.o.BarDrink.quantity * Party.Event.nbPeople  // price of drink * number of drinks per person * number of people
                        };
                    } else {
                        Categories[o.o.BarDrink.category].drinks.push({o: o.o, p: o.p});
                        Categories[o.o.BarDrink.category].price += o.p * o.o.BarDrink.quantity * Party.Event.nbPeople;
                    }
                }
            });


            // refresh listOfHighLevelCommands
            listOfHighLevelCommands = _remove_given_indexes_from_list(listOfHighLevelCommands, indexesToRemove);

            // fetching category name
            var categoriesToBeFetched = [];
            Object.keys(Categories).forEach(function (key) {
                categoriesToBeFetched.push(models.Category.findOne({where: {id: key}}));
            })
            Promise.all(categoriesToBeFetched).then(function (categoriesList) {
                categoriesList.forEach(function (category) {
                    Categories[category.id].name = category.name;
                });

                // calculating ice
                VARIABLES.get('ice').then(function (OptionIce) {
                    var totalpriceIce = 0;
                    Object.keys(Categories).forEach(function (key) {
                        var IceSelected = [];
                        if (Categories[key].drinks.length > 0) {
                            var Drink = Categories[key].drinks[0].o;
                            var ice = JSON.parse(Drink.BarDrink.ice);
                            if (ice != undefined) {
                                if (ice.optional != undefined) {
                                    ice.optional.forEach(function (option) {
                                        if (OptionIce[option.id].Price) {
                                            var priceIce = OptionIce[option.id].Price.price;
                                            totalpriceIce += priceIce * option.count;
                                            IceSelected.push({
                                                o: OptionIce[option.id],
                                                p: priceIce,
                                                count: option.count,
                                                id: option.id
                                            });
                                        }
                                    });
                                }

                                Categories[key].ice = {
                                    name: ice.type,
                                    price: totalpriceIce,
                                    options: IceSelected
                                };
                            }
                        }
                    });

                    // upgrade glasses
                    Object.keys(Categories).forEach(function (key) {
                        if (Categories[key].drinks.length > 0) {
                            var Drink = Categories[key].drinks[0].o;
                            if(Drink.BarDrink.upgrade){
                                // TODO add price for upgrade
                            }

                            Categories[key].upgrade = Drink.BarDrink.upgrade
                        }
                    });


                    // finally formatting drinks
                    var Drinks = [];
                    Object.keys(Categories).forEach(function (key) {
                        var formatted_drinks = [], formatted_ice = [];
                        Categories[key].drinks.forEach(function (drink) {
                            formatted_drinks.push(_format_command_item("Drink", drink.o, drink.p,
                                drink.o.BarDrink.quantity + " x " + Party.Event.nbPeople, {
                                    id: drink.o.id,
                                    type: drink.o.toString()
                                }));
                        });

                        if (Categories[key].ice != undefined) {
                            Categories[key].ice.options.forEach(function (optionice) {
                                formatted_ice.push(_format_command_item("Ice", optionice.o, optionice.p, optionice.count,
                                    {id: optionice.id, description: optionice.o.description, category: key, type: "ICE"}));
                            });
                        } else {
                            formatted_ice = null
                        }

                        var p;
                        if (Categories[key].ice) {
                            p = (Categories[key].price + Categories[key].ice.price)
                        } else {
                            p = Categories[key].price
                        }

                        Drinks.push({
                            d: Categories[key].name,
                            p: p,
                            drinks: formatted_drinks,
                            ice: formatted_ice,
                            upgrade: Categories[key].upgrade
                        });
                    });

                    setOfCommandes["Drinks"] = Drinks;
                    //callback
                    next();
                });
            });
        }

        var expand_theme = function (next) {
            var indexesToRemove = [];
            listOfHighLevelCommands.forEach(function (o, index) {
                if (o.o.toString() == "[object SequelizeInstance:Theme]") {
                    indexesToRemove.push(index);
                    setOfCommandes["Theme"] = _format_command_item("Theme", o.o, o.p, "", {
                        id: o.o.id,
                        type: o.o.toString()
                    });
                }
            });
            // refresh listOfHighLevelCommands
            listOfHighLevelCommands = _remove_given_indexes_from_list(listOfHighLevelCommands, indexesToRemove);

            next();
        }

        var get_lower_level = function (next) {
            expand_theme(function () {
                expand_drinks(function () {
                    expand_services(function () {
                        next();
                    })
                })
            });
        }

        var get_high_level = function (next) {

            // ----------------
            // processing commande
            if (Party.ifFormule) {
                // formule
                setOfCommandes.ifFormule = true;
                Party.getFormule().then(function (formule) {
                    var itemList = []
                    formule.getItems({include: [models.Price]})
                        .then(function (items) {
                            var totalPrice = 0
                            items.forEach(function (item) {
                                itemList.push(
                                    _format_command_item("FORMULEITEM", item, item.Price.price, "")
                                )
                                if (!_.isUndefined(item.Price))
                                    totalPrice += item.Price.price
                            })

                            setOfCommandes.Formule = _format_command_item("FORMULE", formule, "", "")
                            setOfCommandes.FormuleItems = itemList
                            setOfCommandes.FormuleNbPpl = formule.nbPpl
                            setOfCommandes.Total = totalPrice

                            // callback
                            next()
                        })
                })

            } else {
                // sur measure
                // -------------
                setOfCommandes.ifFormule = false;

                // theme
                if (Party.Event && Party.Event.ThemeId) {
                    toBeLoaded.push(
                        models.Theme.findOne({where: {id: Party.Event.ThemeId}})
                    );
                    _addAfterLoadAction("Theme", "id", "ThemeId");
                }

                // drinks
                if (Party.Bar) {
                    toBeLoaded.push(Party.Bar.getDrinks());
                }

                // services
                toBeLoaded.push(Party.getServices());

                _resolve_all_toBeLoaded(listOfHighLevelCommands, function () {
                    // callback
                    next();
                });
            }
        }

        // creating commande
        this.getMacommande = function (next) {
            MV.getUserParty(models, sid, function (err, p) {
                Party = p;

                get_high_level(function () {

                    if (setOfCommandes.ifFormule) {
                        next(null, setOfCommandes)
                    } else {
                        // loading higher level objects
                        get_lower_level(function () {
                            get_additional_options(function () {
                                calculate_total_price(function () {
                                    // callback
                                    next(null, setOfCommandes);
                                })
                            })
                        })
                    }
                });
            })
        };

    }
    ;

module.exports = Commande;

