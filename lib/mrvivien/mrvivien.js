var Promise = require("bluebird");
var API = require('../api/engine'),
    engine = new API();
var VARIABLES = require('../../config/variables');
var Commande = require('./commandegenerator');

var MrVivien = function () {

    //---
    // user related

    this.getUserParty = function (models, sid, next) {
        models.Party.findAll({
            where: {
                'sid': sid,
                'done': false
            },
            include: [models.Event, models.Bar],
            order: [
                ['createdAt', 'DESC']
            ]
        })
            .then(function (r) {
                if (r.length == 0) {
                    var newParty = models.Party.build({sid: sid});
                    newParty.save()
                        .then(function (party) {
                            next(null, party);
                        });
                } else {
                    next(null, r[0]);
                }
            });
    };


    this.getAlcoholByCategory = function (models, category, next) {
        engine.findAllCustom(models, models.DrinkCategory, {CategoryId: category}, function (err, r) {
            var drinksToBeFetched = [];
            r.r.forEach(function (DrinkCategory) {
                drinksToBeFetched.push(
                    models.Drink.findOne({where: {id: DrinkCategory.DrinkId}})
                );
            });

            Promise.all(drinksToBeFetched).then(function (drinks) {
                var alcoholsToBeFetched = [];
                drinks.forEach(function (drink) {
                    alcoholsToBeFetched.push(
                        drink.getAlcohol()
                    );
                });

                Promise.all(alcoholsToBeFetched)
                    .then(function (alcoholLists) {
                        var alcohols = [], alcoholSeen = [];
                        alcoholLists.forEach(function (alcoholList) {
                            alcoholList.forEach(function (alcohol) {
                                if (alcoholSeen.indexOf(alcohol.id) == -1) {
                                    alcohols.push(alcohol);
                                    alcoholSeen.push(alcohol.id);
                                }
                            });
                        });

                        var pricesToBeFetched = [];
                        alcohols.forEach(function (alcohol) {
                            pricesToBeFetched.push(
                                alcohol.getPrice()
                            );
                        });

                        Promise.all(pricesToBeFetched)
                            .then(function (prices) {
                                for (var i = 0; i < prices.length; i++) alcohols[i].dataValues.Price = prices[i];
                                next(null, alcohols);
                            });

                    });
            });

        });
    };

    this.getAlcoholAndCategoryForDrink = function (models, drinkid, next) {
        models.Drink.findOne({where: {id: parseInt(drinkid)}})
            .then(function (Drink) {
                Drink.getCategories()
                    .then(function (categories) {
                        Drink.getAlcohol().then(function (alcohols) {
                            next(null, {alcohols: alcohols, categories: categories})
                        })
                    })
            })
    }

    this.getDrinksByCategoryAndAlcohol = function (models, category, alcoholsAllowed, next) {
        var listOfDrinks = [];
        engine.findOne(models.Category, 'id', category, function (err, Category) {
            // get all drinks from category
            Category.getDrinks().then(function (drinks) {
                // get alcohols in drinks
                var alcoholsToBeFetched = [];
                drinks.forEach(function (drink) {
                    alcoholsToBeFetched.push(
                        models.AlcoholInDrink.findAll({
                            where: {
                                DrinkId: drink.id
                            }
                        })
                    );
                });
                // if allowed alchols defined
                if (alcoholsAllowed) {
                    Promise.all(alcoholsToBeFetched).then(function (alcoholsInDrink) {
                        for (var i = 0; i < drinks.length; i++) {
                            var alcohols = alcoholsInDrink[i];

                            for (var j = 0; j < alcohols.length; j++) {
                                var alcohol = alcohols[j];
                                // check if alcohols in drink is allowed
                                if (alcoholsAllowed.indexOf(alcohol.AlcoholId.toString()) != -1) {
                                    listOfDrinks.push(drinks[i]);
                                    break;
                                }
                            }
                        }
                        next(null, listOfDrinks);
                    });
                } else {
                    // if no allowed alcohols defined, add all drinks
                    next(null, drinks);
                }
            })
        })
    }

    this.getDrinksByCategory = function (models, categoryid, next) {
        engine.findOne(models.Category, 'id', categoryid, function (err, Category) {
            Category.getDrinks().then(function (drinks) {
                next(null, {drinks: drinks})
            })
        })
    }

    this.getItemsForFormule = function (models, formuleid, next) {
        engine.findOne(models.Formule, 'id', formuleid, function (err, Formule) {
            Formule.getItems().then(function (items) {
                var pricesToFetch = []
                items.forEach(function (Item) {
                    pricesToFetch.push(models.Price.findOne({where: {id: Item.PriceId}}))
                })
                Promise.all(pricesToFetch)
                    .then(function (prices) {
                        var totalPrice = {price: 0}
                        prices.forEach(function (Price) {
                            totalPrice.price += Price.price
                        })
                        next(null, {items: items, price: totalPrice, prices: prices})
                    })
            })
        })
    }
};

module.exports = MrVivien;