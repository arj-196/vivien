var Promise = require("bluebird");
var API = require('../api/engine'),
    engine = new API();
var VARIABLES = require('../../config/variables');
var Error = require('../../lib/error/error'),
    error = new Error();
var helper = require('../../lib/utils/common');
var _ = require('underscore')

var CommandeManager = function (models, MV, sid) {

    this.removeItem = function (d, next) {
        MV.getUserParty(models, sid, function (err, Party) {

            if (d.type == "[object SequelizeInstance:Theme]") {
                Party.Event.setTheme(null)
                    .then(function () {
                        next({s: true, r: d})
                    })
            }
            else if (d.type == "[object SequelizeInstance:Drink]") {
                models.Drink.find({where: {id: parseInt(d.id)}})
                    .then(function (Drink) {
                        Party.Bar.removeDrink(Drink)
                            .then(function () {
                                next({s: true, r: d})
                            })
                    })
            }
            else if (d.type == "ICE") {
                var id = parseInt(d.id), categoryId = parseInt(d.category)
                Party.Bar.getDrinks()
                    .then(function (Drinks) {
                        Drinks.forEach(function (Drink) {
                            if (Drink.BarDrink.category == categoryId) {
                                var ice = JSON.parse(Drink.BarDrink.ice)
                                var newIceOptions = []
                                ice.optional.forEach(function (option) {
                                    if (option.id != id) {
                                        newIceOptions.push(option)
                                    }
                                })
                                // update ice.optional
                                ice.optional = newIceOptions
                                Drink.BarDrink.update({
                                    ice: JSON.stringify(ice)
                                }).then(function () {
                                    next({s: true, r: d})
                                })
                            }
                        })
                    })
            }

            else if (d.type == "[object SequelizeInstance:Service]") {
                var serviceid = parseInt(d.serviceid)
                Party.getServices()
                    .then(function (services) {
                        var selectedService = null
                        services.forEach(function (Service) {
                            if (Service.id == serviceid) {
                                selectedService = Service
                            }
                        })

                        Party.removeService(selectedService)
                            .then(function () {
                                next({s: true, r: d})
                            })
                    })

            } else {
                next({s: false, r: d})
            }

        })

    }

    this.getAdditionalServiceInformation = function (d, next) {
        var itemsToBeLoaded = []
        if (d.id == "service_bar" || d.id == "service_glass" || d.id == "service_fontaine") {
            if (_.isArray(d.body)) {
                d.body.forEach(function (id) {
                    itemsToBeLoaded.push(models.Drink.find({where: {id: parseInt(id)}}))
                })
            }
        } else if (d.id == "texture_au_choix") {
            itemsToBeLoaded.push(VARIABLES.get("texture_au_choix", d.body.id))

        } else if (!_.isUndefined(d.body.options) && !_.isUndefined(d.body.options.drinkId)) {
            itemsToBeLoaded.push(
                models.Drink.find({where: {id: parseInt(d.body.options.drinkId)}})
            )
        }

        Promise.all(itemsToBeLoaded)
            .then(function (items) {
                var drinkNames = ""
                for (var i = 0; i < items.length; i++) {
                    var Item = items[i]
                    drinkNames += Item.name
                    if (i != items.length - 1) {
                        drinkNames += Item.name + ","
                    }
                    drinkNames += " "
                }
                next({o: d, drinks: items, id: d.id, names: drinkNames})
            })
    }

    this.getAdditionalServiceInformationAsync = function (d) {
        var self = this
        return new Promise(function (resolve, reject) {
            self.getAdditionalServiceInformation(d, function (r) {
                resolve(r)
            })
        })
    }
};

function formatServiceTypeName(name) {
    var p = new RegExp("_", "g")
    return name.replace(p, "-")
    //return name
}

module.exports = CommandeManager;

