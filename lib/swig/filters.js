var Error = require('../error/error'),
    error = new Error();
var routes = require('../../config/routes.js');


var getRoute = function(route) {
    var r = routes[route];
    if(r)
        return r;
    else {
        error.STDError([__dirname, __filename ].join('/'), "getRoute", "unkown route : "  + route);
        return '/undefined';
    }
};


var formattedPrice = function (val) {
    var res = "";
    var arr = val.toString().split(".");
    if (arr.length === 1) { res = val.toString() + ",00"; }
    else
    {
        if (arr[1].length === 1) { res = val.toString() + "0"; }
        else { res = arr[0] + "," + arr[1].substr(0, 2); }
    }
    return res;
}

var keys = function(o){
    return Object.keys(o)
}

module.exports = function (swig) {

    swig.setFilter('route', getRoute);
    swig.setFilter('formatprice', formattedPrice);
    swig.setFilter('keys', keys);

};