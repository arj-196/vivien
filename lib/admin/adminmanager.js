module.exports = {

    editDrink: function (models, data, next) {
        models.Drink.findOne({where: {id: data.id}})
            .then(function (Drink) {

                // drink prop
                Drink.update({
                        name: data.name,
                        description: data.description,
                        saveur: data.saveur,
                        updatedAt: new Date()
                    })
                    .then(function () {
                        // price
                        models.Price.findOne({where: {id: Drink.PriceId}})
                            .then(function (Price) {
                                Price.update({
                                    price: data.price
                                })
                            })
                            .then(function () {
                                // alcohols
                                models.Alcohol.findAll({
                                        where: {
                                            id: {in: data.alcoholList}
                                        }
                                    })
                                    .then(function (Alcohols) {
                                        Drink.setAlcohol(Alcohols)
                                            .then(function () {
                                                // categories
                                                models.Category.findAll({where: {id: {in: data.categoryList}}})
                                                    .then(function (Categories) {
                                                        Drink.setCategories(Categories)
                                                            .then(function () {
                                                                next(null, "done!")
                                                            })
                                                    })
                                            })
                                    })
                            })
                    })
            })
    },

    createDrink: function (models, data, next) {
        models.Drink.create({
                name: data.name,
                description: data.description,
                saveur: data.saveur,
                type: randomIndex(4),
                list: randomIndex(2),
                Price: {
                    price: data.price
                }
            }, {
                include: [models.Price]
            })
            .then(function (Drink) {
                // alcohols
                models.Alcohol.findAll({
                        where: {
                            id: {in: data.alcoholList}
                        }
                    })
                    .then(function (Alcohols) {
                        Drink.setAlcohol(Alcohols)
                            .then(function () {
                                // categories
                                models.Category.findAll({where: {id: {in: data.categoryList}}})
                                    .then(function (Categories) {
                                        Drink.setCategories(Categories)
                                            .then(function () {
                                                next(null, "done!")
                                            })
                                    })
                            })
                    })
            })
    },

    deleteDrink: function (models, data, next) {
        models.Drink.findOne({where: {id: data.id}})
            .then(function (Drink) {
                models.Price.findOne({where: {id: Drink.PriceId}})
                    .then(function (Price) {
                        Drink.destroy().then(function () {
                            Price.destroy().then(function () {
                                next(null)
                            })
                        })
                    })
            })
    },

    editAlcohol: function (models, data, next) {
        models.Alcohol.findOne({where: {id: data.id}})
            .then(function (Alcohol) {
                Alcohol.update({
                        name: data.name,
                        type: data.type,
                        brand: data.brand,
                        updatedAt: new Date()
                    })
                    .then(function () {
                        models.Price.findOne({where: {id: Alcohol.PriceId}})
                            .then(function (Price) {
                                Price.update({
                                        price: data.price
                                    })
                                    .then(function () {
                                        next(null)
                                    })
                            })
                    })
            })
    },

    createAlcohol: function (models, data, next) {
        models.Alcohol.create({
                name: data.name,
                type: data.type,
                brand: data.brand,
                Price: {
                    price: data.price
                }
            }, {
                include: [models.Price]
            })
            .then(function (Drink) {
                next(null, "done!")
            })
    },

    deleteAlcohol: function (models, data, next) {
        models.Alcohol.findOne({where: {id: data.id}})
            .then(function (Alcohol) {
                models.Price.findOne({where: {id: Alcohol.PriceId}})
                    .then(function (Price) {
                        Alcohol.destroy().then(function () {
                            Price.destroy().then(function () {
                                next(null)
                            })
                        })
                    })
            })
    },

    editCategory: function (models, data, next) {
        models.Category.findOne({where: {id: data.id}})
            .then(function (Category) {
                // category prop
                Category.update({
                    name: data.name,
                    description: data.desc1,
                    descriptionLarge: data.desc2,
                    updatedAt: new Date()
                }).then(function () {
                    models.Drink.findAll({
                        where: {
                            id: {in: data.drinks}
                        }
                    }).then(function (Drinks) {
                        Category.setDrinks(Drinks)
                            .then(function () {
                                next(null)
                            })
                    })
                })
            })
    },

    createCategory: function (models, data, next) {
        models.Category.create({
            name: data.name,
            description: data.desc1,
            descriptionLarge: data.desc2,
            list: randomIndex(3),
            Price: {
                price: data.price
            }
        }, {
            include: [models.Price]
        }).then(function (Category) {
            models.Drink.findAll({
                where: {
                    id: {in: data.drinks}
                }
            }).then(function (Drinks) {
                Category.setDrinks(Drinks)
                    .then(function () {
                        next(null)
                    })
            })
        })
    },

    deleteCategory: function (models, data, next) {
        models.Category.findOne({where: {id: data.id}})
            .then(function (Category) {
                models.Price.findOne({where: {id: Category.PriceId}})
                    .then(function (Price) {
                        Category.destroy().then(function () {
                            Price.destroy().then(function () {
                                next(null)
                            })
                        })
                    })
            })
    },

    editFormule: function (models, data, next) {
        models.Formule.findOne({where: {id: data.id}})
            .then(function (Formule) {
                Formule.update({
                    name: data.name,
                    nbPpl: data.nbppl,
                }).then(function () {
                    next(null)
                })
            })
    },

    createFormuleItem: function (models, data, next) {
        models.Formule.findOne({where: {id: data.id}})
            .then(function (Formule) {
                models.Item.create({
                    name: data.itemname,
                    description: data.itemdesc,
                    Price: {
                        price: data.itemprice
                    }
                }, {
                    include: [models.Price]
                }).then(function (Item) {
                    Formule.addItem(Item).then(function () {
                        next(null, {item: Item, price: Item.Price})
                    })
                })
            })
    },

    deleteFormuleItem: function (models, data, next) {
        models.Formule.findOne({where: {id: data.formuleid}})
            .then(function (Formule) {
                models.Item.findOne({where: {id: data.itemid}})
                    .then(function (Item) {
                        Formule.removeItem(Item)
                            .then(function () {
                                next(null)
                            })
                    })
            })
    },

    createFormule: function (models, data, next) {
        models.Formule.create({})
            .then(function (Formule) {
                next(null, Formule)
            })
    },

    deleteFormule: function (models, data, next) {
        models.Formule.findOne({where: {id: data.id}})
            .then(function (Formule) {
                Formule.getItems().then(function (items) {
                    Formule.destroy().then(function () {
                        var itemsToRemove = []
                        items.forEach(function (Item) {
                            itemsToRemove.push(Item.destroy())
                        })
                        Promise.all(itemsToRemove).then(function () {
                            next(null)
                        })
                    })
                })
            })
    }
}

function randomIndex(maxIndex) {
    return Math.floor((Math.random() * maxIndex) + 1);
}