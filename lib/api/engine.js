var api = function () {

    //---
    // core

    this.find = function (model, by, target, next) {
        model.find(this.getWhereConditions(by, target))
            .then(function (r) {
                next(null, {s: true, r: r});
            });
    };

    this.findCustom = function (model, whereclause, next) {
        model.find(whereclause)
            .then(function (r) {
                next(null, {s: true, r: r});
            });
    };

    this.findAllCustom = function (models, model, clause, next) {
        var associations = [];
        Object.keys(model.associations).forEach(function (association) {
            if (models[association] !== undefined)
                if (association !== model.name)
                    associations.push(models[association]);
        });

        var whereClause = {where: clause};
        whereClause['include'] = associations;

        model.findAll(whereClause)
            .then(function (r) {
                next(null, {s: true, r: r});
            });
    };

    this.findAll = function (model, next) {
        model.findAndCountAll()
            .then(function (r) {
                next(null, {s: true, r: r});
            });
    };

    this.findByIteration = function (models, iteration, modalName, next) {
        var MAXPERREQUEST = 10
        if (modalName == "Party") {
            models[modalName].findAll({
                    limit: MAXPERREQUEST,
                    offset: MAXPERREQUEST * iteration,
                    order: [['updatedAt', 'DESC']],
                    include: [
                        models.Event, models.Person
                    ]
                })
                .then(function (r) {
                    next(null, {s: true, r: r});
                });
        } else {
            models[modalName].findAll({
                    limit: MAXPERREQUEST,
                    offset: MAXPERREQUEST * iteration,
                    order: [['updatedAt', 'DESC']]
                })
                .then(function (r) {
                    next(null, {s: true, r: r});
                });
        }
    };


    this.findAndCount = function (model, by, target, next) {

        model.findAndCountAll(this.getWhereConditions(by, target))
            .then(function (r) {
                next(null, {s: true, r: r});
            });
    };

    this.findOne = function (model, by, target, next) {
        model.findOne(this.getWhereConditions(by, target))
            .then(function (r) {
                next(null, r);
            });
    };

    this.findAndExpand = function (models, model, by, target, next) {
        var associations = [];
        Object.keys(model.associations).forEach(function (association) {
            if (models[association] !== undefined)
                associations.push(models[association]);
        });

        var whereClause = this.getWhereConditions(by, target);
        whereClause['include'] = associations;

        model.findAndCountAll(whereClause)
            .then(function (r) {
                next(null, {s: true, r: r});
            });
    };

    this.update = function (model, set, fields, next) {
        model.update(set, fields)
            .then(function () {
                next();
            });
    };

    this.getWhereConditions = function (by, target) {
        var whereClause = {where: {}};
        whereClause.where[by] = target;
        return whereClause;
    };


};


module.exports = api;