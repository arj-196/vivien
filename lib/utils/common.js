module.exports = {

    ifTrue: function (o, a) {
        if(!o)
            return false;
        else
            return o.dataValues[a] == true;
    },

    isObject: function (o) {
       return Object.prototype.toString.call(o) == "[object Object]";
    },

    isArray: function (o) {
        return Object.prototype.toString.call(o) == "[object Array]";
    }
};