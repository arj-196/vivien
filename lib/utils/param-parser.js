var Error = require('../error/error'),
    error = new Error();

module.exports = function() {

    this.parse = function(param) {

        if(!isNaN(param)) // is number
            return param;
        else {
            // else try to parse
            try{
                return JSON.parse(param);
            } catch (err){
                //error.STDError([__dirname, __filename ].join('/'), "parse", "unkown type", err);

                // assuming param is type String
                return param;
            }
        }
    }

};
