var Error = function(){
    this.APIError = function(path, message){
        return {
            s: false,
            e:{
                path: path,
                message: message
            }
        };
    };

    this.renderError = function (path, fn, message, res) {
        res.json({
            path: path,
            fn: fn,
            message: message
        });
    };

    this.STDError = function (path, fn, message, err) {
        this.print("--------", "");
        this.print("ERR:", message);
        this.print("FILE:", path);
        this.print("FN:", fn);
    };

    this.print = function(title, message) {
        console.error(title, message);
    };
};

module.exports = Error;