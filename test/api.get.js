var should = require('should');
var request = require('request');

var env = 'development';
var config = {
    "development": {
        host: 'localhost',
        port: 8000
    }
};

var getUrl = function(path) {
  return 'http://' + config[env].host + ':' + config[env].port + path;
};

var printResults = function (path, chunk) {
    console.log('-----------');
    console.log(typeof chunk);
    console.log('Path: ', path);
    console.log('' + chunk);
};


describe('API.GET', function() {

    describe('/:model/all', function() {

        it('verify responsive object', function (done) {
            request.get(getUrl('/_api/get/core/Formule/all'), function (err, res, body) {
                res.statusCode.should.equal(200);
                body.should.be.an.instanceOf(String);
                var o = JSON.parse(body);
                if(o.s){
                    if(o.r.count > 0){
                        o.r.rows.length.should.equal(o.r.count);
                        o.r.rows.forEach(function (row) {
                            row.should.not.equal(undefined);
                        });
                    }
                }
                done();
            })
        });

    });


    describe('/:model/:id', function() {

        it('verify responsive object', function (done) {
            request.get(getUrl('/_api/get/core/Drink/[1,2]'), function (err, res, body) {
                res.statusCode.should.equal(200);
                body.should.be.an.instanceOf(String);
                var o = JSON.parse(body);
                o.r.count.should.equal(2);
                o.r.rows.length.should.equal(o.r.count);
                o.r.rows.forEach(function (row) {
                    row.should.not.equal(undefined);
                });
                done();
            })
        });

    });

    describe('/byField/:model/:field/:param', function() {

        it('verify responsive object', function (done) {
            request.get(getUrl('/_api/get/core/byField/Drink/id/[1,2]'), function (err, res, body) {
                res.statusCode.should.equal(200);
                body.should.be.an.instanceOf(String);
                var o = JSON.parse(body);
                o.r.count.should.equal(2);
                o.r.rows.length.should.equal(o.r.count);
                o.r.rows.forEach(function (row) {
                    row.should.not.equal(undefined);
                });
                done();
            })
        });

    });

    describe('/expand/:model/:id', function() {

        it('verify responsive object', function (done) {
            var iter = 1;
            var models = ['Drink', 'Event'];
            models.forEach(function (model) {
                request.get(getUrl('/_api/get/core/expand/'+ model+'/[1,2,3,4,5,6,7,8]'), function (err, res, body) {
                    res.statusCode.should.equal(200);
                    body.should.be.an.instanceOf(String);
                    var o = JSON.parse(body);
                    o.r.rows.length.should.equal(o.r.count);
                    o.r.rows.forEach(function (row) {
                        row.should.not.equal(undefined);
                        if(model === 'Drink'){
                            row.Alcohol.should.not.equal(undefined);
                            row.Price.should.not.equal(undefined);
                        } else if (model == 'Event'){
                            row.Theme.should.not.equal(undefined);
                        }
                    });
                    if(iter == models.length)
                        done();
                    else
                        iter++;
                })
            });
        });
    });


});


