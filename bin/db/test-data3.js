var Promise = require('bluebird');
var Faker = require('Faker');

console.warn("Creating Test Data");

// loading models
var env = process.env.NODE_ENV || 'development';
var config = require('../../config/config.js')[env];
var Sequelize = require('sequelize');

var sequelize = new Sequelize(config.database, config.username, config.password, config);

var models = require('../../models/index')(sequelize, Sequelize.DataTypes);


sequelize.sync({})
    .then(function () {
        createFormules();
    });

var createFormules = function () {
    var formules = [
        {name: "STARTER", nbppl: 50},
        {name: "CLASSIQUE", nbppl: 50},
        {name: "ADIEU JEUNE FILLE !", nbppl: 50},
        {name: "FETES DE FIN D'ANNEES", nbppl: 50}
    ]


    formules.forEach(function (formule) {
        var itemsToSave = []
        for (var i = 0; i < 10; i++) {
            itemsToSave.push(sequelize.models.Item.create({
                name: Faker.Lorem.words(3).join(" "),
                description: Faker.Lorem.sentence(),
            }))
        }
        Promise.all(itemsToSave)
            .then(function (items) {
                sequelize.models.Formule.create({
                    name: formule.name,
                    nbPpl: formule.nbppl,
                    type: 0,
                    start: null,
                    end: null,
                }).then(function (savedFormule) {
                    items.forEach(function (item) {
                        savedFormule.addItem(item)
                        sequelize.models.Price.create({
                            price: Faker.random.number(50)
                        }).then(function (price) {
                            item.setPrice(price)
                        })
                    })

                })
            })
    })
};


function randomIndex(maxIndex) {
    return Math.floor((Math.random() * maxIndex) + 1);
}