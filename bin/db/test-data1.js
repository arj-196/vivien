var Promise = require('bluebird');
var Faker = require('Faker');

console.warn("Creating Test Data");

// loading models
var env       = process.env.NODE_ENV || 'development';
var config    = require('../../config/config.js')[env];
var Sequelize = require('sequelize');

var sequelize = new Sequelize(config.database, config.username, config.password, config);

var models = require('../../models/index')(sequelize, Sequelize.DataTypes);


sequelize.sync({})
    .then(function() {
        for(var iter=1; iter < 200; iter++){
            createTestData();
        }
    });

var createTestData = function(){

    sequelize.models.Drink.create({
        name: "drink " + Faker.Lorem.words(1)[0],
        description: Faker.Lorem.sentence(),
        Price: {
            price: Faker.random.number(500)
        }
    },{
        include: [sequelize.models.Price]
    })
        .then(function(drink1) {

            // category
            sequelize.models.Category.create({
                name: "category " + Faker.Lorem.words(1)[0],
                description: Faker.Lorem.sentence()
            }).then(function (category) {
                category.setDrink(drink1);
            });

            // alcohol
            sequelize.models.Alcohol.create({
                name: "alcohol " + Faker.Lorem.words(1)[0]
            }).then(function (vodka) {
                sequelize.models.Alcohol.create({
                    name: "alcohol " + Faker.Lorem.words(1)[0]
                }).then(function (rhum) {
                    drink1.addAlcohol(vodka);
                    drink1.addAlcohol(rhum);
                });
            });

        });


    // party
    Promise.join(

        //barman
        sequelize.models.Barmen.create({
            barmanId: Faker.random.number(99999),
            firstname: Faker.Name.firstName(),
            lastname: Faker.Name.lastName(),
            sex: 'f',
            age: Faker.random.number(40)
        })

        // event
        , sequelize.models.Event.create({
            nbPeople: 50,
            hour: Faker.random.number(10) + 'pm',
            duration: Faker.random.number(10) + 'hrs',
            postal: Faker.Address.zipCode(),
            city: Faker.Address.city(),
            address: Faker.Address.streetAddress(),
            ifBio: false,
            ifOrganised: true
        }, {
            include: [sequelize.models.Theme]
        })

        , sequelize.models.Theme.create({
            name: 'theme ' + Faker.Lorem.words(1)[0],
            Price: {
                price: Faker.random.number(500)
            }
        }, {
            include: [sequelize.models.Price]
        })

        // bar
        , sequelize.models.Bar.create({
            description: Faker.Lorem.words(1)[0],
            typeDrinks: [1]
        })

        // drinks
        , sequelize.models.Drink.create({
            name: Faker.Lorem.words(1)[0],
            description: Faker.Lorem.sentence(),
            Price: {
                price: Faker.random.number(500)
            }
        },{
            include: [sequelize.models.Price]
        })

        , sequelize.models.Drink.create({
            name: Faker.Lorem.words(1)[0],
            description: Faker.Lorem.sentence(),
            Price: {
                price: Faker.random.number(500)
            }
        },{
            include: [sequelize.models.Price]
        })

        , function(barman, event, theme, bar, drink1, drink2) {
            event.setTheme(theme);
            bar.addDrink(drink1);
            bar.addDrink(drink2);
            bar.save();

            sequelize.models.Party.create({
                ifFormule: false,
                date: Faker.Date.future(Faker.random.number(5))
            })
                .then(function (party) {
                    party.addBarmen(barman);
                    party.setEvent(event);
                    party.setBar(bar);
                });
        }


    );


};