var Promise = require('bluebird');
var Faker = require('Faker');

console.warn("Creating Test Data");

// loading models
var env = process.env.NODE_ENV || 'development';
var config = require('../../config/config.js')[env];
var Sequelize = require('sequelize');

var sequelize = new Sequelize(config.database, config.username, config.password, config);

var models = require('../../models/index')(sequelize, Sequelize.DataTypes);


sequelize.sync({})
    .then(function () {
        createTypeDrinks();
        createDrinkCategories();
    });

var createTypeDrinks = function () {
    var themes = {
        "1": {name: "médieval", Price: 999},
        "2": {name: "indépendance day", Price: 999},
        "3": {name: "révolution", Price: 999},
        "4": {name: "couleurs", Price: 999},
        "5": {name: "hipie", Price: 999},
        "6": {name: "50's", Price: 999},
        "7": {name: "60's", Price: 999},
        "8": {name: "70's", Price: 999},
        "9": {name: "80's", Price: 999},
        "10": {name: "90's", Price: 999},
        "11": {name: "50's", Price: 999},
        "12": {name: "60's", Price: 999},
        "13": {name: "70's", Price: 999}
    };

    for (var i = 1; i < 14; i++) {
        var item = themes[i];
        sequelize.models.Theme.create({
            name: item.name,
            list: randomIndex(3),
            Price: {
                price: Faker.random.number(50)
            }
        }, {
            include: [sequelize.models.Price]
        });
    }
};

var createDrinkCategories = function () {
    var categories = {

        0: {
            type: "cocktails",
            name: "des cocktails",
            description: "Mojito, Cosmopolitain, Long Island, etc.",
            descriptionLarge: 'Location verrerie "basic", les ingrédients  et les glaéons'
        },
        1: {
            type: "champagne",
            name: "du champagne",
            description: "Brut, Rosé, Blancs de blancs, Cuvée spéciale, etc.",
            descriptionLarge: 'Location verrerie "basic", les ingrédients néces "basic"'
        },
        2: {
            type: "longs drinks",
            name: "des longs drinks",
            description: "Vodka Redbull&reg;, Gin Tonic;, Wishky Coca-Cola&reg;, etc.",
            descriptionLarge: 'Location verrerie "basic", les ingrédients nécessaires é ns'
        },
        3: {
            type: "biére",
            name: "de la biére",
            description: "Blonde, Blanche, Brune, Desperados, Corona, etc.",
            descriptionLarge: "Location 50 verres é biére,s"
        },
        4: {
            type: "shots",
            name: "des shots",
            description: "Vodka Redbull&reg;, Gin Tonic;, Wishky Coca-Cola&reg;, etc.",
            descriptionLarge: 'Location verrerie'
        },
        5: {
            type: "softs",
            name: "des softs",
            description: "just de fruits frais, jus et sodas, smoothies, etc.",
            descriptionLarge: 'Location verrerie "basic", les ingrédients nécessaires é la r"'
        }
    };

    var iterDrink = 0;
    var drinks = {

        1: {
            name: "le fameux mojito",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        2: {
            name: "les caïpirihna (5)",
            img: null,
            saveur: "saveur3",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        3: {
            name: "les martini (4)",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        4: {
            name: "négroni",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        5: {
            name: "le russe blanc",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        6: {
            name: "pimm's",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        7: {
            name: "la vodka/téquila",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        8: {
            name: "le sex on the beach",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        9: {
            name: "la pina colada",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        10: {
            name: "les margarita (4)",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        11: {
            name: "le cosmopolitain",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        12: {
            name: "le kiwizz",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        13: {
            name: "le maï taï",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        14: {
            name: "les daïquiris (3)",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        15: {
            name: "le mint julep",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        16: {
            name: "blue lagin",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        17: {
            name: "gin/vodka/rhum fizzz",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        18: {
            name: "old fashioned",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        19: {
            name: "cointrau concomber",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        20: {
            name: "les mojitos (4)",
            img: null,
            saveur: "saveur2",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        21: {
            name: "l'alexandra",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        22: {
            name: "nuit de noces",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        23: {
            name: "gin garrden martini",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        24: {
            name: "sugar-sugar",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        25: {
            name: "french slash",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        26: {
            name: "quatres saisons",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        27: {
            name: "tagada orange",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        28: {
            name: "sex appeal",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        29: {
            name: "le vrai bloody mary",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        30: {
            name: "l'aviation",
            img: null,
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        }

    };

    // building alcohols
    var alcoholsToBeSaved = [];
    for (var i = 0; i < 6; i++) {
        alcoholsToBeSaved.push(
            sequelize.models.Alcohol.create({
                name: Faker.Lorem.words(1)[0],
                type: Faker.Lorem.words(1)[0],
                brand: Faker.Lorem.words(1)[0],
                Price: {
                    price: Faker.random.number(50)
                }
            }, {
                include: [sequelize.models.Price]
            })
        );
    }

    Promise.all(alcoholsToBeSaved).then(function (alcohols) {
        for (var i = 0; i < 6; i++) {
            sequelize.models.Category.create({
                type: categories[i].type,
                name: categories[i].name,
                description: categories[i].description,
                descriptionLarge: categories[i].descriptionLarge,
                list: randomIndex(3),
                Price: {
                    price: Faker.random.number(50)
                }
            }, {
                include: [sequelize.models.Price]
            })
                .then(function (Category) {
                    for (var j = 0; j < 30; j++) {
                        if(iterDrink > 29){
                            iterDrink = 1;
                        }
                        iterDrink++;

                        sequelize.models.Drink.create({
                            name: drinks[iterDrink].name,
                            description: Faker.Lorem.sentence(20),
                            saveur: Faker.Lorem.sentence(5),
                            type: randomIndex(4),
                            list: randomIndex(2),
                            Price: {
                                price: Faker.random.number(50)
                            }
                        }, {
                            include: [sequelize.models.Price]
                        }).then(function (Drink) {
                            Category.addDrink(Drink);
                            Drink.addAlcohol(alcohols[randomIndex(alcohols.length) - 1]);
                            Drink.addAlcohol(alcohols[randomIndex(alcohols.length) - 1]);
                        }).catch(function (err) {
                            console.log(err);
                        });
                    }
                });
        }
    });

};


function randomIndex(maxIndex) {
    return Math.floor((Math.random() * maxIndex) + 1);
}