var Promise = require("bluebird");
var _ = require('underscore')

module.exports = {

    Bio: {
        PriceId: 1,
        type: "option_bio",
        description: "Vous avez choisi l'option bio"
    },

    Price: {
        glass: {
            superieur: 12
        }
    },

    // type event
    typeEvent: {
        "1": "anniversaire",
        "2": "mariage",
        "3": "célébration religieuse",
        "4": "célébration privée",
        "5": "comité d'entreprise",
        "6": "pot de départ",
        "7": "salon professionnel",
        "8": "soirée de rallye",
        "9": "boom (sans alcool",
        "10": "enterrement de vie de garéon",
        "11": "enterrement de vie de jeune fille",
        "12": "formule de saison",
        "13": "formule du mois",
        "14": "bar é shots",
        "15": "formule s'adapter é mon théme",
        "16": "salon",
        "17": "autre"
    },

    // decision Theme
    decisionTheme: {
        1: "oui, j'ai un théme dément !",
        2: "oui, mais je n'ai pas de théme",
        3: "non, je n'ai pas de théme"
    },

    // type themes
    typeTheme: {
        "1": {description: "médieval", Price: {price: 999}},
        "2": {description: "indépendance day", Price: {price: 999}},
        "3": {description: "révolution", Price: {price: 999}},
        "4": {description: "couleurs", Price: {price: 999}},
        "5": {description: "hipie", Price: {price: 999}},
        "6": {description: "50's", Price: {price: 999}},
        "7": {description: "60's", Price: {price: 999}},
        "8": {description: "70's", Price: {price: 999}},
        "9": {description: "80's", Price: {price: 999}},
        "10": {description: "90's", Price: {price: 999}},
        "11": {description: "50's", Price: {price: 999}},
        "12": {description: "60's", Price: {price: 999}},
        "13": {description: "70's", Price: {price: 999}}
    },

    // drink categories
    categories: {
        10: {
            type: "cocktails",
            name: "des cocktails",
            Price: {price: 9999.99},
            description: "Mojito, Cosmopolitain, Long Island, etc.",
            descriptionLarge: 'Location verrerie "basic", les ingrédients nécessaires é la réalisation,, les pailles, les touilleurs, les petites serviettes et les glaéons'
        },
        1: {
            type: "champagne",
            name: "du champagne",
            Price: {price: 9999.99},
            description: "Brut, Rosé, Blancs de blancs, Cuvée spéciale, etc.",
            descriptionLarge: 'Location verrerie "basic", les ingrédients nécessaires é la réalisation,'
        },

        2: {
            type: "longs drinks",
            name: "des longs drinks",
            Price: {price: 9999.99},
            description: "Vodka Redbull&reg;, Gin Tonic;, Wishky Coca-Cola&reg;, etc.",
            descriptionLarge: 'Location verrerie "basic", les ingrédients nécessaires é la réalisation,es petites serviettes et les glaéons'
        },

        3: {
            type: "biére",
            name: "de la biére",
            Price: {price: 9999.99},
            description: "Blonde, Blanche, Brune, Desperados, Corona, etc.",
            descriptionLarge: "Location 50 verres é biére, 1 fét é biére (30 litres) au choix, 1 pompe é biére, 2 adjvants"
        },

        4: {
            type: "shots",
            name: "des shots",
            Price: {price: 9999.99},
            description: "Vodka Redbull&reg;, Gin Tonic;, Wishky Coca-Cola&reg;, etc.",
            descriptionLarge: 'Location verrerie "basic", les ingrédients nécessaires é la réalisation,es petites serviettes'
        },
        5: {
            type: "softs",
            name: "des softs",
            Price: {price: 9999.99},
            description: "just de fruits frais, jus et sodas, smoothies, etc.",
            descriptionLarge: 'Location verrerie "basic", les ingrédients nécessaires é la réalisation,'
        }
    },

    alcohols: {
        0: {
            img: "/content/img/alcooldebase/alcool.png",
            description: "GREY GOOSE",
            name: "Vodka",
            Price: {price: 999.99}
        },
        1: {
            img: "/content/img/alcooldebase/alcool.png",
            description: "GREY GOOSE",
            name: "Vodka",
            Price: {price: 999.99}
        },
        2: {
            img: "/content/img/alcooldebase/alcool.png",
            description: "GREY GOOSE",
            name: "Vodka",
            Price: {price: 999.99}
        },
        3: {
            img: "/content/img/alcooldebase/alcool.png",
            description: "GREY GOOSE",
            name: "Vodka",
            Price: {price: 999.99}
        },
        4: {
            img: "/content/img/alcooldebase/alcool.png",
            description: "GREY GOOSE",
            name: "Vodka",
            Price: {price: 999.99}
        },
        5: {
            img: "/content/img/alcooldebase/alcool.png",
            description: "GREY GOOSE",
            name: "Vodka",
            Price: {price: 999.99}
        },
        6: {
            img: "/content/img/alcooldebase/alcool.png",
            description: "GREY GOOSE",
            name: "Vodka",
            Price: {price: 999.99}
        },
        7: {
            img: "/content/img/alcooldebase/alcool.png",
            description: "GREY GOOSE",
            name: "Vodka",
            Price: {price: 999.99}
        }
    },

    typeDrinks: {
        1: {name: "Les Classiques"},
        2: {name: "Les cocktails de saison"},
        3: {name: "Les hards"},
        4: {name: "Notre sélection"}

    },

    drinks: {

        1: {
            name: "le fameux mojito",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        2: {
            name: "les caïpirihna (5)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur3",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        3: {
            name: "les martini (4)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        4: {
            name: "négroni",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        5: {
            name: "le russe blanc",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        6: {
            name: "pimm's",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        7: {
            name: "la vodka/téquila",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        8: {
            name: "le sex on the beach",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        9: {
            name: "la pina colada",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        10: {
            name: "les margarita (4)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        11: {
            name: "le cosmopolitain",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        12: {
            name: "le kiwizz",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        13: {
            name: "le maï taï",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        14: {
            name: "les daïquiris (3)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        15: {
            name: "le mint julep",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        16: {
            name: "blue lagin",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        17: {
            name: "gin/vodka/rhum fizzz",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        18: {
            name: "old fashioned",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        19: {
            name: "cointrau concomber",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        20: {
            name: "les mojitos (4)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur2",
            description: "la liste des ingrédients",
            type: 0,
            list: 1
        },
        21: {
            name: "l'alexandra",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        22: {
            name: "nuit de noces",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        23: {
            name: "gin garrden martini",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        24: {
            name: "sugar-sugar",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        25: {
            name: "french slash",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        26: {
            name: "quatres saisons",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        27: {
            name: "tagada orange",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        28: {
            name: "sex appeal",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        29: {
            name: "le vrai bloody mary",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        },
        30: {
            name: "l'aviation",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            type: 0,
            list: 2
        }

    },

    item_type_mapping: {
        0: "classic",
        1: "saison",
        2: "hard",
        3: "selection"
    },

    service_bar: {
        4: {description: "1 heure", Price: {price: 654}, PriceId: 1, type: "service_bar"},
        1: {description: "2 heure", Price: {price: 754}, PriceId: 1, type: "service_bar"},
        2: {description: "3 heure", Price: {price: 854}, PriceId: 1, type: "service_bar"},
        3: {description: "4 heure", Price: {price: 454}, PriceId: 1, type: "service_bar"}
    },

    service_fontaine: {
        4: {description: "2 litres", Price: {price: 456}, PriceId: 1, type: "service_fontaine"},
        1: {description: "4 litres", Price: {price: 656}, PriceId: 1, type: "service_fontaine"},
        2: {description: "8 litres", Price: {price: 756}, PriceId: 1, type: "service_fontaine"},
        3: {description: "11 litres", Price: {price: 856}, PriceId: 1, type: "service_fontaine"}
    },

    service_glass: {
        4: {description: "1,5 litres", Price: {price: 12}, PriceId: 1, type: "service_glass"},
        1: {description: "2 litres", Price: {price: 12}, PriceId: 1, type: "service_glass"},
        2: {description: "2,5 litres", Price: {price: 12}, PriceId: 1, type: "service_glass"},
        3: {description: "3 litres", Price: {price: 12}, PriceId: 1, type: "service_glass"}
    },

    rajoutePersonnel: {
        1: {description: "commis", Price: {price: 160}, type: "rajoutePersonnel"},
        2: {description: "barman", Price: {price: 198}, type: "rajoutePersonnel"}
    },

    completerservicestage5: {
        1: {
            name: "location de verres",
            Price: {price: 98},
            description: "Mojito, Cosmopolitain, Long Island, etc.",
            subtitle: 'Location verrerie "basic", les ingrédients nécessaires é la réalisation, set de condiments, la décoration des verres "basic", les pailles, les touilleurs, les petites serviettes et les glaéons'
        },
        2: {
            name: "location de matériel",
            Price: {price: 79},
            description: "Brut, Rosé, Blancs de blancs, Cuvée spéciale, etc.",
            subtitle: 'Location verrerie "basic", les ingrédients nécessaires é la réalisation, set de condiments, la décoration des verres "basic"'
        },
        3: {
            name: "achat de glaéons",
            Price: {price: 59},
            description: "Vodka Redbull&reg;, Gin Tonic;, Wishky Coca-Cola&reg;, etc.",
            subtitle: 'Location verrerie "basic", les ingrédients nécessaires é la réalisation, set de condiments, les pailles, les touilleurs, les petites serviettes et les glaéons'
        }

    },

    ice: {

        1: {description: "glaçons cubes (10 kg)", Price: {price: 8.90}},
        2: {description: "glaçe pilée (10 kg)", Price: {price: 14}},
        3: {description: "monsieur glaçon x 50", Price: {price: 25}},
        4: {description: "glaçon colorés (5 kg)", Price: {price: 25}},
        5: {description: "glaçons aux fruits (5 kg )", Price: {price: 50}},
        6: {description: "glaçons coeur surprise x 50", Price: null},
        7: {description: "glaçons goût x 50", Price: null},
        8: {description: "glaçons lumineux x 50", Price: null}
    },

    animation: {

        16: {description: "les quatres saisons", img: "/content/img/animation/01.jpg", Price: {price: 456}, type: null},
        1: {
            description: "fontaine à cocktail",
            img: "/content/img/42d0842fbda2edda67e7ffd0ea9fe42c_1.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "fontaine"
        },
        2: {
            description: "fontaine à absynthe",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "absynthe"
        },
        3: {
            description: "cascade de champagne",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "cascadechampagne"
        },
        4: {
            description: "bar à bonbon",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "barabonbon"
        },
        5: {
            description: "bar enflamé",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "barenflame"
        },
        6: {
            description: "shots enflamés",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "shotsenflames"
        },
        7: {
            description: "bar feu d'artifices",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "barfeudartifices"
        },
        8: {
            description: "duo devil",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "duodevil"
        },
        9: {
            description: "trio devil",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "triodevil"
        },
        10: {
            description: "verre & pailles xxl",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "verresetpaillesxxl"
        },
        11: {
            description: "cotillons et déguisements",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "cotillonsetdeguisements"
        },
        12: {
            description: "orangina renversé",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "oranginarenverse"
        },
        13: {
            description: "cocktail à bonbon",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "cocktailabonbon"
        },
        14: {
            description: "rainbow shots",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "rainbowshots"
        },
        15: {
            description: "caviar de cocktail",
            img: "/content/img/animation/01.jpg",
            Price: {price: 456},
            PriceId: 1,
            type: "caviardecocktail"
        }
    },

    animation_option_fontaine: {
        0: {description: "je veux une fontaine de 3 litres", Price: {price: 456}},
        1: {description: "je veux une fontaine de 4 litres", Price: {price: 456}},
        2: {description: "je veux une fontaine de 5 litres", Price: {price: 456}},
        3: {description: "je veux une fontaine de 10 litres", Price: {price: 456}}
    },

    absynthe_type: {
        0: {name: "Les absynthes"},
        1: {name: "Les absynthes de saison"}
    },

    absynthe_drinks: {

        0: {
            name: "le fameux mojito",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        1: {
            name: "les mojitos (4)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur2",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        2: {
            name: "les caïpirihna (5)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur3",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        3: {
            name: "les martini (4)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        4: {
            name: "négroni",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        5: {
            name: "le russe blanc",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        6: {
            name: "pimm's",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        7: {
            name: "la vodka/téquila",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        8: {
            name: "le sex on the beach",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        9: {
            name: "la pina colada",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        10: {
            name: "les margarita (4)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        11: {
            name: "le cosmopolitain",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        12: {
            name: "le kiwizz",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        13: {
            name: "le maï taï",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveurs: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        }
    },

    shots_type: {
        0: {name: "Les shots"},
        1: {name: "Les shots de saison"}
    },

    shots_drinks: {
        0: {
            name: "le fameux mojito",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        1: {
            name: "les mojitos (4)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur2",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        2: {
            name: "les caïpirihna (5)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur3",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        15: {
            name: "le mint julep",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 2
        },
        16: {
            name: "blue lagin",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 2
        },
        17: {
            name: "gin/vodka/rhum fizzz",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 2
        }
    },

    animation_options_shotsflames: {
        0: {Price: {price: 14.15}}
    },

    verre_type: {
        0: {name: "1,5 litres", Price: {price: 11.12}},
        1: {name: "2 litres", Price: {price: 15.12}},
        2: {name: "2,5 litres", Price: {price: 20.12}},
        3: {name: "3 litres", Price: {price: 25.12}}
    },

    verre_drink_type: {
        0: {name: "Les classiques"},
        1: {name: "Le cocktails de saison"},
        2: {name: "Notre sélection"}
    },

    verre_drinks: {
        0: {
            name: "le fameux mojito",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        1: {
            name: "les mojitos (4)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur2",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        2: {
            name: "les caïpirihna (5)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur3",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        15: {
            name: "le mint julep",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 2
        },
        16: {
            name: "blue lagin",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 2
        },
        17: {
            name: "gin/vodka/rhum fizzz",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 2
        }
    },

    cocktail_a_bonbon_type: {
        0: {name: "Les classiques"},
        1: {name: "Le cocktails de saison"},
        2: {name: "Notre sélection"}

    },

    cocktail_a_bonbon_drinks: {

        0: {
            name: "le fameux mojito",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 1
        },
        1: {
            name: "les mojitos (4)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur2",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        2: {
            name: "les caïpirihna (5)",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur3",
            description: "la liste des ingrédients",
            multiple: true,
            type: 0,
            list: 1
        },
        15: {
            name: "le mint julep",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 2
        },
        16: {
            name: "blue lagin",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 2
        },
        17: {
            name: "gin/vodka/rhum fizzz",
            img: "824bdca281e3e16a6551e245783af755.jpg",
            saveur: "saveur1",
            description: "la liste des ingrédients",
            multiple: false,
            type: 0,
            list: 2
        }
    },

    animation_options_quatresaisons: {
        0: {Price: {price: 48.48}}
    },

    animation_options_optcascadechampagne: {
        0: {Price: {price: 48.48}}
    },

    animation_options_bonbonabar: {
        1: {name: "optrecipient", Price: {price: 14.14}},
        2: {name: "optcone", Price: {price: 15.15}},
        3: {name: "shtroomphs", Price: {price: 16.16}},
        4: {name: "crocodiles", Price: {price: 17.17}},
        5: {name: "fraises", Price: {price: 18.18}},
        6: {name: "cocas", Price: {price: 19.19}},
        7: {name: "oeufs", Price: {price: 20.20}},
        8: {name: "poidsbonbon", Price: 0}
    },

    animation_options_devil: {
        1: {name: "shotsduodevilprice", Price: {price: 11.22}},
        2: {name: "shotstriodevilprice", Price: {price: 33.44}}

    },

    animation_options_orangina: {
        1: {name: "oranginarenverseprice", Price: {price: 11.22}},
        2: {name: "cocktailabonbonprice", Price: {price: 33.44}}
    },

    animation_options_barenflame: {
        1: {Price: {price: 10}}
    },




    revetement_texture: {
        0: {name: "Bois", img: "/content/img/revetement/bois.jpg", PriceId: 2, Price: {price: 30.00}, type: "revetement_texture"},
        1: {name: "Python", img: "/content/img/revetement/python.jpg", PriceId: 2, Price: {price: 50.00}, type: "revetement_texture"},
        2: {name: "Bleu", img: "/content/img/revetement/bleu.jpg", PriceId: 2, Price: {price: 40.00}, type: "revetement_texture"},
        3: {name: "Rouge", img: "/content/img/revetement/rouge.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        4: {name: "Marble", img: "/content/img/revetement/marble.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        5: {name: "Cuir", img: "/content/img/revetement/crocodile.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        6: {name: "Rose", img: "/content/img/revetement/rose.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        7: {name: "Vert", img: "/content/img/revetement/vert.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        8: {name: "Capitoné", img: "/content/img/revetement/capitone.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        9: {name: "Crocodile", img: "/content/img/revetement/crocodile.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        10: {name: "Jaune", img: "/content/img/revetement/jaune.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        11: {name: "Noir", img: "/content/img/revetement/noir.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        12: {name: "Fleuri", img: "/content/img/revetement/fleuri.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        13: {name: "Autre", img: "/content/img/revetement/autre.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        14: {name: "Strass", img: "/content/img/revetement/strass.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"},
        15: {name: "Autre", img: "/content/img/revetement/autre.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_texture"}
    },

    revetement_coleur: {
        0: {name: "COULEUR 1", img: "/content/img/revetement/bois.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        1: {name: "COULEUR 2", img: "/content/img/revetement/python.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        2: {name: "COULEUR 3", img: "/content/img/revetement/bleu.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        3: {name: "COULEUR 4", img: "/content/img/revetement/rouge.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        4: {name: "COULEUR 5", img: "/content/img/revetement/marble.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        5: {name: "COULEUR 6", img: "/content/img/revetement/crocodile.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        6: {name: "COULEUR 7", img: "/content/img/revetement/rose.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        7: {name: "COULEUR 8", img: "/content/img/revetement/vert.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        8: {name: "COULEUR 9", img: "/content/img/revetement/capitone.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        9: {name: "COULEUR 10", img: "/content/img/revetement/crocodile.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        10: {name: "COULEUR 11", img: "/content/img/revetement/jaune.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        11: {name: "COULEUR 12", img: "/content/img/revetement/noir.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        12: {name: "COULEUR 13", img: "/content/img/revetement/fleuri.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        13: {name: "COULEUR 14", img: "/content/img/revetement/autre.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        14: {name: "COULEUR 15", img: "/content/img/revetement/strass.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"},
        15: {name: "COULEUR 16", img: "/content/img/revetement/autre.jpg", PriceId: 2, Price: {price: 20.00}, type: "revetement_coleur"}
    },

    texture_au_choix: {
        0: {name: "TEXTURE 1", img: "/content/img/revetement/bois.jpg", PriceId: 2, Price: {price: 20.00}, type: "texture_au_choix"},
        1: {name: "TEXTURE 2", img: "/content/img/revetement/python.jpg", PriceId: 2, Price: {price: 20.00}, type: "texture_au_choix"},
        2: {name: "TEXTURE 3", img: "/content/img/revetement/bleu.jpg", PriceId: 2, Price: {price: 20.00}, type: "texture_au_choix"},
        3: {name: "TEXTURE 4", img: "/content/img/revetement/rouge.jpg", PriceId: 2, Price: {price: 20.00}, type: "texture_au_choix"},
        4: {name: "TEXTURE 5", img: "/content/img/revetement/marble.jpg", PriceId: 2, Price: {price: 20.00}, type: "texture_au_choix"},
        5: {name: "TEXTURE 6", img: "/content/img/revetement/crocodile.jpg", PriceId: 2, Price: {price: 20.00}, type: "texture_au_choix"},
        6: {name: "TEXTURE 7", img: "/content/img/revetement/rose.jpg", PriceId: 2, Price: {price: 20.00}, type: "texture_au_choix"},
        7: {name: "TEXTURE 8", img: "/content/img/revetement/vert.jpg", PriceId: 2, Price: {price: 20.00}, type: "texture_au_choix"}
    },

    extension_bar_1: {PriceId: 2, Price: {price: 150}, category: "extension_bar_1", type: "extension_bar_1"},

    extension_bar_2: {PriceId: 2, Price: {price: 250}, category: "extension_bar_2", type: "extension_bar_2"},

    deux_bar: {PriceId: 2, Price: {price: 1000}, category: "deux_bar", type: "deux_bar"},

    no_lift: {PriceId: 2, Price: {price: 1000}, category: "no_lift", type: "no_lift"},

    get: function (name, id) {
        var self = this;
        return new Promise(function (resolve, reject) {
            if (self[name] != undefined) {
                if (id == undefined)
                    resolve(self[name]);
                else
                    resolve(self[name][id]);
            }
            else{
                reject();
            }
        });
    }
};