module.exports = {

    // landing
    "home": "public/home.html",

    // espace particulier
    "espace_particulier": "public/formulessurmesures.html",
    "tutorial_particulier": "public/tutoriel.html",

    //---------------------------

    // formules

    // formules
    "formule_list": "public/tableauformules.html",

    "formule_detail": "public/formulesdetails.html",

    //---------------------------

    // sur measure

    // LEVEL 1

    // sur_measure_l1:stage1 :
    "sur_measure_l1_1": "public/convives.html",

    // sur_measure_l1:stage2 :
    "sur_measure_l1_2": "public/renseignement.html",

    // sur_measure_l1:stage3 :
    "sur_measure_l1_3": "public/propositionformule.html",

    // sur_measure_l1:stage4 :
    "sur_measure_l1_4": "public/theme.html",

    // sur_measure_l1:stage5 :
    "sur_measure_l1_5": "public/ifbio.html",

    // END LEVEL 1
    //---------------------------

    // LEVEL 2

    // sur_measure_l2:stage1 :
    "sur_measure_l2_1": "public/typedeboissons.html",

    // sur_measure_l2:stage2a :
    "sur_measure_l2_2_a": "public/decoration.html",

    // sur_measure_l2:stage2b :
    "sur_measure_l2_2_b": "public/combiendecocktails.html",

    // sur_measure_l2:stage2c :
    "sur_measure_l2_2_c": "public/alcooldebase.html",

    // sur_measure_l2:stage2d :
    "sur_measure_l2_2_d": "public/cartecocktails.html",

    // sur_measure_l2:stage3 :
    "sur_measure_l2_3": "public/recettesupplementaire.html",

    // sur_measure_l2:stage4 :
    "sur_measure_l2_4": "public/lesglacons.html",

    // sur_measure_l2:stage5 :
    "sur_measure_l2_5": "public/completerleservice.html",

    // sur_measure_l2:stage6 :
    "sur_measure_l2_6": "public/cartecocktailsorange.html",

    // END LEVEL 2
    //---------------------------


    // LEVEL 3

    // sur_measure_l3:stage1 :
    "sur_measure_l3_1": "public/animation.html",

    // END LEVEL 3
    //---------------------------

    // LEVEL 4

    // sur_measure_l4:stage1 :
    "sur_measure_l4_1": "public/lebar.html",

    // END LEVEL 4
    //---------------------------

    // LEVEL 5

    // sur_measure_l5:stage2 :
    "sur_measure_l5_2": "public/voulezvousunbarman.html",

    // sur_measure_l5:stage3 :
    "sur_measure_l5_3": "public/barmanfilleougarcon.html",

    // sur_measure_l5:stage4 :
    "sur_measure_l5_4": "public/rajouterpersonnel.html",

    // sur_measure_l5:stage5 :
    "sur_measure_l5_5": "public/completerservice.stage5.html",

    // END LEVEL 5
    //---------------------------

    // Finalize Command

    // finalize:stage1:offer
    "finalize_1_offer": "public/offre.html",

    // finalize:stage2:macommande
    "finalize_2_macommande": "public/macommande.html",

    "finalize_2_macommande_list": "public/macommande_list.html",

    // finalize:stage4:info personal
    "finalize_3_information_personal": "public/quelquesconfidences.html",

    "finalize_4_paye_ma_commande": "public/payementonline.html",



    // END Finalize Command
    //---------------------------


    // message
    "message": "public/message.html",
    // alert
    "alert": "public/alert.html",
    // validate age
    "validateAge": "public/majeures.html",
    // error
    "error": "error.html",
    // contact
    "contact": "public/contactermonsieurvivian.html",

    test: "test.html",


    //---------------------------
    // ADMIN

    admin_landing: "admin/landing.html",

    admin_drinks: "admin/drinks.html",

    admin_alcohols: "admin/alcohols.html",

    admin_categories: "admin/categories.html",

    admin_formules: "admin/formules.html",

    admin_parties: "admin/parties.html",

};