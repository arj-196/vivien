/**
 * Contains Map of Route to Controller
 *
 */
module.exports = {

    "prefix": {
    "all": "/*",
    "espace": "/espace*",
    "espace_particulier": "/espace/particulier*"
    },

    // default:homepage
    "root": "/",

    // default:espaceParticulier
    "espace_particulier": "/espace/particulier",

    // default:tutoParticulier
    "tutorial_particulier": "/espace/particulier/tuto",

    //---------------------------

    // formules

    // formule:listFormule
    "formule_list": "/espace/particulier/formules",

    // formule:formuleDetails
    "formule_details": "/espace/particulier/formules/:ID",

    "formule_choose": "/espace/particulier/formules/:ID/choisir",

    //---------------------------

    // sur measure

    // LEVEL 1

    // sur_measure_l1:stage1 :
    "sur_measure_l1_1": "/espace/particulier/surmeasure/1/1",

    // sur_measure_l1:stage2 :
    "sur_measure_l1_2": "/espace/particulier/surmeasure/1/2",

    // sur_measure_l1:stage3 :
    "sur_measure_l1_3": "/espace/particulier/surmeasure/1/3",

    // sur_measure_l1:stage4 :
    "sur_measure_l1_4": "/espace/particulier/surmeasure/1/4",

    // sur_measure_l1:stage5 :
    "sur_measure_l1_5": "/espace/particulier/surmeasure/1/5",

    // sur_measure_l1:stage6 :
    "sur_measure_l1_6": "/espace/particulier/surmeasure/1/6",

    // END LEVEL 1
    //---------------------------

    // LEVEL 2

    // sur_measure_l2:stage1 :
    "sur_measure_l2_1": "/espace/particulier/surmeasure/2/1",

    // sur_measure_l2:stage2 :
    "sur_measure_l2_2": "/espace/particulier/surmeasure/2/2",

    // sur_measure_l2:stage2a :
    "sur_measure_l2_2_a": "/espace/particulier/surmeasure/2/2/a/:category",

    // sur_measure_l2:stage2b :
    "sur_measure_l2_2_b": "/espace/particulier/surmeasure/2/2/b/:category/:upgrade",

    // sur_measure_l2:stage2c :
    "sur_measure_l2_2_c": "/espace/particulier/surmeasure/2/2/c/:category/:upgrade/:nbdrinks",

    // sur_measure_l2:stage2d :
    "sur_measure_l2_2_d": "/espace/particulier/surmeasure/2/2/d/:category/:upgrade/:nbdrinks/:alcools",

    // sur_measure_l2:stage3 :
    "sur_measure_l2_3": "/espace/particulier/surmeasure/2/3/:category",

    // sur_measure_l2:stage4 :
    "sur_measure_l2_4": "/espace/particulier/surmeasure/2/4/:category",

    // sur_measure_l2:stage5 :
    "sur_measure_l2_5": "/espace/particulier/surmeasure/2/5/:category",

    // sur_measure_l2:stage6 :
    "sur_measure_l2_6": "/espace/particulier/surmeasure/2/6/:category",

    // sur_measure_l2:stageVerify :
    "sur_measure_l2_verify": "/espace/particulier/surmeasure/2/ok",

    // END LEVEL 2
    //---------------------------


    // LEVEL 3

    // sur_measure_l3:stage1 :
    "sur_measure_l3_1": "/espace/particulier/surmeasure/3/",

    // sur_measure_l3:stageVerify :
    "sur_measure_l3_verify": "/espace/particulier/surmeasure/3/ok",

    // END LEVEL 3
    //---------------------------

    // LEVEL 4

    // sur_measure_l4:stage1 :
    "sur_measure_l4_1": "/espace/particulier/surmeasure/4/",

    // sur_measure_l4:stageVerify :
    "sur_measure_l4_verify": "/espace/particulier/surmeasure/4/ok",

    // END LEVEL 4
    //---------------------------


    // LEVEL 5

    // sur_measure_l5:stage1 :
    "sur_measure_l5_1": "/espace/particulier/surmeasure/5/",

    // sur_measure_l5:stage2 :
    "sur_measure_l5_2": "/espace/particulier/surmeasure/5/2",

    // sur_measure_l5:stage3 :
    "sur_measure_l5_3": "/espace/particulier/surmeasure/5/3",

    // sur_measure_l5:stage4 :
    "sur_measure_l5_4": "/espace/particulier/surmeasure/5/4",

    // sur_measure_l5:stage5 :
    "sur_measure_l5_5": "/espace/particulier/surmeasure/5/5",

    // sur_measure_l5:stage6 :
    "sur_measure_l5_6": "/espace/particulier/surmeasure/5/6",

    // sur_measure_l5:stageVerify :
    "sur_measure_l5_verify": "/espace/particulier/surmeasure/5/ok",

    // END LEVEL 5
    //---------------------------


    // LEVEL 6

    // sur_measure_l6:stage1 :
    "sur_measure_l6_1": "/espace/particulier/surmeasure/6/",

    // sur_measure_l6:stageVerify :
    "sur_measure_l6_verify": "/espace/particulier/surmeasure/6/ok",

    // END LEVEL 6
    //---------------------------

    // Finalize Command

    // finalize:stage1:offer
    "finalize_1_offer": "/espace/particulier/finalize/1/offre",

    // finalize:stage2:macommande
    "finalize_2_macommande": "/espace/particulier/finalize/2/macommande",

    "finalize_2_macommande_list": "/espace/particulier/finalize/2/macommande/telecharger",

    // finalize:stage4:information personal
    "finalize_3_information_personal": "/espace/particulier/finalize/3/macommande/info/perso",

    // finalize:stage4:payement
    "finalize_4_paye_ma_commande": "/espace/particulier/finalize/4/macommande/payement",

    // finalize:verify
    "finalize_verify": "/espace/particulier/finalize/5/macommande/termine!",

    // END Finalize Command
    //---------------------------


    // Payment
    //---------------------------

    "payment_braintree_generate_token": "/payement/generate/token/client",

    "payment_braintree_checkout": "/payement/checkout",

    // END Payement
    //---------------------------



    // ----
    // other

    // default:verifyAgeGet
    "validateAge":  "/verify/age",

    // default:contact
    "contact": "/contacter/",

    // default:errorView
    "error": "/err/",

    // test
    test: "/test",
    //---------------------------

    // ADMIN
    //---------------------------

    admin_landing: "/admin/",

    admin_drinks: "/admin/drinks/",

    admin_alcohols: "/admin/alcohols/",

    admin_categories: "/admin/categories/",

    admin_formules: "/admin/formules/",

    admin_parties: "/admin/parties/",

};