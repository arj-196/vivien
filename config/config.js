module.exports = {
  "development": {
    "username": "root",
    "password": "wireless",
    "database": "mrvivien",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "storage": "./session.mysql",
    "logging": false,
    "agent": false
  },

  "test": {
    "username": "root",
    "password": null,
    "database": "mrvivien",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "storage": "./session.mysql",
    "logging": false
  },

  "production": {
    "username": "root",
    "password": null,
    "database": "mrvivien",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "storage": "./session.mysql",
    "logging": false
  }
}
