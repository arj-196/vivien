﻿function formattedPrice(val) {
    var res = "";
    var arr = val.toString().split(".");
    if (arr.length === 1) { res = val.toString() + ",00"; }
    else
    {
        if (arr[1].length === 1) { res = val.toString() + "0"; }
        else { res = arr[0] + "," + arr[1].substr(0, 2); }
    }
    return res;
}
function open_popup(parent, popup) {
    $(parent).css({ "height": "100%", "overflow": "hidden" });
    $(popup).show();
    $(popup).trigger('open');

}
function close_popup(parent, popup) {
    $(parent).css({ "height": "", "overflow": "" });
    $(popup).hide();
    $(popup).trigger('close');
}