var ITERATION = 0
$(document).ready(function () {
    loadDrinks()
    loadDropdownContent()
    $('.list-drink').find('.pagination').find('.trigger').click(paginateDrinksList)
})

var loadDrinks = function () {
    var container = $('#list-drink')
    container.empty()

    API.get('/core/Drink/all/' + ITERATION)
        .then(
            function (rd) {
                var drinkInformationToFetch = []
                rd.r.forEach(function (Drink) {
                    drinkInformationToFetch.push(
                        getDrinkInformation(Drink)
                    )
                })
                Promise.all(drinkInformationToFetch)
                    .then(function (drinkInformation) {
                        for (var i = 0; i < drinkInformation.length; i++) {
                            var Drink = rd.r[i]
                            var Price = drinkInformation[i].price
                            var alcohols = drinkInformation[i].alcohols
                            var categories = drinkInformation[i].categories
                            var categoryString = ""
                            categories.forEach(function (Category) {
                                categoryString += TEMPLATES.DRINK.LIST.ALCOHOL
                                    .replace('$NAME$', Category.name)
                            })
                            var alcoholString = ""
                            alcohols.forEach(function (Alcohol) {
                                alcoholString += TEMPLATES.DRINK.LIST.ALCOHOL
                                    .replace('$NAME$', Alcohol.name)
                            })
                            var SEPDATE = '-'
                            var createdAt = new Date(Drink.createdAt)
                            var createdAtString = createdAt.getDate() + SEPDATE + (createdAt.getMonth() + 1) + SEPDATE + createdAt.getFullYear()
                            var updatedAt = new Date(Drink.updatedAt)
                            var updatedAtString = updatedAt.getDate() + SEPDATE + (updatedAt.getMonth() + 1) + SEPDATE + updatedAt.getFullYear()

                            container.append(
                                TEMPLATES.DRINK.LIST.ITEM
                                    .replace('$DRINKNAME$', Drink.name)
                                    .replace('$PRICE$', Price.price)
                                    .replace('$ALCOHOLS$', alcoholString)
                                    .replace('$CATEGORIES$', categoryString)
                                    .replace('$CREATEDAT$', createdAtString)
                                    .replace('$UPDATEDAT$', updatedAtString)
                                    .replace('$DATA$',
                                        "data-drinkid='" + Drink.id + "'"
                                    )
                            )
                        }

                        //// define height at initialization
                        //console.log(container.data())
                        //if(_.isUndefined(container.data().setheight)){
                        //    container.css({
                        //        height: container.height() + 'px'
                        //    })
                        //    container.data('setheight', false)
                        //}
                        //console.log(container.data())

                        showDrink(null, {drinkid: rd.r[0].id})
                    })
            }
        )
}

var paginateDrinksList = function () {
    var self = $(this)
    if (ITERATION > -1) {
        if (self.data().direction == 'next') {
            ITERATION++
            loadDrinks()
        } else {
            if (ITERATION != 0) {
                ITERATION--
                loadDrinks()
            }
        }
    }
}

var showDrink = function (o, data) {
    var container = $('#detail-drink')
    container.parent().dimmer('show')

    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }
    getDrink(data.drinkid)
        .then(function (r) {
            var Drink = r.r.rows[0]
            getDrinkInformation(Drink)
                .then(function (r) {
                    var Price = r.price
                    var alcohols = r.alcohols
                    var categories = r.categories
                    // format data
                    var idstr = "drink-" + Drink.id
                    container.empty()
                    container.append(
                        TEMPLATES.DRINK.DETAILS.CONTAINER
                            .replace('$ID$', idstr)
                            .replace('$NAME$', Drink.name)
                            .replace('$DESC$', Drink.description)
                            .replace('$PRICE$', Price.price)
                            .replace('$IMG$', Drink.img)
                            .replace('$DATA$', "data-drinkid='" + Drink.id + "'")
                    )
                    // alcohols
                    container = $('.list-alcohol')
                    alcohols.forEach(function (Alcohol) {
                        container.append(
                            TEMPLATES.DRINK.DETAILS.ALCOHOL.ITEM
                                .replace('$NAME$', Alcohol.name)
                        )
                    })
                    // categories
                    container = $('.list-category')
                    categories.forEach(function (Category) {
                        container.append(
                            TEMPLATES.DRINK.DETAILS.ALCOHOL.ITEM
                                .replace('$NAME$', Category.name)
                        )
                    })
                    $('#detail-drink').parent().dimmer('hide')
                })
        })
}

var getDrink = function (drinkid) {
    return new Promise(function (resolve, reject) {
        API.get('/core/Drink/' + drinkid)
            .then(function (r) {
                resolve(r)
            })
    })
}

var getDrinkInformation = function (Drink) {
    return new Promise(function (resolve, reject) {
        API.get('/core/Price/' + Drink.PriceId)
            .then(function (rp) {
                var Price = null
                if (rp.r.count == 1)
                    Price = rp.r.rows[0]

                API.get('/v/alcoholandcategories/by/drink/' + Drink.id)
                    .then(function (rac) {
                        resolve({price: Price, alcohols: rac.alcohols, categories: rac.categories})
                    })
            })
    })
}

var loadDropdownContent = function () {
    // categories
    API.get('/core/Category/all/')
        .then(function (r) {
            var container = $('select[name="categories"]')
            r.r.rows.forEach(function (Category) {
                container.append(
                    TEMPLATES.DRINK.FORM.SELECT.ITEM
                        .replace('$NAME$', Category.name)
                        .replace('$VALUE$', Category.id)
                )
            })
        })

    // alcohols
    API.get('/core/Alcohol/all/')
        .then(function (r) {
            var container = $('select[name="alcohols"]')
            r.r.rows.forEach(function (Alcohol) {
                container.append(
                    TEMPLATES.DRINK.FORM.SELECT.ITEM
                        .replace('$NAME$', Alcohol.name)
                        .replace('$VALUE$', Alcohol.id)
                )
            })
        })
}

var deleteDrink = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }

    if(confirm("Are you sure you want to delete this Drink ?")){
        data.action = "delete"
        API.post('/edit/drink', data)
            .then(function (r) {
                self.parent().parent().slideUp()
            })
    }
}

var editDrink = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.data()
    }
    getDrink(data.drinkid)
        .then(function (r) {
            if (r.r.count == 1) {
                var container = $('#form-drink')
                var Drink = r.r.rows[0]
                getDrinkInformation(Drink)
                    .then(function (r) {
                        var Price = r.price
                        var alcohols = r.alcohols
                        var categories = r.categories

                        // fill data
                        container.find('[name="action"]')
                            .val("edit")

                        container.find('[name="drinkid"]')
                            .val(Drink.id)

                        container.find('[name="name"]')
                            .val(Drink.name)

                        container.find('[name="description"]')
                            .val(Drink.description)

                        container.find('[name="saveur"]')
                            .val(Drink.saveur)

                        container.find('[name="price"]')
                            .val(Price.price)

                        var alcoholIDS = _.map(_.pluck(alcohols, 'id'), function (d) {
                            return d.toString()
                        })
                        container.find('[name="alcohols"]')
                            .dropdown('set exactly', alcoholIDS)

                        var categoryIDS = _.map(_.pluck(categories, 'id'), function (d) {
                            return d.toString()
                        })
                        container.find('[name="categories"]')
                            .dropdown('set exactly', categoryIDS)

                        $('#modal-drink')
                            .modal('setting', 'closable', false)
                            .modal('show')
                        ;
                    })
            }
        })
}

var createDrink = function () {
    var container = $('#form-drink')
    // remove data
    container.find('[name="action"]')
        .val("new")

    container.find('[name="drinkid"]')
        .val(null)

    container.find('[name="name"]')
        .val(null)

    container.find('[name="description"]')
        .val(null)

    container.find('[name="saveur"]')
        .val(null)

    container.find('[name="price"]')
        .val(null)

    container.find('[name="alcohols"]')
        .dropdown('set exactly', [])

    container.find('[name="categories"]')
        .dropdown('set exactly', [])

    $('#modal-drink')
        .modal('setting', 'closable', false)
        .modal('show')
    ;

}

var submitFormDrink = function () {
    var data = $('#form-drink').form('get values')
    var isValid = true, message = ""
    // validation
    if (data.name.trim() == "") {
        isValid = false
        message = "Name cannot be empty"
    }

    if (data.description.trim() == "" || data.description.split(" ").length < 10) {
        isValid = false
        message = "Description has to be atleast 10 words long"
    }

    if (data.saveur.trim() == "") {
        isValid = false
        message = "Saveur cannot be empty"
    }

    if (_.isNaN(parseFloat(data.price))) {
        isValid = false
        message = "Price must be a valid number"
    }

    if (_.isNull(data.alcohols)) {
        isValid = false
        message = "you must select atleast one alcohol"
    }

    if (_.isNull(data.categories)) {
        isValid = false
        message = "you must select atleast one category"
    }

    if (isValid) {
        API.post('/edit/drink', data)
            .then(function (r) {
                showDrink(null, data)
                loadDrinks()
                $('#modal-drink')
                    .modal('hide')

            })

    } else {
        alert(message)
    }

}
