var TEMPLATES = {


    DRINK: {

        LIST: {
            ITEM: '<tr> ' +
            '    <td>$DRINKNAME$</td> ' +
            '    <td>$ALCOHOLS$</td> ' +
            '    <td>$CATEGORIES$</td> ' +
            '    <td>$PRICE$ <i class="euro icon"></i></td> ' +
            '    <td>$UPDATEDAT$</td> ' +
            '    <td>$CREATEDAT$</td> ' +
            '    <td $DATA$>' +
            '       <i class="search icon cursor" onclick="showAlcohol(this)"></i>' +
            '       <i class="minus square icon red cursor" onclick="deleteDrink(this)"></i>' +
            '   </td> ' +
            '</tr> '
            ,

            ALCOHOL: '$NAME$' +
            '<i class="ellipsis vertical icon" style="margin-right:10px;margin-left:10px;"></i>'
        }
        ,

        DETAILS: {
            CONTAINER: '<div id="$ID$" class="column eight wide" style="padding:40px;"> ' +
            '    <div class="ui middle aligned divided list" style="font-size:16px;"> ' +
            '        <div class="item"> ' +
            '            <div class="right floated content"> ' +
            '                <div class="ui button" onclick="editDrink(this)" $DATA$> ' +
            '                    <i class="write icon" style="margin:0;"></i> ' +
            '                </div> ' +
            '            </div> ' +
            '            <div class="content"> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> name </label>' +
            '            <div class="content"> ' +
            '                $NAME$ ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> description </label>' +
            '            <div class="content" style="padding-right:70px;"> ' +
            '                $DESC$ ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> price </label>' +
            '            <div class="content"> ' +
            '                $PRICE$ <i class="euro icon"></i> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> categories </label>' +
            '            <div class="content"> ' +
            '                <div class="ui list list-category"> ' +
            '                </div> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> alcohols </label>' +
            '            <div class="content"> ' +
            '                <div class="ui list list-alcohol"> ' +
            '                </div> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '</div> ' +
            '<div class="column eight wide"> ' +
            '    <a class="ui medium image right floated"> ' +
            '        <img src="/content/img/drinks/$IMG$"> ' +
            '    </a> ' +
            '</div> '
            ,

            ALCOHOL: {
                ITEM: '<div class="item" $DATA$>$NAME$ </div>'
                ,
            }
        },

        FORM: {

            SELECT: {
                ITEM: '<option value="$VALUE$">$NAME$</option>'
                ,
            }
        }
    },

    ALCOHOL: {

        LIST: {
            ITEM: '<tr> ' +
            '    <td>$NAME$</td> ' +
            '    <td>$TYPE$</td> ' +
            '    <td>$BRAND$</td> ' +
            '    <td>$PRICE$ <i class="euro icon"></i></td> ' +
            '    <td>$UPDATEDAT$</td> ' +
            '    <td>$CREATEDAT$</td> ' +
            '    <td $DATA$>' +
            '       <i class="search icon cursor" onclick="showAlcohol(this)"></i>' +
            '       <i class="minus square icon red cursor" onclick="deleteAlcohol(this)"></i>' +
            '   </td> ' +
            '</tr> '
            ,
        }
        ,

        DETAILS: {
            CONTAINER: '<div id="$ID$" class="column eight wide" style="padding:40px;"> ' +
            '    <div class="ui middle aligned divided list" style="font-size:16px;"> ' +
            '        <div class="item"> ' +
            '            <div class="right floated content"> ' +
            '                <div class="ui button" onclick="editDrink(this)" $DATA$> ' +
            '                    <i class="write icon" style="margin:0;"></i> ' +
            '                </div> ' +
            '            </div> ' +
            '            <div class="content"> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> name </label>' +
            '            <div class="content"> ' +
            '                $NAME$ ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> type </label>' +
            '            <div class="content" style="padding-right:70px;"> ' +
            '                $TYPE$ ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> brand </label>' +
            '            <div class="content" style="padding-right:70px;"> ' +
            '                $BRAND$ ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> price </label>' +
            '            <div class="content"> ' +
            '                $PRICE$ <i class="euro icon"></i> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '</div> ' +
            '<div class="column eight wide"> ' +
            '    <a class="ui medium image right floated"> ' +
            '        <img src="$IMG$"> ' +
            '    </a> ' +
            '</div> '
            ,
        }
    },

    CATEGORY: {
        LIST: {
            ITEM: '<tr> ' +
            '    <td>$NAME$</td> ' +
            '    <td>$PRICE$ <i class="euro icon"></i></td> ' +
            '    <td>$UPDATEDAT$</td> ' +
            '    <td>$CREATEDAT$</td> ' +
            '    <td $DATA$>' +
            '       <i class="search icon cursor" onclick="showCategory(this)"></i>' +
            '       <i class="minus square icon red cursor" onclick="deleteCategory(this)"></i>' +
            '   </td> ' +
            '</tr> '
            ,
        }
        ,

        DETAILS: {
            CONTAINER: '<div id="$ID$" class="column sixteen wide" style="padding:40px;"> ' +
            '    <div class="ui middle aligned divided list" style="font-size:16px;"> ' +
            '        <div class="item"> ' +
            '            <div class="right floated content"> ' +
            '                <div class="ui button" onclick="editCategory(this)" $DATA$> ' +
            '                    <i class="write icon" style="margin:0;"></i> ' +
            '                </div> ' +
            '            </div> ' +
            '            <div class="content"> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> name </label>' +
            '            <div class="content"> ' +
            '                $NAME$ ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> description </label>' +
            '            <div class="content" style="padding-right:70px;"> ' +
            '                $DESCRIPTION$ ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> description large </label>' +
            '            <div class="content" style="padding-right:70px;"> ' +
            '                $DESCRIPTIONLARGE$ ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> price </label>' +
            '            <div class="content"> ' +
            '                $PRICE$ <i class="euro icon"></i> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> drinks </label>' +
            '            <div class="content"> ' +
            '                <div class="ui list list-sub-drinks"> ' +
            '                </div> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '</div> '
            ,
        }
    },

    FORMULES: {

        LIST: {
            ITEM: '<tr> ' +
            '    <td>$NAME$</td> ' +
            '    <td>$NBPPL$</td> ' +
            '    <td>$PRICE$ <i class="euro icon"></i></td> ' +
            '    <td>$UPDATEDAT$</td> ' +
            '    <td>$CREATEDAT$</td> ' +
            '    <td $DATA$>' +
            '       <i class="search icon cursor" onclick="showFormule(this)"></i>' +
            '       <i class="minus square icon red cursor" onclick="deleteFormule(this)"></i>' +
            '   </td> ' +
            '</tr> '
            ,
        }
        ,

        DETAILS: {
            CONTAINER: '<div id="$ID$" class="column sixteen wide" style="padding:40px;"> ' +
            '    <div class="ui middle aligned divided list" style="font-size:16px;"> ' +
            '        <div class="item"> ' +
            '            <div class="right floated content"> ' +
            '                <div class="ui button" onclick="editFormule(this)" $DATA$> ' +
            '                    <i class="write icon" style="margin:0;"></i> ' +
            '                </div> ' +
            '            </div> ' +
            '            <div class="content"> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> name </label>' +
            '            <div class="content"> ' +
            '                $NAME$ ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> price </label>' +
            '            <div class="content"> ' +
            '                $PRICE$ <i class="euro icon"></i> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="item" style="position: relative;"> ' +
            '            <label class="" style="font-size:7px;' +
            '                   text-transform:uppercase;color:grey;position:absolute;top:-1px;right:3px;"> items </label>' +
            '            <div class="content"> ' +
            '                <div class="ui list list-formule-items"> ' +
            '                </div> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '</div> '
            ,

            ITEM:
                '<div class="ui piled segment" style="margin:4px;position:relative;">' +
                '   <div style="position:absolute;right:5px;top:10px;display:$DISPLAY$" $DATA$>' +
                '       <i class="minus square icon red cursor" onclick="removeItem(this)"></i>' +
                '   </div>' +
                '   <h4 style="margin:1px;">$NAME$ : $PRICE$ <i class="euro icon"></i></h4>' +
                '   <p>$DESC$</p>' +
                '</div>'
            ,
        }
    },

    PARTY: {
        LIST: {
            ITEM: '<tr> ' +
            '    <td>$FIRSTNAME$</td> ' +
            '    <td>$LASTNAME$</td> ' +
            '    <td>$EMAIL$</td> ' +
            '    <td>$PHONE$</td> ' +
            '    <td>$CITY$</td> ' +
            '    <td>$WHOIAM$</td> ' +
            '    <td>$PAYEMENT$</td> ' +
            '    <td>$PRICE$ <i class="euro icon"></i></td> ' +
            '    <td>$EVENTDAT$</td> ' +
            '    <td>$UPDATEDAT$</td> ' +
            '    <td>$CREATEDAT$</td> ' +
            '    <td $DATA$>' +
            '       <i class="search icon cursor" onclick="showParty(this)"></i>' +
            '       <i class="minus square icon red cursor" onclick="deleteParty(this)"></i>' +
            '   </td> ' +
            '</tr> '
            ,
        },

        DETAILS: {
            CONTAINER:

            '<div id="$ID$" class="column sixteen wide" style="padding:40px;"> ' +
            '    <div class="" style="font-size:16px;"> ' +
            '        <!--<div class="">--> ' +
            '        <!--<div class="right floated content">--> ' +
            '        <!--<div class="ui button" onclick="editParty(this)" $DATA$>--> ' +
            '        <!--<i class="write icon" style="margin:0;"></i>--> ' +
            '        <!--</div>--> ' +
            '        <!--</div>--> ' +
            '        <!--<div class="content">--> ' +
            '        <!--</div>--> ' +
            '        <!--</div>--> ' +
            '        <div class="ui grid"> ' +
            '            <div class="column eight wide"> ' +
            '                <h3 class="ui dividing header"> ' +
            '                    Person ' +
            '                </h3> ' +
            '                <div class="ui middle aligned divided list" style="font-size:16px;"> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">prenom</label> ' +
            '                        <div class="content">$FIRSTNAME$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">nom</label> ' +
            '                        <div class="content">$LASTNAME$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">street</label> ' +
            '                        <div class="content">$STREET$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">city</label> ' +
            '                        <div class="content">$CITY$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">country</label> ' +
            '                        <div class="content">$COUNTRY$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">postal</label> ' +
            '                        <div class="content">$POSTAL$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">iam</label> ' +
            '                        <div class="content">$IAM$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">sex</label> ' +
            '                        <div class="content">$SEX$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">email</label> ' +
            '                        <div class="content">$EMAIL$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">telephone</label> ' +
            '                        <div class="content">$PHONE$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">bday</label> ' +
            '                        <div class="content">$BDAY$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                                color:grey;position:absolute;top:-1px;right:3px;">callme</label> ' +
            '                        <div class="content">$CALLME$</div> ' +
            '                    </div> ' +
            '                </div> ' +
            '            </div> ' +
            '            <div class="column eight wide"> ' +
            '                <h3 class="ui dividing header"> ' +
            '                    Event ' +
            '                </h3> ' +
            '                <div class="ui middle aligned divided list" style="font-size:16px;"> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">theme</label> ' +
            '                        <div class="content">$THEME$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">number of peopel</label> ' +
            '                        <div class="content">$NBPEOPLE$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">hour</label> ' +
            '                        <div class="content">$HOUR$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">date</label> ' +
            '                        <div class="content">$DATE$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">duration</label> ' +
            '                        <div class="content">$DURATION$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">postal</label> ' +
            '                        <div class="content">$POSTAL$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">city</label> ' +
            '                        <div class="content">$CITY$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">country</label> ' +
            '                        <div class="content">$COUNTRY$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">address</label> ' +
            '                        <div class="content">$ADDRESS$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">if Bio</label> ' +
            '                        <div class="content">$IFBIO$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">if Organise</label> ' +
            '                        <div class="content">$IFORGANISE$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">type</label> ' +
            '                        <div class="content">$TYPE$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">place</label> ' +
            '                        <div class="content">$PLACE$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">floor</label> ' +
            '                        <div class="content">$FLOOR$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">lift</label> ' +
            '                        <div class="content">$LIFT$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">preference of barmen</label> ' +
            '                        <div class="content">$PREFBARMAN$</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">nbBarmen</label> ' +
            '                        <div class="content">$NBBARMEN$ barmen</div> ' +
            '                    </div> ' +
            '                    <div class="item" style="position: relative;"> ' +
            '                        <label class="" style="font-size:7px;text-transform:uppercase; ' +
            '                        color:grey;position:absolute;top:-1px;right:3px;">nbCommis</label> ' +
            '                        <div class="content">$NBCOMMIS$ commis</div> ' +
            '                    </div> ' +
            '                </div> ' +
            '            </div> ' +
            '            <div class="column eight wide"> ' +
            '                <h3 class="ui dividing header"> ' +
            '                    Drinks ' +
            '                </h3> ' +
            '                <div class="ui middle aligned divided list" id="list-drink-category"> ' +
            '                </div> ' +
            '            </div> ' +
            '            <div class="column eight wide"> ' +
            '                <h3 class="ui dividing header"> ' +
            '                    Services ' +
            '                </h3> ' +
            '                <div class="ui middle aligned divided list" id="list-services"> ' +
            '                </div> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '</div> '
            ,

            ITEM: {
                DRINK: {
                    CATEGORY:
                    '<div class="item"> ' +
                    '    <i class="bar icon"></i> ' +
                    '    <div class="content"> ' +
                    '        <div class="header">$CATEGORY$</div> ' +
                    '        <div class="description"></div> ' +
                    '        <div class="list" id="$ID$"> ' +
                    '        </div> ' +
                    '    </div> ' +
                    '</div> '
                    ,
                    DRINK:
                    '<div class="item" style="margin-bottom:3px;"> ' +
                    '    <i class="chevron right middle aligned icon"></i> ' +
                    '    <div class="content"> ' +
                    '        <div class="description">$DESCRIPTION$</div> ' +
                    '        <div class="header">$QUANTITY$ : $PRICE$<i class="euro icon"></i></div> ' +
                    '    </div> ' +
                    '</div> '
                    ,
                },

                ICE: {
                    CATEGORY:
                    '<div class="item"> ' +
                    '    <i class="cube icon"></i> ' +
                    '    <div class="content"> ' +
                    '        <div class="header">$CATEGORY$</div> ' +
                    '        <div class="description"></div> ' +
                    '        <div class="list" id="$ID$"> ' +
                    '        </div> ' +
                    '    </div> ' +
                    '</div> '
                },

                UPGRADE: {
                    CATEGORY:
                    '<div class="item"> ' +
                    '    <i class="empty star icon"></i> ' +
                    '    <div class="content"> ' +
                    '        <div class="header">$CATEGORY$</div> ' +
                    '        <div class="description"></div> ' +
                    '        <div class="list" id="$ID$"> ' +
                    '        </div> ' +
                    '    </div> ' +
                    '</div> '
                },

                SERVICE: {
                    CATEGORY:
                    '<div class="item"> ' +
                    '    <i class="archive icon"></i> ' +
                    '    <div class="content"> ' +
                    '        <div class="header">$CATEGORY$</div> ' +
                    '        <div class="description"></div> ' +
                    '        <div class="list" id="$ID$"> ' +
                    '        </div> ' +
                    '    </div> ' +
                    '</div> '
                    ,
                    SERVICE:
                    '<div class="item item-service" style="margin-bottom:3px;" $DATA$> ' +
                    '    <i class="chevron right middle aligned icon"></i> ' +
                    '    <div class="content"> ' +
                    '        <div class="description">$DESCRIPTION$ : $ADD$</div> ' +
                    '        <div class="header">$QUANTITY$ : $PRICE$<i class="euro icon"></i></div> ' +
                    '    </div> ' +
                    '</div> '
                    ,
                }
            }
            ,
        },
    }
}