var ITERATION = 0
$(document).ready(function () {
    loadFormules()
    loadDropdownContent()
    $('.list-formule').find('.pagination').find('.trigger').click(paginateDrinksList)
})

var loadFormules = function () {
    var container = $('#list-formule')
    container.empty()

    API.get('/core/Formule/all/' + ITERATION)
        .then(
            function (rd) {
                var formuleInformationToFetch = []
                rd.r.forEach(function (Formule) {
                    formuleInformationToFetch.push(
                        getFormuleInformation(Formule)
                    )
                })
                Promise.all(formuleInformationToFetch)
                    .then(function (formuleInformation) {
                        for (var i = 0; i < formuleInformation.length; i++) {
                            var Formule = rd.r[i]
                            var Price = formuleInformation[i].price

                            var SEPDATE = '-'
                            var createdAt = new Date(Formule.createdAt)
                            var createdAtString = createdAt.getDate() + SEPDATE + (createdAt.getMonth() + 1) + SEPDATE + createdAt.getFullYear()
                            var updatedAt = new Date(Formule.updatedAt)
                            var updatedAtString = updatedAt.getDate() + SEPDATE + (updatedAt.getMonth() + 1) + SEPDATE + updatedAt.getFullYear()

                            container.append(
                                TEMPLATES.FORMULES.LIST.ITEM
                                    .replace('$NAME$', Formule.name)
                                    .replace('$PRICE$', Price.price)
                                    .replace('$NBPPL$', Formule.nbPpl)
                                    .replace('$CREATEDAT$', createdAtString)
                                    .replace('$UPDATEDAT$', updatedAtString)
                                    .replace('$DATA$',
                                        "data-formuleid='" + Formule.id + "'"
                                    )
                            )
                        }
                        showFormule(null, {formuleid: rd.r[0].id})
                    })
            }
        )
}

var paginateDrinksList = function () {
    var self = $(this)
    if (ITERATION > -1) {
        if (self.data().direction == 'next') {
            ITERATION++
            loadFormules()
        } else {
            if (ITERATION != 0) {
                ITERATION--
                loadFormules()
            }
        }
    }
}

var showFormule = function (o, data) {
    var container = $('#detail-formule')
    container.parent().dimmer('show')
    $('.form-formule-item-list').empty()

    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }
    getFormule(data.formuleid)
        .then(function (r) {
            var Formule = r.r.rows[0]
            getFormuleInformation(Formule)
                .then(function (r) {
                    var Price = r.price
                    var items = r.items
                    var prices = r.prices

                    // format data
                    container.empty()
                    container.append(
                        TEMPLATES.FORMULES.DETAILS.CONTAINER
                            .replace('$NAME$', Formule.name)
                            .replace('$DESC$', Formule.description)
                            .replace('$PRICE$', Price.price)
                            .replace('$DATA$', "data-formuleid='" + Formule.id + "'")
                    )

                    container = $('.list-formule-items')
                    for (var i = 0; i < items.length; i++) {
                        var Item = items[i]
                        var P = prices[i]
                        container.append(
                            TEMPLATES.FORMULES.DETAILS.ITEM
                                .replace('$NAME$', Item.name)
                                .replace('$PRICE$', P.price)
                                .replace('$DESC$', Item.description)
                                .replace('$DISPLAY$', 'none')
                        )
                    }
                    $('#detail-formule').parent().dimmer('hide')
                })
        })
}

var getFormule = function (formuleid) {
    return new Promise(function (resolve, reject) {
        API.get('/core/Formule/' + formuleid)
            .then(function (r) {
                resolve(r)
            })
    })
}

var getFormuleInformation = function (Formule) {
    return new Promise(function (resolve, reject) {
        API.get('/v/formuleitems/' + Formule.id)
            .then(function (rac) {
                resolve({price: rac.price, items: rac.items, prices: rac.prices})
            })
    })
}

var loadDropdownContent = function () {
}

var deleteFormule = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }

    if (confirm("Are you sure you want to delete this Drink ?")) {
        data.action = "delete"
        API.post('/edit/formule', data)
            .then(function (r) {
                self.parent().parent().slideUp()
            })
    }
}

var editFormule = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.data()
    }
    getFormule(data.formuleid)
        .then(function (r) {
            if (r.r.count == 1) {
                var container = $('#form-formule')
                var Formule = r.r.rows[0]
                getFormuleInformation(Formule)
                    .then(function (r) {
                        var Price = r.price
                        var items = r.items
                        var prices = r.prices

                        // fill data
                        container.find('[name="action"]')
                            .val("edit")

                        container.find('[name="formuleid"]')
                            .val(Formule.id)

                        container.find('[name="name"]')
                            .val(Formule.name)

                        container.find('[name="nbppl"]')
                            .val(Formule.nbPpl)

                        container = $('.form-formule-item-list')
                        for (var i = 0; i < items.length; i++) {
                            var Item = items[i]
                            var P = prices[i]
                            container.append(
                                TEMPLATES.FORMULES.DETAILS.ITEM
                                    .replace('$NAME$', Item.name)
                                    .replace('$PRICE$', P.price)
                                    .replace('$DESC$', Item.description)
                                    .replace('$DATA$', 'data-formuleid="' + Formule.id + '" data-itemid="' + Item.id + '"')
                            )
                        }

                        $('#modal-formule')
                            .modal('setting', 'closable', false)
                            .modal('show')
                        ;
                    })
            }
        })
}

var addItem = function () {
    var container = $('#form-formule')
    container.find('[name="action"]')
        .val("newitem")
    var data = container.form('get values')
    var isValid = true, message = ""

    // validation
    if (data.name.trim() == "") {
        isValid = false
        message = "Name cannot be empty"
    }

    if (data.itemname.trim() == "") {
        isValid = false
        message = "Item Name cannot be empty"
    }

    if (data.itemdesc.trim() == "") {
        isValid = false
        message = "Item Description cannot be empty"
    }

    if (_.isNaN(parseFloat(data.itemprice))) {
        isValid = false
        message = "Item Price must be a valid number"
    }

    if (isValid) {
        API.post('/edit/formule', data)
            .then(function (r) {
                // TODO add item after added
                console.log("response add ITEM", r)
                container = $('.form-formule-item-list')
                //container.append(
                //    TEMPLATES.FORMULES.DETAILS.ITEM
                //        .replace('$NAME$', Item.name)
                //        .replace('$PRICE$', P.price)
                //        .replace('$DESC$', Item.description)
                //        .replace('$DATA$', 'data-formuleid="' + Formule.id + '" data-itemid="' + Item.id + '"')
                //)
            })

    } else {
        alert(message)
    }
}

var removeItem = function (o) {
    var self = $(o)
    var data = self.parent().data()
    data.action = "deleteitem"
    API.post('/edit/formule', data)
        .then(function (r) {
            self.parent().parent().slideUp()
        })
}

var createFormule = function () {

    var data = {
        action: "new"
    }
    API.post('/edit/formule', data)
        .then(function (Formule) {
            console.log("response new formule", Formule)

            var container = $('#form-formule')
            // remove data
            container.find('[name="action"]')
                .val("edit")

            container.find('[name="formuleid"]')
                .val(Formule.id)

            container.find('[name="name"]')
                .val(null)
            container.find('[name="nbppl"]')
                .val(null)
            container.find('[name="itemname"]')
                .val(null)
            container.find('[name="itemdesc"]')
                .val(null)
            container.find('[name="itemprice"]')
                .val(null)

            $('.form-formule-item-list').empty()

            $('#modal-formule')
                .modal('setting', 'closable', false)
                .modal('show')

        })

}

var submitFormFormule = function () {
    var data = $('#form-formule').form('get values')
    var isValid = true, message = ""
    // validation
    if (data.name.trim() == "") {
        isValid = false
        message = "Name cannot be empty"
    }

    if (_.isNaN(data.nbppl)) {
        isValid = false
        message = "Number of people has to be a valid number"
    }

    if (isValid) {
        API.post('/edit/formule', data)
            .then(function (r) {
                showFormule(null, data)
                loadFormules()
                $('#modal-formule')
                    .modal('hide')

            })

    } else {
        alert(message)
    }

}
