// TODO type glasses

var ITERATION = 0
$(document).ready(function () {
    loadParties()
    loadDropdownContent()
    $('.list-drink').find('.pagination').find('.trigger').click(paginateDrinksList)
})

var loadParties = function () {
    var container = $('#list-drink')
    container.empty()

    API.get('/core/Party/all/' + ITERATION)
        .then(
            function (rd) {
                var partyInformationToFetch = []
                rd.r.forEach(function (Alcohol) {
                    partyInformationToFetch.push(
                        getPartyInformation(Alcohol)
                    )
                })
                Promise.all(partyInformationToFetch)
                    .then(function (partyInformation) {
                        for (var i = 0; i < partyInformation.length; i++) {
                            try {
                                var Party = rd.r[i]
                                var Person = _.isNull(Party.Person) ? {} : Party.Person
                                var Event = _.isNull(Party.Event) ? {} : Party.Event
                                var price = partyInformation[i].price
                                var SEPDATE = '/'
                                var createdAt = new Date(Party.createdAt)
                                var createdAtString = createdAt.getDate() + SEPDATE + (createdAt.getMonth() + 1) + SEPDATE + createdAt.getFullYear()
                                var updatedAt = new Date(Party.updatedAt)
                                var updatedAtString = updatedAt.getDate() + SEPDATE + (updatedAt.getMonth() + 1) + SEPDATE + updatedAt.getFullYear()
                                //var eventAt = new Date(Event.date)
                                //var eventAtString = eventAt.getDate() + SEPDATE + (eventAt.getMonth() + 1) + SEPDATE + eventAt.getFullYear()

                                container.append(
                                    TEMPLATES.PARTY.LIST.ITEM
                                        .replace('$FIRSTNAME$', Person.prenom)
                                        .replace('$LASTNAME$', Person.nom)
                                        .replace('$EMAIL$', Person.email)
                                        .replace('$PHONE$', Person.telephone)
                                        .replace('$CITY$', Event.city)
                                        .replace('$WHOIAM$', Person.iam)
                                        .replace('$PAYEMENT$', Party.payement)
                                        .replace('$PRICE$', price)
                                        .replace('$EVENTDAT$', Event.date)
                                        .replace('$CREATEDAT$', createdAtString)
                                        .replace('$UPDATEDAT$', updatedAtString)
                                        .replace('$DATA$',
                                            "data-partyid='" + Party.id + "'"
                                        )
                                )
                            } catch (err){
                                console.error(err)
                            }
                        }
                        showParty(null, {partyid: rd.r[0].id})
                    })
            }
        )
}

var paginateDrinksList = function () {
    var self = $(this)
    if (ITERATION > -1) {
        if (self.data().direction == 'next') {
            ITERATION++
            loadAlcohols()
        } else {
            if (ITERATION != 0) {
                ITERATION--
                loadAlcohols()
            }
        }
    }
}

var showParty = function (o, data) {
    var container = $('#detail-drink')
    container.parent().dimmer('show')

    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }
    getParty(data.partyid)
        .then(function (Party) {
            getPartyInformation(Party)
                .then(function (r) {
                    var price = r.price
                    var CMD = r.cmd
                    var Person = _.isNull(Party.Person) ? {} : Party.Person
                    var Event = _.isNull(Party.Event) ? {} : Party.Event
                    var Theme = _.isNull(r.theme) ? {} : r.theme

                    console.log(Party)
                    console.log(r)

                    // format data
                    var idstr = "party-" + Party.id
                    container.empty()
                    container.append(
                        TEMPLATES.PARTY.DETAILS.CONTAINER
                            //.replace('$PAYEMENT$', Party.payement)
                            .replace('$DATA$', "data-partyid='" + Party.id + "'")

                            // Person
                            .replace('$FIRSTNAME$', Person.prenom)
                            .replace('$LASTNAME$', Person.nom)
                            .replace('$STREET$', Person.street)
                            .replace('$EMAIL$', Person.email)
                            .replace('$CITY$', Event.city)
                            .replace('$COUNTRY$', Event.country)
                            .replace('$POSTAL$', Event.postal)
                            .replace('$PHONE$', Person.telephone)
                            .replace('$IAM$', Person.iam)  // TODO format
                            .replace('$SEX$', Person.sex)
                            .replace('$BDAY$', Person.bday)
                            .replace('$CALLME$', Person.callme)  // TODO format

                            // Event
                            .replace('$THEME$', Theme.name)
                            .replace('$NBPEOPLE$', Event.nbPeople)
                            .replace('$HOUR$', Event.hour)
                            .replace('$DATE$', Event.date)
                            .replace('$DURATION$', Event.duration)
                            .replace('$POSTAL$', Event.postal)
                            .replace('$CITY$', Event.city)
                            .replace('$COUNTRY$', Event.country)
                            .replace('$ADDRESS$', Event.address)
                            .replace('$IFBIO$', Event.ifBio)
                            .replace('$IFORGANISE$', Event.ifOrganised)  // TODO format
                            .replace('$TYPE$', Event.type)
                            .replace('$PLACE$', Event.place)
                            .replace('$FLOOR$', Event.floor)
                            .replace('$LIFT$', Event.lift)
                            .replace('$PREFBARMAN$', Party.barmanPref)
                            .replace('$NBBARMEN$', Event.nbBarmen)
                            .replace('$NBCOMMIS$', Event.nbCommis)
                    )

                    // drinks
                    container = $('#list-drink-category')
                    CMD.Drinks.forEach(function (Category, index) {
                        var idStr = 'drink-category-' + index
                        container.append(
                            TEMPLATES.PARTY.DETAILS.ITEM.DRINK.CATEGORY
                                .replace('$CATEGORY$', Category.d)
                                .replace('$ID$', idStr)
                        )
                        var list = $('#' + idStr)
                        Category.drinks.forEach(function (Drink) {
                            list.append(
                                TEMPLATES.PARTY.DETAILS.ITEM.DRINK.DRINK
                                    .replace('$DESCRIPTION$', Drink.d)
                                    .replace('$QUANTITY$', Drink.a)
                                    .replace('$PRICE$', Drink.p)
                            )
                        })

                        // ice
                        if(Category.ice.length > 0){
                            idStr = 'ice-category-' + index
                            container.append(
                                TEMPLATES.PARTY.DETAILS.ITEM.ICE.CATEGORY
                                    .replace('$CATEGORY$', Category.d + " : ICE")
                                    .replace('$ID$', idStr)
                            )

                            list = $('#' + idStr)
                            Category.ice.forEach(function (Ice) {
                                list.append(
                                    TEMPLATES.PARTY.DETAILS.ITEM.DRINK.DRINK
                                        .replace('$DESCRIPTION$', Ice.d)
                                        .replace('$QUANTITY$', Ice.a)
                                        .replace('$PRICE$', Ice.p)
                                )
                            })
                        }

                        // upgrade
                        if(Category.upgrade){
                            container.append(
                                TEMPLATES.PARTY.DETAILS.ITEM.UPGRADE.CATEGORY
                                    .replace('$CATEGORY$', Category.d + " : upgrade Glasses : YES")
                            )
                        }
                    })

                    // services
                    container = $('#list-services')
                    _.keys(CMD.Services).forEach(function (ServiceType) {
                        var idStr = 'service-' + ServiceType
                        container.append(
                            TEMPLATES.PARTY.DETAILS.ITEM.SERVICE.CATEGORY
                                .replace('$CATEGORY$', ServiceType)
                                .replace('$ID$', idStr)
                        )

                        var list = $('#' + idStr)
                        CMD.Services[ServiceType].forEach(function (Service) {
                            var dataString = ""
                            _.keys(Service.o).forEach(function (prop) {
                                dataString += " data-" + prop + "='" + Service.o[prop] + "' "
                            })

                            list.append(
                                TEMPLATES.PARTY.DETAILS.ITEM.SERVICE.SERVICE
                                    .replace('$DESCRIPTION$', Service.d)
                                    .replace('$QUANTITY$', Service.a)
                                    .replace('$PRICE$', Service.p)
                                    .replace('$DATA$', dataString)
                            )
                        })

                    })
                    fetch_additional_service_information()

                    // finally
                    $('#detail-drink').parent().dimmer('hide')
                })
        })
}

function fetch_additional_service_information() {
    $.each($('div[data-body]'), function () {
        var self = $(this)
        PlatForm.getAdditionalServiceInformation({data: JSON.stringify(self.data())}, function (r) {
            var text = self.find('.description').text()
            if (r.drinks.length > 0) {
                text = text.replace('$ADD$', r.names)
            } else {
                text =  text.replace('$ADD$', '')
            }
            self.find('.description').text(text)
        })
    })
}

var getParty = function (partyid) {
    return new Promise(function (resolve, reject) {
        API.get('/v/aparty/' + partyid)
            .then(function (r) {
                resolve(r)
            })
    })
}

var getPartyInformation = function (Party) {
    return new Promise(function (resolve, reject) {
        API.get('/v/partyinfo/' + Party.id)
            .then(function (rp) {

                resolve({price: rp.cmd.Total, cmd: rp.cmd, theme: rp.theme})
            })
    })
}

var loadDropdownContent = function () {
}

var deleteParty = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }

    //if(confirm("Are you sure you want to delete this Alcohol ?")){
    //    data.action = "delete"
    //    API.post('/edit/alcohol', data)
    //        .then(function (r) {
    //            self.parent().parent().slideUp()
    //        })
    //}
}

// TODO

var editDrink = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.data()
    }
    getParty(data.alcoholid)
        .then(function (r) {
            if (r.r.count == 1) {
                var container = $('#form-alcohol')
                var Alcohol = r.r.rows[0]
                getAlcoholInformation(Alcohol)
                    .then(function (r) {
                        var Price = r.price

                        // fill data
                        container.find('[name="action"]')
                            .val("edit")

                        container.find('[name="alcoholid"]')
                            .val(Alcohol.id)

                        container.find('[name="name"]')
                            .val(Alcohol.name)

                        container.find('[name="type"]')
                            .val(Alcohol.type)

                        container.find('[name="brand"]')
                            .val(Alcohol.brand)

                        container.find('[name="price"]')
                            .val(Price.price)

                        $('#modal-alcohol')
                            .modal('setting', 'closable', false)
                            .modal('show')
                        ;
                    })
            }
        })
}

var createParty = function () {
    var container = $('#form-alcohol')
    // remove data
    container.find('[name="action"]')
        .val("new")

    container.find('[name="alcoholid"]')
        .val(null)

    container.find('[name="name"]')
        .val(null)

    container.find('[name="type"]')
        .val(null)

    container.find('[name="brand"]')
        .val(null)

    container.find('[name="price"]')
        .val(null)


    $('#modal-alcohol')
        .modal('setting', 'closable', false)
        .modal('show')
    ;

}

var submitFormParty = function () {
    var data = $('#form-alcohol').form('get values')
    var isValid = true, message = ""
    // validation
    if (data.name.trim() == "") {
        isValid = false
        message = "Name cannot be empty"
    }

    if (data.type.trim() == "") {
        isValid = false
        message = "Type cannot be empty"
    }

    if (data.brand.trim() == "") {
        isValid = false
        message = "Brand cannot be empty"
    }

    if (_.isNaN(parseFloat(data.price))) {
        isValid = false
        message = "Price must be a valid number"
    }

    if (isValid) {
        API.post('/edit/alcohol', data)
            .then(function (r) {
                showParty(null, data)
                loadParties()
                $('#modal-alcohol')
                    .modal('hide')

            })

    } else {
        alert(message)
    }

}
