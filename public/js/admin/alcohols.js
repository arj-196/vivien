var ITERATION = 0
$(document).ready(function () {
    loadAlcohols()
    loadDropdownContent()
    $('.list-drink').find('.pagination').find('.trigger').click(paginateDrinksList)
})

var loadAlcohols = function () {
    var container = $('#list-drink')
    container.empty()

    API.get('/core/Alcohol/all/' + ITERATION)
        .then(
            function (rd) {
                var alcoholInformationToFetch = []
                rd.r.forEach(function (Alcohol) {
                    alcoholInformationToFetch.push(
                        getAlcoholInformation(Alcohol)
                    )
                })
                Promise.all(alcoholInformationToFetch)
                    .then(function (alcoholInformation) {
                        for (var i = 0; i < alcoholInformation.length; i++) {
                            var Alcohol = rd.r[i]
                            var Price = alcoholInformation[i].price
                            var SEPDATE = '-'
                            var createdAt = new Date(Alcohol.createdAt)
                            var createdAtString = createdAt.getDate() + SEPDATE + (createdAt.getMonth() + 1) + SEPDATE + createdAt.getFullYear()
                            var updatedAt = new Date(Alcohol.updatedAt)
                            var updatedAtString = updatedAt.getDate() + SEPDATE + (updatedAt.getMonth() + 1) + SEPDATE + updatedAt.getFullYear()

                            container.append(
                                TEMPLATES.ALCOHOL.LIST.ITEM
                                    .replace('$NAME$', Alcohol.name)
                                    .replace('$PRICE$', Price.price)
                                    .replace('$TYPE$', Alcohol.type)
                                    .replace('$BRAND$', Alcohol.brand)
                                    .replace('$CREATEDAT$', createdAtString)
                                    .replace('$UPDATEDAT$', updatedAtString)
                                    .replace('$DATA$',
                                        "data-alcoholid='" + Alcohol.id + "'"
                                    )
                            )
                        }
                        showAlcohol(null, {alcoholid: rd.r[0].id})
                    })
            }
        )
}

var paginateDrinksList = function () {
    var self = $(this)
    if (ITERATION > -1) {
        if (self.data().direction == 'next') {
            ITERATION++
            loadAlcohols()
        } else {
            if (ITERATION != 0) {
                ITERATION--
                loadAlcohols()
            }
        }
    }
}

var showAlcohol = function (o, data) {
    var container = $('#detail-drink')
    container.parent().dimmer('show')

    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }
    getAlcohol(data.alcoholid)
        .then(function (r) {
            var Alcohol = r.r.rows[0]
            getAlcoholInformation(Alcohol)
                .then(function (r) {
                    var Price = r.price
                    // format data
                    var idstr = "alcohol-" + Alcohol.id
                    container.empty()
                    container.append(
                        TEMPLATES.ALCOHOL.DETAILS.CONTAINER
                            .replace('$ID$', idstr)
                            .replace('$NAME$', Alcohol.name)
                            .replace('$TYPE$', Alcohol.type)
                            .replace('$BRAND$', Alcohol.brand)
                            .replace('$PRICE$', Price.price)
                            .replace('$IMG$', Alcohol.img)
                            .replace('$DATA$', "data-alcoholid='" + Alcohol.id + "'")
                    )
                    $('#detail-drink').parent().dimmer('hide')
                })
        })
}

var getAlcohol = function (alcoholid) {
    return new Promise(function (resolve, reject) {
        API.get('/core/Alcohol/' + alcoholid)
            .then(function (r) {
                resolve(r)
            })
    })
}

var getAlcoholInformation = function (Drink) {
    return new Promise(function (resolve, reject) {
        API.get('/core/Price/' + Drink.PriceId)
            .then(function (rp) {
                var Price = null
                if (rp.r.count == 1)
                    Price = rp.r.rows[0]

                resolve({price: Price})
            })
    })
}

var loadDropdownContent = function () {
}

var deleteAlcohol = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }

    if(confirm("Are you sure you want to delete this Alcohol ?")){
        data.action = "delete"
        API.post('/edit/alcohol', data)
            .then(function (r) {
                self.parent().parent().slideUp()
            })
    }
}

var editDrink = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.data()
    }
    getAlcohol(data.alcoholid)
        .then(function (r) {
            if (r.r.count == 1) {
                var container = $('#form-alcohol')
                var Alcohol = r.r.rows[0]
                getAlcoholInformation(Alcohol)
                    .then(function (r) {
                        var Price = r.price

                        // fill data
                        container.find('[name="action"]')
                            .val("edit")

                        container.find('[name="alcoholid"]')
                            .val(Alcohol.id)

                        container.find('[name="name"]')
                            .val(Alcohol.name)

                        container.find('[name="type"]')
                            .val(Alcohol.type)

                        container.find('[name="brand"]')
                            .val(Alcohol.brand)

                        container.find('[name="price"]')
                            .val(Price.price)

                        $('#modal-alcohol')
                            .modal('setting', 'closable', false)
                            .modal('show')
                        ;
                    })
            }
        })
}

var createAlcohol = function () {
    var container = $('#form-alcohol')
    // remove data
    container.find('[name="action"]')
        .val("new")

    container.find('[name="alcoholid"]')
        .val(null)

    container.find('[name="name"]')
        .val(null)

    container.find('[name="type"]')
        .val(null)

    container.find('[name="brand"]')
        .val(null)

    container.find('[name="price"]')
        .val(null)


    $('#modal-alcohol')
        .modal('setting', 'closable', false)
        .modal('show')
    ;

}

var submitFormDrink = function () {
    var data = $('#form-alcohol').form('get values')
    var isValid = true, message = ""
    // validation
    if (data.name.trim() == "") {
        isValid = false
        message = "Name cannot be empty"
    }

    if (data.type.trim() == "") {
        isValid = false
        message = "Type cannot be empty"
    }

    if (data.brand.trim() == "") {
        isValid = false
        message = "Brand cannot be empty"
    }

    if (_.isNaN(parseFloat(data.price))) {
        isValid = false
        message = "Price must be a valid number"
    }

    if (isValid) {
        API.post('/edit/alcohol', data)
            .then(function (r) {
                showAlcohol(null, data)
                loadAlcohols()
                $('#modal-alcohol')
                    .modal('hide')

            })

    } else {
        alert(message)
    }

}
