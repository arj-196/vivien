var ITERATION = 0
$(document).ready(function () {
    loadCategories()
    loadDropdownContent()
    $('.list-drink').find('.pagination').find('.trigger').click(paginateDrinksList)
})

var loadCategories = function () {
    var container = $('#list-drink')
    container.empty()
    API.get('/core/Category/all/' + ITERATION)
        .then(
            function (rd) {
                var categoryInformationToFetch = []
                rd.r.forEach(function (Category) {
                    categoryInformationToFetch.push(
                        getCategoryInformation(Category)
                    )
                })
                Promise.all(categoryInformationToFetch)
                    .then(function (categoryInformation) {
                        for (var i = 0; i < categoryInformation.length; i++) {
                            var Category = rd.r[i]
                            var Price = categoryInformation[i].price
                            var SEPDATE = '-'
                            var createdAt = new Date(Category.createdAt)
                            var createdAtString = createdAt.getDate() + SEPDATE + (createdAt.getMonth() + 1) + SEPDATE + createdAt.getFullYear()
                            var updatedAt = new Date(Category.updatedAt)
                            var updatedAtString = updatedAt.getDate() + SEPDATE + (updatedAt.getMonth() + 1) + SEPDATE + updatedAt.getFullYear()

                            container.append(
                                TEMPLATES.CATEGORY.LIST.ITEM
                                    .replace('$NAME$', Category.name)
                                    .replace('$PRICE$', Price.price)
                                    .replace('$CREATEDAT$', createdAtString)
                                    .replace('$UPDATEDAT$', updatedAtString)
                                    .replace('$DATA$',
                                        "data-categoryid='" + Category.id + "'"
                                    )
                            )
                        }
                         showCategory(null, {categoryid: rd.r[0].id})
                    })
            }
        )
}

var paginateDrinksList = function () {
    var self = $(this)
    if (ITERATION > -1) {
        if (self.data().direction == 'next') {
            ITERATION++
            loadCategories()
        } else {
            if (ITERATION != 0) {
                ITERATION--
                loadCategories()
            }
        }
    }
}

var showCategory = function (o, data) {
    var container = $('#detail-category')
    container.parent().dimmer('show')

    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }
    getCategory(data.categoryid)
        .then(function (r) {
            var Category = r.r.rows[0]
            getCategoryInformation(Category)
                .then(function (r) {
                    var Price = r.price
                    var drinks = r.drinks
                    // format data
                    var idstr = "alcohol-" + Category.id
                    container.empty()
                    container.append(
                        TEMPLATES.CATEGORY.DETAILS.CONTAINER
                            .replace('$ID$', idstr)
                            .replace('$NAME$', Category.name)
                            .replace('$DESCRIPTION$', Category.description)
                            .replace('$DESCRIPTIONLARGE$', Category.descriptionLarge)
                            .replace('$PRICE$', Price.price)
                            .replace('$DATA$', "data-categoryid='" + Category.id + "'")
                    )
                    container = $('.list-sub-drinks')
                    drinks.forEach(function (Drink) {
                        container.append(
                            TEMPLATES.DRINK.DETAILS.ALCOHOL.ITEM
                                .replace('$NAME$', Drink.name)
                        )
                    })
                    $('#detail-category').parent().dimmer('hide')
                })
        })
}

var getCategory = function (categoryid) {
    return new Promise(function (resolve, reject) {
        API.get('/core/Category/' + categoryid)
            .then(function (r) {
                resolve(r)
            })
    })
}

var getCategoryInformation = function (Category) {
    return new Promise(function (resolve, reject) {
        API.get('/core/Price/' + Category.PriceId)
            .then(function (rp) {
                var Price = null
                if (rp.r.count == 1)
                    Price = rp.r.rows[0]

                API.get('/v/drinks/by/category/' + Category.id)
                    .then(function (rac) {
                        resolve({price: Price, drinks: rac.drinks})
                    })
            })
    })
}

var loadDropdownContent = function () {
    // drinks
    API.get('/core/Drink/all/')
        .then(function (r) {
            var container = $('select[name="drinks"]')
            r.r.rows.forEach(function (Drink) {
                container.append(
                    TEMPLATES.DRINK.FORM.SELECT.ITEM
                        .replace('$NAME$', Drink.name)
                        .replace('$VALUE$', Drink.id)
                )
            })
        })
}

var deleteCategory = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.parent().data()
    }

    if(confirm("Are you sure you want to delete this Category ?")){
        data.action = "delete"
        API.post('/edit/category', data)
            .then(function (r) {
                self.parent().parent().slideUp()
            })
    }
}

var editCategory = function (o, data) {
    if (_.isUndefined(data)) {
        var self = $(o)
        data = self.data()
    }
    getCategory(data.categoryid)
        .then(function (r) {
            if (r.r.count == 1) {
                var container = $('#form-category')
                var Category = r.r.rows[0]
                getCategoryInformation(Category)
                    .then(function (r) {
                        var Price = r.price
                        var drinks = r.drinks

                        // fill data
                        container.find('[name="action"]')
                            .val("edit")

                        container.find('[name="categoryid"]')
                            .val(Category.id)

                        container.find('[name="name"]')
                            .val(Category.name)

                        container.find('[name="desc1"]')
                            .val(Category.description)

                        container.find('[name="desc2"]')
                            .val(Category.descriptionLarge)

                        container.find('[name="price"]')
                            .val(Price.price)

                        var drinkIDS = _.map(_.pluck(drinks, 'id'), function (d) {
                            return d.toString()
                        })
                        container.find('[name="drinks"]')
                            .dropdown('set exactly', drinkIDS)

                        $('#modal-alcohol')
                            .modal('setting', 'closable', false)
                            .modal('show')
                        ;
                    })
            }
        })
}

var createCategoriy = function () {
    var container = $('#form-category')
    // remove data
    container.find('[name="action"]')
        .val("new")

    container.find('[name="categoryid"]')
        .val(null)

    container.find('[name="name"]')
        .val(null)

    container.find('[name="desc1"]')
        .val(null)

    container.find('[name="desc2"]')
        .val(null)

    container.find('[name="price"]')
        .val(null)

    container.find('[name="drinks"]')
        .dropdown('set exactly', [])

    $('#modal-alcohol')
        .modal('setting', 'closable', false)
        .modal('show')
    ;
}

var submitFormCategory = function () {
    var data = $('#form-category').form('get values')
    var isValid = true, message = ""
    // validation
    if (data.name.trim() == "") {
        isValid = false
        message = "Name cannot be empty"
    }

    if (data.desc1.trim() == "" || data.desc1.split(" ").length < 5) {
        isValid = false
        message = "Description 1 has to be atleast 10 words long"
    }

    if (data.desc2.trim() == "" || data.desc2.split(" ").length < 5) {
        isValid = false
        message = "Description has to be atleast 5 words long"
    }

    if (_.isNaN(parseFloat(data.price))) {
        isValid = false
        message = "Price must be a valid number"
    }

    if (_.isNull(data.drink)) {
        data.drink = []
    }

    if (isValid) {
        API.post('/edit/category', data)
            .then(function (r) {
                showCategory(null, data)
                loadCategories()
                $('#modal-alcohol')
                    .modal('hide')

            })
    } else {
        alert(message)
    }

}
