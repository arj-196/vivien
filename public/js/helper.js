var _API = function() {
    this.url = {
        root: '/_api'
    };

    this.get = function(url) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var request =  $.ajax({
                url: self.url.root + '/get' + url,
                method: 'get',
                dataType: 'json'
            });

            request.done(function (r) {
                resolve(r);
            });

            request.fail(function(r) {
                reject('Error get request failed');
            });
        });
    };

    this.post = function(url, data) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var request =  $.ajax({
                url: self.url.root + '/post' + url,
                method: 'post',
                dataType: 'json',
                data: data
            });

            request.done(function (r) {
                resolve(r);
            });

            request.fail(function(r) {
                reject('Error get request failed');
            });
        });

    }

};

var API = new _API();

