var _PlatForm = function() {

    this.maCommandeTotal = function(next) {
        API.get("/user/macommande/price")
            .then(function(r) {
                next(r);
            });
    };

    this.maCommandeDetails = function (next) {
        API.get("/user/macommande/details")
            .then(function (r) {
                next(r);
            });
    }

    this.getUserParty = function(next) {
        API.get("/user/party")
            .then(function(r) {
                next(r);
            })
    };

    this.getModelAll = function (name, next) {
        API.get('/core/' + name + '/all')
            .then(function(r) {
                next(r);
            });
    };

    this.getModelBy = function(name, clause, next) {
        API.post("/core/" + name, clause)
            .then(function(r) {
                console.log("got by", r);
                if(!r)
                    next([]);
                else
                    next(r);
            });
    };

    this.getAlcoholByCategory = function(category, next) {
        API.get("/v/alcohol/by/category/" + category)
            .then(function (r) {
                console.log("got byC", r);
                next(r);
            });
    };

    this.getFixedVariables = function(name, next) {
        this.getVariable(name, next);
    };

    this.getVariable = function (name, next) {
        API.get("/variable/" + name)
            .then(function (r) {
                var list = [];
                for(var i in r){
                    r[i]['id'] = parseInt(i);
                    //r[i]['Price'] = {
                    //    price: Math.floor((Math.random() * 1000) + 1)
                    //};
                    list.push(r[i]);
                }
                console.log("got v", name, list);
                next(list);
            })
            .catch(function(err) {
                console.error(err);
            })
    };

    this.removeCommandItem = function (data, next) {
        API.post("/commande/remove/", data)
            .then(function (r) {
                next(r)
            })
    }

    this.getAdditionalServiceInformation = function(data, next){
        API.post("/service/additional/information", data)
            .then(function (r) {
                next(r)
            })
    }

    this.core = API;
};

var PlatForm = new _PlatForm();