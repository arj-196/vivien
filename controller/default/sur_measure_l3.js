var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var VARIABLES = require('../../config/variables');
var Promise = require('bluebird');
var API = require('../../lib/api/engine'),
    api = new API();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();


module.exports = function (app) {

    // STAGE 1 GET : Choose Animation
    var stage1GetAnimation = function (req, res) {
        res.render(VIEWS.sur_measure_l3_1, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l3_1, stage1GetAnimation);

    // Stage 1 POST
    var stage1PostAnimation = function (req, res) {
        var data = req.body;

        MV.getUserParty(req.models, req.session.id, function (err, Party) {

            // removing re defined services
            var servicesToBeRemoved = [];
            Party.getServices().then(function (services) {
                services.forEach(function (Service) {
                    if(Service.type == "animation") {
                        servicesToBeRemoved.push(Party.removeService(Service))
                    }
                });
            })

            Promise.all(servicesToBeRemoved).then(function () {

                if (data['choix-status'] == 'true') {
                    var animationSet = JSON.parse(data['animation-list']);

                    // create a service for each animation
                    var listOfAnimations = [];
                    Object.keys(animationSet).forEach(function (key) {
                        var animation = animationSet[key];
                        listOfAnimations.push(
                            req.models.Service.create({
                                type: "animation",
                                "meta": animationSet[key].id,
                                "body": {type: key, options: animation},
                                "status": true
                            })
                        );
                    })

                    Promise.all(listOfAnimations).then(function (savedServices) {

                        // add services to party
                        var servicesToAdd = [];
                        savedServices.forEach(function (savedService) {
                            servicesToAdd.push(Party.addService(savedService));
                        });

                        // when all services saved
                        Promise.all(savedServices).then(function () {
                            res.redirect(ROUTES.sur_measure_l3_verify);
                        });
                    });

                } else {
                    res.redirect(ROUTES.sur_measure_l3_verify);
                }

            });
        });
    };
    app.post(ROUTES.sur_measure_l3_1, stage1PostAnimation);

    // STAGE Verify GET :
    var stageVerify = function (req, res) {
        // TODO finish
        res.render(VIEWS.message, {session: req.session, stage: "stage3complete"});
    };
    app.get(ROUTES.sur_measure_l3_verify, stageVerify);


};
