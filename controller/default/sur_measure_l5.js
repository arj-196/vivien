var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var VARIABLES = require('../../config/variables');
var Promise = require('bluebird');
var API = require('../../lib/api/engine'),
    api = new API();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();
var Error = require('../../lib/error/error'),
    error = new Error();


module.exports = function (app) {

    // STAGE 1 GET : Besoins de Renforts
    var stage1GetBesoinsReforts = function (req, res) {
        var replaceString = ""
        var sepActions = ";"
        var sepNameToValue = ":"

        // number drinks
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            Party.Bar.getDrinks()
                .then(function (drinks) {
                    var nbDrinks = 0
                    drinks.forEach(function (Drink) {
                        nbDrinks += Drink.BarDrink.quantity * Party.Event.nbPeople
                    })

                    replaceString += "$NBDRINK$" + sepNameToValue + nbDrinks + sepActions

                    // TODO algo barmen + comis
                    var nbBarmen = 2
                    var nbCommis = 2
                    replaceString += "$NBBARMEN$" + sepNameToValue + nbBarmen + sepActions
                    replaceString += "$NBCOMIS$" + sepNameToValue + nbCommis + sepActions

                    // callback
                    res.render(VIEWS.message, {session: req.session, stage: "besoinsrenforts", replacestring: replaceString});


                    // adding barmen and comis
                    Party.Event.update({
                        nbBarmen: nbBarmen,
                        nbCommis: nbCommis
                    })
                })
        })
    };
    app.get(ROUTES.sur_measure_l5_1, stage1GetBesoinsReforts);

    // STAGE 2 GET : Voulez vous un barman
    var stage2GetVoulezVousBarman = function (req, res) {
        res.render(VIEWS.sur_measure_l5_2, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l5_2, stage2GetVoulezVousBarman);

    // Stage 2 POST
    var stage2PostVoulezVousBarman = function (req, res) {
        var data = req.body;
        if (parseInt(data.choix) === 0) {
            // no barman
            res.render(VIEWS.alert, {session: req.session, stage: "dontunderstand"});
        } else {
            // yes barman
            res.redirect(ROUTES.sur_measure_l5_3);
        }
    };
    app.post(ROUTES.sur_measure_l5_2, stage2PostVoulezVousBarman);

    // STAGE 3 GET : Barman fille ou garcons
    var stage3GetBarmanFilleOuGarcons = function (req, res) {
        res.render(VIEWS.sur_measure_l5_3, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l5_3, stage3GetBarmanFilleOuGarcons);

    // Stage 3 POST
    var stage3PostBarmanFilleOuGarcons = function (req, res) {
        var data = req.body;
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            Party.update({
                barmanPref: data.choix
            }).then(function () {
                res.redirect(ROUTES.sur_measure_l5_4);
            });
        });
    };
    app.post(ROUTES.sur_measure_l5_3, stage3PostBarmanFilleOuGarcons);

    // STAGE 4 GET : Rajouter Personnel
    var stage4GetRajouterPersonnel = function (req, res) {
        res.render(VIEWS.sur_measure_l5_4, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l5_4, stage4GetRajouterPersonnel);

    // Stage 4 POST
    var stage4PostRajouterPersonnel = function (req, res) {
        var data = req.body;
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            Party.update({
                rajoutePersonnel: data.choix
            }).then(function () {
                //res.redirect(ROUTES.sur_measure_l5_5);
                res.redirect(ROUTES.sur_measure_l5_verify);  // TODO pass to stage l5_5
            });
        });

    };
    app.post(ROUTES.sur_measure_l5_4, stage4PostRajouterPersonnel);

    // STAGE 5 GET : Completer Service
    var stage5GetCompleterService = function (req, res) {
        res.render(VIEWS.sur_measure_l5_5, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l5_5, stage5GetCompleterService);

    // STAGE 5 POST
    var stage5PostCompleterService = function (req, res) {
        var data = req.body;
        if(data.noservice != undefined){
            // no service
            res.redirect(ROUTES.sur_measure_l5_verify);
        } else {
            var services = [], service;
            for (var i = 0; i < data.choixservice.length; i++) {
                service = req.models.Service.create({
                    status: false,
                    type: "service_stage5",
                    meta: parseInt(data.choixservice[i]),
                    body: []
                });
                services.push(service);
            }

            Promise.all(services).then(function (savedServices) {
                MV.getUserParty(req.models, req.session.id, function (err, Party) {
                    var servicesToAdd = [];
                    savedServices.forEach(function (savedService) {
                        servicesToAdd.push(Party.addService(savedService));
                    });

                    // when all services saved
                    Promise.all(servicesToAdd).then(function () {
                        res.redirect(ROUTES.sur_measure_l5_6);
                    });
                });
            });
        }
    };
    app.post(ROUTES.sur_measure_l5_5, stage5PostCompleterService);

    // STAGE 6 GET : Completer Service Details
    var stage6GetCompleterServiceDetails = function (req, res) {
        // TODO finish
        error.renderError(
            [__filename].join('/'),
            ROUTES.sur_measure_l5_6,
            "TODO",
            res
        );

        //res.render(VIEWS.sur_measure_l5_6, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l5_6, stage6GetCompleterServiceDetails);


    // STAGE Verify GET
    var stageVerify = function (req, res) {
        // TODO finish
        res.render(VIEWS.message, {session: req.session, stage: "stage5complete"});
    };
    app.get(ROUTES.sur_measure_l5_verify, stageVerify);


};
