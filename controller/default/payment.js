var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var VARIABLES = require('../../config/variables');
var Promise = require('bluebird');
var API = require('../../lib/api/engine'),
    api = new API();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();
var Error = require('../../lib/error/error'),
    error = new Error();
var env = require('../../config/env')
var CommandeGenerator = require('../../lib/mrvivien/commandegenerator');

var braintree = require('braintree')
var gateway = braintree.connect({
    environment: braintree.Environment.Sandbox,
    merchantId: env.braintree.merchaintId,
    publicKey: env.braintree.publicKey,
    privateKey: env.braintree.privateKey
})

module.exports = function (app) {

    var generateBraintreeClientToken = function (req, res) {
        gateway.clientToken.generate({}, function (err, response) {
            res.send(response.clientToken)
        })
    }
    app.get(ROUTES.payment_braintree_generate_token, generateBraintreeClientToken)

    var checkout = function (req, res) {
        var Commande = new CommandeGenerator(req.models, MV, req.session.id);
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            Commande.getMacommande(function (err, macommande) {
                if (err)
                    console.log(err)
                else {
                    var nonce = req.body.payment_method_nonce
                    gateway.transaction.sale({
                        amount: macommande.Total, // TODO enter amount here
                        paymentMethodNonce: nonce,
                    }, function (err, result) {
                        if (err)
                            console.log(err)
                        else {
                            Party.createTransaction({
                                body: result
                            })

                            if(result.success)
                                res.redirect(ROUTES.finalize_verify)
                            else
                                res.redirect(ROUTES.finalize_4_paye_ma_commande)
                        }
                    })
                }
            })
        })
    }
    app.post(ROUTES.payment_braintree_checkout, checkout)
}