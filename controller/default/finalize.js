var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var VARIABLES = require('../../config/variables');
var Promise = require('bluebird');
var API = require('../../lib/api/engine'),
    api = new API();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();
var Error = require('../../lib/error/error'),
    error = new Error();
var CommandeGenerator = require('../../lib/mrvivien/commandegenerator');
var _CommandManager = require('../../lib/mrvivien/commandemanager');
var swig = require('swig');
var path = require('path');
var fs = require('fs');
var pdf = require('html-pdf');
var _ = require('underscore')

module.exports = function (app) {

    // stage1 : GET : offer
    var stage1Getoffer = function (req, res) {
        // TODO finish
        res.render(VIEWS.finalize_1_offer, {session: req.session});
    };
    app.get(ROUTES.finalize_1_offer, stage1Getoffer);

    // stage2 : GET : ma commande
    var stage2GetMaCommande = function (req, res) {
        // TODO finish
        res.render(VIEWS.finalize_2_macommande, {session: req.session});
    };
    app.get(ROUTES.finalize_2_macommande, stage2GetMaCommande);

    // stage2b : GET : download pdf ma commande
    var stage2GetMaCommandeList = function (req, res) {
        var Commande = new CommandeGenerator(req.models, MV, req.session.id);
        var CommandManager = new _CommandManager(req.models, MV, req.session.id);

        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            Commande.getMacommande(function (err, macommande) {
                var infoToLoad = []
                // fetching additional service information
                _.keys(macommande.Services).forEach(function (typeService) {
                    macommande.Services[typeService].forEach(function (Service) {
                        Service.o.body = JSON.parse(Service.o.body)
                        infoToLoad.push(
                            CommandManager.getAdditionalServiceInformationAsync(Service.o)
                        )
                    })
                })
                Promise.all(infoToLoad)
                    .then(function (info) {
                        var i = 0
                        _.keys(macommande.Services).forEach(function (typeService) {
                            macommande.Services[typeService].forEach(function (Service) {
                                Service.op = info[i]
                            })
                            i++
                        })

                        // rendering page
                        var html = swig.renderFile(
                            path.join(__dirname, "../../views/" + VIEWS.finalize_2_macommande_list),
                            {session: req.session, CMD: macommande, Party: Party})

                        // pdf options
                        var options = {format: 'Letter'};
                        var filename = path.join(__dirname, '../../public' + '/tmp/' + req.session.id + '.pdf')

                        // create pdf
                        pdf.create(html, options).toFile(filename, function (err, respdf) {
                            if (err) {
                                //console.log(err); // TODO some error here
                            }
                            res.download(filename, 'mrvivien_facture.pdf', function (err) {
                                if (err) {
                                    console.log(err)
                                }
                                else
                                    fs.unlink(filename, function (err) {
                                        if (err)
                                            console.log(err)
                                    })
                            })
                        });

                        // test render to browser
                        //res.render(VIEWS.finalize_2_macommande_list, {session: req.session, CMD: macommande, Party: Party});
                    })
            })
        })
    };
    app.get(ROUTES.finalize_2_macommande_list, stage2GetMaCommandeList);

    // stage4 : GET : info personal
    var stage3GetInfoPersonal = function (req, res) {
        // TODO finish
        res.render(VIEWS.finalize_3_information_personal, {session: req.session});
    };
    app.get(ROUTES.finalize_3_information_personal, stage3GetInfoPersonal);

    // stage4 : POST
    var stage3PostInfoPersonal = function (req, res) {
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            var params = req.body
            Party.createPerson({
                prenom: params.prenom,
                nom: params.nom,
                street: params.street,
                city: params.city,
                country: params.country,
                postal: params.postal,
                email: params.email,
                iam: params.iam,
                sex: params.sex,
                telephone: params.telephone,
                bday: params.day + "/" + params.month + "/" + params.year,
                callme: params.callme,
            })

            if (params.ifsameaddress == undefined) {
                Party.Event.update({
                    address: params.street2,
                    postal: params.postal2,
                    city: params.city2,
                    country: params.country2,
                    floor: params.floor2,
                    place: params.typeplace,
                    lift: params.lift,
                    hour: params.time,
                    date: params.date,
                })
            } else {
                Party.Event.update({
                    address: params.street,
                    postal: params.postal,
                    city: params.city,
                    country: params.country,
                    floor: params.floor,
                    place: params.typeplace,
                    lift: params.lift,
                    hour: params.time,
                    date: params.date,
                })
            }

            if (params.lift == '0') {
                Party.createService({
                    type: "no-lift",
                    meta: "no_lift",
                    body: true,
                    status: true
                })
            }

            res.redirect(ROUTES.finalize_4_paye_ma_commande);
        })
    };
    app.post(ROUTES.finalize_3_information_personal, stage3PostInfoPersonal);


    // stage4 : GET : paye ma commande
    var stage4GetPayeMaCommande = function (req, res) {
        res.render(VIEWS.finalize_4_paye_ma_commande)
    };
    app.get(ROUTES.finalize_4_paye_ma_commande, stage4GetPayeMaCommande);

    // stage5 : GET : verify
    var stageGetVerify = function (req, res) {
        // TODO finish
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            var Commande = new CommandeGenerator(req.models, MV, req.session.id);
            Commande.getMacommande(function (err, macommande) {

                Party.getTransactions().then(function (transactions) {
                    var ifPaid = false
                    transactions.forEach(function (Transaction) {
                        var body = Transaction.body
                        if(body.success){
                            if(parseFloat(body.transaction.amount) == parseFloat(macommande.Total)){
                                ifPaid = true
                            }
                        }
                    })

                    if(ifPaid){
                        // payement is done
                        Party.update({
                            payement: true
                        }).then(function () {
                            res.render(VIEWS.message, {session: req.session, stage: "finalizecomplete"});
                        })
                    } else {
                        // TODO
                        error.renderError(
                            [__filename].join('/'),
                            ROUTES.finalize_verify,
                            "PAYMENT NOT DONE",
                            res
                        );
                    }
                })

        })
    }
    );
}
;
app.get(ROUTES.finalize_verify, stageGetVerify);

}
;