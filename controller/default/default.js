var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var API = require('../../lib/api/engine'),
    api = new API();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();


module.exports = function(app) {

    // Homepage
    var homepage = function (req, res) {
        res.render(VIEWS.home);
    };
    app.get(ROUTES.root, homepage);

    // Validate Age
    var validateAgeGet = function (req, res) {
        res.render(VIEWS.validateAge);
    };
    app.get(ROUTES.validateAge, validateAgeGet);

    var validateAgePost = function (req, res) {
        var one_year = 1000 * 60 * 60 * 24 * 365.25;     // TODO not perfect. Gives an average value
        var birth = new Date(req.body.year, req.body.month - 1, req.body.day);
        var now = new Date();
        var difference_ms = birth.getTime() - now.getTime();
        var difference_year = Math.abs(Math.round(difference_ms / one_year));

        if (difference_year >= 18) {
            api.findOne(req.models.Session, 'sid', req.sessionID, function (err, session) {
                if (err)
                    throw err;
                req.session.ageConsent = true;
                api.update(session, {ageConsent: true}, {fields: ['ageConsent']}, function () {
                    // final redirection
                    res.redirect(ROUTES.tutorial_particulier);
                });
            });
        } else {
            req.session.ageConsent = false;
            res.redirect(ROUTES.error + "NOE");
        }
    };
    app.post(ROUTES.validateAge, validateAgePost);

    // Espace Particulier
    var espaceParticulier = function (req, res) {
        res.render(VIEWS.espace_particulier, {session: req.session});

        // check if exist party by session
        MV.getUserParty(req.models, req.session.id, function(){});
    };
    app.get(ROUTES.espace_particulier, espaceParticulier);

    // Tutorial
    var tutoParticulier = function (req, res) {
        req.session.frequent = true;
        res.render(VIEWS.tutorial_particulier);
    };
    app.get(ROUTES.tutorial_particulier, tutoParticulier);

    // Error
    var errorView = function (req, res) {
        var messages = {
            NOE: "You are not old enough!"
        };

        res.render(VIEWS.error, {
            message: messages[req.params.message],
            error: {}
        });
    };
    app.get(ROUTES.error, errorView);

    // Contact Monsieur Vivien  
    var contact = function (req, res) {
        res.render(VIEWS.contact, {session: req.session});
    };
    app.get(ROUTES.contact, contact);

    var test = function (req, res) {
        res.render(VIEWS.test)
    }
    app.get(ROUTES.test, test)
};