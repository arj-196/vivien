var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var VARIABLES = require('../../config/variables');
var Promise = require('bluebird');
var API = require('../../lib/api/engine'),
    api = new API();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();


module.exports = function (app) {

    // STAGE 1 GET : Choose Bar
    var stage1GetChooseBar = function (req, res) {
        res.render(VIEWS.sur_measure_l4_1, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l4_1, stage1GetChooseBar);

    // Stage 1 POST
    var stage1PostChooseBar = function (req, res) {
        var data = req.body;

        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            // removing re defined services
            var servicesToBeRemoved = [];
            Party.getServices().then(function (services) {
                services.forEach(function (Service) {
                    if (Service.type == "service-bar") {
                        servicesToBeRemoved.push(Party.removeService(Service))
                    }
                });
            })

            Promise.all(servicesToBeRemoved).then(function () {
                if (data.choix.toString() == 'true') {
                    var options = JSON.parse(data.options);

                    var servicesToBeSaved = [];
                    Object.keys(options).forEach(function (key) {
                        var option = options[key];

                        servicesToBeSaved.push(
                            req.models.Service.create({
                                type: "service-bar",
                                meta: key,
                                body: option,
                                status: true
                            })
                        );
                    });

                    Promise.all(servicesToBeSaved).then(function (services) {
                        var servicesToBeAdded = [];
                        services.forEach(function (Service) {
                            servicesToBeAdded.push(
                                Party.addService(Service)
                            )
                        });

                        Promise.all(servicesToBeAdded).then(function () {
                            res.redirect(ROUTES.sur_measure_l4_verify);
                        })
                    });
                } else {
                    res.redirect(ROUTES.sur_measure_l4_verify);
                }
            });
        });
    };

    app.post(ROUTES.sur_measure_l4_1, stage1PostChooseBar);

    // STAGE Verify GET
    var stageVerify = function (req, res) {
        // TODO finish
        res.render(VIEWS.message, {session: req.session, stage: "stage4complete"});
    };
    app.get(ROUTES.sur_measure_l4_verify, stageVerify);


};
