var ROUTES = require('../../config/routes.js')
var VIEWS = require('../../config/views.js')
var helper = require('../../lib/utils/common')
var API = require('../../lib/api/engine'),
    engine = new API()
var Paramparser = require('../../lib/utils/param-parser'),
    paramparser = new Paramparser();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();

module.exports = function (app) {

    // list formules
    var formuleList = function (req, res, next) {
        res.render(VIEWS.formule_list, {
            session: req.session,
        })
    }
    app.get(ROUTES.formule_list, formuleList)

    // detail formule
    var formuleDetail = function (req, res, next) {
        res.render(VIEWS.formule_detail, {
            session: req.session,
            id: req.params.ID
        })
    }
    app.get(ROUTES.formule_details, formuleDetail)

    var formuleChoose = function (req, res) {
        engine.findOne(req.models.Formule, 'id', paramparser.parse(req.params.ID), function (err, formule) {
            MV.getUserParty(req.models, req.session.id, function (err, Party) {
                Party.update({
                    ifFormule: true,
                }).then(function () {
                    Party.setFormule(formule)
                    req.models.Event.create({
                        nbPeople: formule.nbPpl
                    }).then(function (Event) {
                        Party.setEvent(Event)
                            .then(function () {
                                res.redirect(ROUTES.finalize_2_macommande)
                            })
                    })
                })
            })
        });
    }
    app.get(ROUTES.formule_choose, formuleChoose)

}