var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var VARIABLES = require('../../config/variables');
var Promise = require('bluebird');
var API = require('../../lib/api/engine'),
    api = new API();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();
var Error = require('../../lib/error/error'),
    error = new Error();


module.exports = function (app) {

    // STAGE 1 GET : Type Drinks
    var stage1GetCategories = function (req, res) {
        // check if exist bar
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            if (!Party.Bar) {
                req.models.Bar.build({}).save()
                    .then(function (Bar) {
                        Party.setBar(Bar);
                    });
            }
        });
        res.render(VIEWS.sur_measure_l2_1, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l2_1, stage1GetCategories);
    // Stage 1 POST
    var stage1PostCategories = function (req, res) {
        var params = req.body;

        if (typeof params.choix1boisson == "string") {
            params.choix1boisson = [params.choix1boisson];
        }
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            Party.Bar.update({categories: params.choix1boisson});
        });

        // saving typeDrink in session variable as tmp for further use
        api.findOne(req.models.Session, 'sid', req.session.id, function (err, Session) {
            Session.update({
                tmp: {datatype: "categories", categories: params.choix1boisson}
            });
        });
        res.redirect(ROUTES.sur_measure_l2_2);
    };

    app.post(ROUTES.sur_measure_l2_1, stage1PostCategories);


    var stage2GetChooseDrinks = function (req, res) {
        api.findOne(req.models.Session, 'sid', req.session.id, function (err, Session) {
            var data = Session.get("tmp");
            if (data.datatype != "categories") {
                res.redirect(ROUTES.sur_measure_l2_1);
            } else {
                if (typeof data.categories == "string")
                    data.categories = [data.categories];

                if (data.categories.length > 0) {
                    var categoryId = data.categories.pop();

                    api.findOne(req.models.Category, 'id', categoryId, function (err, Category) {
                        if (!Category) {
                            res.redirect(ROUTES.sur_measure_l2_1);
                        } else {
                            res.render(VIEWS.message, {
                                session: req.session,
                                stage: "choosedrinks",
                                message: {
                                    title: "NOUS ALLONS CHOISIR VOS " + Category.name.toUpperCase(),
                                    next: ROUTES.sur_measure_l2_2_a.replace(":category", categoryId)
                                }
                            });
                            Session.update({
                                tmp: data
                            });
                        }
                    });
                } else {
                    // TODO add section soft!
                    res.redirect(ROUTES.sur_measure_l2_verify);
                }
            }
        });
    };
    app.get(ROUTES.sur_measure_l2_2, stage2GetChooseDrinks);

    var stage2aGetClassiqueOrSuperieur = function (req, res) {
        res.render(VIEWS.sur_measure_l2_2_a, {session: req.session, params: req.params});
    };
    app.get(ROUTES.sur_measure_l2_2_a, stage2aGetClassiqueOrSuperieur);

    var stage2aPostClassiqueOrSuperieur = function (req, res) {
        var data = req.body;
        var params = req.params;
        res.redirect(ROUTES.sur_measure_l2_2_b.replace(":category", params.category).replace(":upgrade", data.upgrade));
    };
    app.post(ROUTES.sur_measure_l2_2_a, stage2aPostClassiqueOrSuperieur);


    var stage2bGetHowManyDrinks = function (req, res) {
        res.render(VIEWS.sur_measure_l2_2_b, {session: req.session, params: req.params});
    };
    app.get(ROUTES.sur_measure_l2_2_b, stage2bGetHowManyDrinks);

    var stage2bPostHowManyDrinks = function (req, res) {
        var data = req.body;
        var params = req.params;
        res.redirect(ROUTES.sur_measure_l2_2_c
                .replace(":category", params.category)
                .replace(":upgrade", params.upgrade)
                .replace(":nbdrinks", data.nbdrinks)
        );
    };
    app.post(ROUTES.sur_measure_l2_2_b, stage2bPostHowManyDrinks);

    var stage2cGetAlcoholBase = function (req, res) {
        res.render(VIEWS.sur_measure_l2_2_c, {session: req.session, params: req.params});
    };
    app.get(ROUTES.sur_measure_l2_2_c, stage2cGetAlcoholBase);

    var stage2cPostAlcoholBase = function (req, res) {
        var data = req.body;
        var params = req.params;

        res.redirect(ROUTES.sur_measure_l2_2_d
                .replace(":category", params.category)
                .replace(":upgrade", params.upgrade)
                .replace(":nbdrinks", params.nbdrinks)
                .replace(":alcools", JSON.stringify(data.alcool))
        );
    };
    app.post(ROUTES.sur_measure_l2_2_c, stage2cPostAlcoholBase);

    var stage2dGetChooseDrink = function (req, res) {
        var alcools = JSON.parse(req.params.alcools);
        if(typeof alcools != "string")
            alcools = alcools.join(",");

        res.render(VIEWS.sur_measure_l2_2_d, {
            session: req.session,
            params: req.params,
            alcoholsAllowed: alcools
        });
    };
    app.get(ROUTES.sur_measure_l2_2_d, stage2dGetChooseDrink);

    var stage2dPostChooseDrink = function (req, res) {
        // TODO check if drinks exist already, and remove them (if user comes to this section twice)
        var data = req.body;
        var params = req.params;

        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            var drinks = JSON.parse(data.drinks);
            drinks.forEach(function (drinkID) {
                api.findOne(req.models.Drink, 'id', drinkID, function (err, Drink) {
                    Party.Bar.addDrink(Drink, {
                        quantity: parseInt(params.nbdrinks),
                        upgrade: params.upgrade == '1',
                        category: params.category
                    });
                });
            });
        });
        //res.redirect(ROUTES.sur_measure_l2_3.replace(":category", params.category));
        res.redirect(ROUTES.sur_measure_l2_4.replace(":category", params.category));
    };
    app.post(ROUTES.sur_measure_l2_2_d, stage2dPostChooseDrink);

    var stage3dGetMoreRecipies = function (req, res) {
        res.render(VIEWS.sur_measure_l2_3, {session: req.session, params: req.params});
    };
    app.get(ROUTES.sur_measure_l2_3, stage3dGetMoreRecipies);

    var stage3dPostMoreRecipies = function (req, res) {
        var data = req.body;
        var params = req.params;

        var nbRecepies = parseInt(data.recettesupp);
        if (nbRecepies > 0) {
            // TODO add recipies
            res.redirect(ROUTES.sur_measure_l2_2_a.replace(":category", params.category));
        } else {
            res.redirect(ROUTES.sur_measure_l2_4.replace(":category", params.category));
        }
    };
    app.post(ROUTES.sur_measure_l2_3, stage3dPostMoreRecipies);

    var stage4GetIceType = function (req, res) {
        res.render(VIEWS.sur_measure_l2_4, {session: req.session, params: req.params});
    };
    app.get(ROUTES.sur_measure_l2_4, stage4GetIceType);

    var stage4PostIceType = function (req, res) {
        var data = req.body;
        var params = req.params;

        var ice = {
            type: data.optglacon,
            optional: JSON.parse(data.optional)
        };

        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            Party.Bar.getDrinks().then(function (drinks) {
                if (drinks.length > 0) {
                    var barID = drinks[0].BarDrink.BarId;

                    // find all bardrink values by barId
                    api.findAndCount(req.models.BarDrink, 'BarId', barID, function (err, r) {
                        // TODO handle error
                        if (!err) {
                            r.r.rows.forEach(function (entry) {
                                if (parseInt(entry.category) === parseInt(params.category)) { // could have problem due to integer parsing
                                    entry.update({
                                        ice: JSON.stringify(ice)
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });

        res.redirect(ROUTES.sur_measure_l2_5.replace(":category", params.category));
    };
    app.post(ROUTES.sur_measure_l2_4, stage4PostIceType);

    var stage5GetCompleterService = function (req, res) {
        res.render(VIEWS.sur_measure_l2_5, {session: req.session, params: req.params});
    };
    app.get(ROUTES.sur_measure_l2_5, stage5GetCompleterService);

    var stage5PostCompleterService = function (req, res) {
        var data = req.body;
        var params = req.params;
        var services = [], service;

        if (data.optionsNO != undefined) {
            // no service
        } else {
            // service(s) defined

            if (data.optbar != undefined) {
                // bar
                service = req.models.Service.create({
                    status: false,
                    type: "bar",
                    meta: parseInt(data.optbar),
                    body: []
                });
                services.push(service);
            }
            if (data.optfontaine != undefined) {
                // fontaine
                service = req.models.Service.create({
                    status: false,
                    type: "fontaine",
                    meta: parseInt(data.optfontaine),
                    body: []
                });
                services.push(service);
            }
            if (data.optverre != undefined) {
                // verre
                service = req.models.Service.create({
                    status: false,
                    type: "verre",
                    meta: parseInt(data.optverre),
                    body: []
                });
                services.push(service);
            }
        }

        // TODO handle error
        Promise.all(services).then(function (savedServices) {
            MV.getUserParty(req.models, req.session.id, function (err, Party) {
                var servicesToAdd = [];
                savedServices.forEach(function (savedService) {
                    servicesToAdd.push(Party.addService(savedService));
                });

                // when all services saved
                Promise.all(servicesToAdd).then(function () {
                    res.redirect(ROUTES.sur_measure_l2_6.replace(":category", params.category));
                });
            });
        });

    };
    app.post(ROUTES.sur_measure_l2_5, stage5PostCompleterService);

    var stage6HandlerServiceChooseDrink = function (req, res) {
        // TODO new service overwriting previous service
        var data = req.body;
        if (data.drinks != undefined) {
            // if post
            var drinks = JSON.parse(data.drinks);
            if (drinks.length > 0) {
                var drinkID = drinks[0];
                api.findOne(req.models.Drink, 'id', drinkID, function (err, Drink) {
                    // TODO handle error
                    if (Drink) {

                        api.findOne(req.models.Service, 'id', data.serviceId, function (err, Service) {
                            Service.update({
                                status: true,
                                body: [drinkID]
                            }).then(function () {
                                stage6RenderPage(req, res);
                            });
                        });
                    }
                });
            }
        } else {
            // if get
            stage6RenderPage(req, res);
        }
    };

    var stage6RenderPage = function (req, res) {
        var params = req.params;
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            // TODO handle error
            if (!err) {
                Party.getServices().then(function (services) {
                    var status = false, type, id, serviceId;

                    for (var i = 0; i < services.length; i++) {
                        var s = services[i];
                        if (!s.status) {
                            status = true;
                            type = s.type;
                            id = s.meta;
                            serviceId = s.id;
                            break;
                        }
                    }

                    if (status) {
                        res.render(VIEWS.sur_measure_l2_6, {
                            session: req.session,
                            params: req.params,
                            data: {
                                action: ROUTES.sur_measure_l2_6.replace(':category', params.category),
                                type: type,
                                id: id,
                                serviceId: serviceId
                            }
                        });
                    } else {
                        // no more services left
                        res.redirect(ROUTES.sur_measure_l2_2);
                    }
                });
            }
        });
    };
    app.get(ROUTES.sur_measure_l2_6, stage6HandlerServiceChooseDrink);
    app.post(ROUTES.sur_measure_l2_6, stage6HandlerServiceChooseDrink);

    var stageVerifyLevel2 = function (req, res) {
        MV.getUserParty(req.models, req.session.id, function (err, Party) {

            // stage 1 : if barid not null
            if (Party.Bar != null) {
                // stage 2 : if drinks for all categories were choosen
                var seenCategories = [];
                var categories = Party.Bar.categories;
                Party.Bar.getDrinks().then(function (drinks) {
                    drinks.forEach(function (drink) {
                        if (categories.indexOf("" + drink.BarDrink.category) != -1) {
                            if (seenCategories.indexOf(drink.BarDrink.category) == -1)
                                seenCategories.push(drink.BarDrink.category);
                        }
                    });
                    // all good
                    if (seenCategories.length == categories.length) {

                        // stage 5 : services
                        Party.getServices().then(function (services) {
                            var status = true;
                            services.forEach(function (service) {
                                if (!service.status)
                                    status = false;
                            });

                            if (status) {
                                res.render(VIEWS.message, {session: req.session, stage: "stage2complete"});
                            } else {
                                // TODO some service not completed
                                error.renderError(
                                    [__filename].join('/'),
                                    ROUTES.sur_measure_l2_verify,
                                    "all service.status not true",
                                    res
                                );
                            }
                        });
                    } else {
                        seenCategories.forEach(function (seenCategory) {
                            var index = categories.indexOf(seenCategory.toString());
                            if (index != -1) {
                                categories.splice(index, 1);
                            }
                        });

                        if (categories.length > 0) {
                            api.findOne(req.models.Session, 'sid', req.session.id, function (err, Session) {
                                Session.update({
                                    tmp: {datatype: "categories", categories: categories}
                                }).then(function () {
                                    res.redirect(ROUTES.sur_measure_l2_2);
                                });
                            });
                        } else {
                            error.renderError(
                                [__filename].join('/'),
                                ROUTES.sur_measure_l2_verify,
                                "Error calculating unseen categories",
                                res
                            );

                        }

                    }
                });
            }
        });

    };
    app.get(ROUTES.sur_measure_l2_verify, stageVerifyLevel2);


};
