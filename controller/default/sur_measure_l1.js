var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var VARIABLES = require('../../config/variables');
var API = require('../../lib/api/engine'),
    api = new API();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();


module.exports = function (app) {

    // STAGE 1 GET : Number people
    var stage1GetNumberPeople = function (req, res) {
        // check if exist event
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            if (!Party.dataValues.Event) {
                // if not event defined create event
                var newEvent = req.models.Event.build();
                newEvent.save().then(function (event) {
                    Party.setEvent(event);
                });
            }
            Party.update({ifFormule: false});
        });
        res.render(VIEWS.sur_measure_l1_1, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l1_1, stage1GetNumberPeople);

    // Stage 1 POST
    var stage1PostNumberPeople = function (req, res) {
        var nbppl = req.body.nbconviveRadioOptions;
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            Party.Event.update({
                nbPeople: nbppl
            });
        });
        res.redirect(ROUTES.sur_measure_l1_2);
    };
    app.post(ROUTES.sur_measure_l1_1, stage1PostNumberPeople);

    // STAGE 2 GET : Renseignements Event
    var stage2GetRenseignementEvent = function (req, res) {
        res.render(VIEWS.sur_measure_l1_2, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l1_2, stage2GetRenseignementEvent);

    // STAGE 2 POST
    var stage2PostRenseignementEvent = function (req, res) {
        // TODO check parameters before using paramparser
        var param = req.body;
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            Party.Event.update({
                hour: param.hour + ":" + param.minute,
                address: param.address,
                city: param.city,
                postal: param.postcode,
                duration: param.duration,
                type: param.type_event
            });

            Party.update({
                date: new Date(param.year, param.month, param.day, (param.hour + 1), param.minute)
            });
        });

        //res.redirect(ROUTES.sur_measure_l1_3);  // TODO need algorithm for formule recommendation
        res.redirect(ROUTES.sur_measure_l1_4)

    };
    app.post(ROUTES.sur_measure_l1_2, stage2PostRenseignementEvent);

    // STAGE 3 GET : Propose Formule
    var stage3GetProposeFormule = function (req, res) {
        // TODO finish
        res.render(VIEWS.sur_measure_l1_3, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l1_3, stage3GetProposeFormule);

    // STAGE 4 GET : Theme
    var stage4GetTheme
        = function (req, res) {
        res.render(VIEWS.sur_measure_l1_4, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l1_4, stage4GetTheme);

    // STAGE 4 POST
    var stage4PostTheme = function (req, res) {
        var param = req.body;
        if (param.choix.toString() === '3') {
            // no theme
            MV.getUserParty(req.models, req.session.id, function (err, Party) {
                var theme = req.models.Theme.build({
                    name: "No Theme",
                    type: 3
                });
                theme.save().then(function (theme) {
                    Party.Event.setTheme(theme);
                    Party.Event.update({ifOrganised: null});
                });
            })
        } else {
            api.findOne(req.models.Theme, 'id', param.choixtheme, function (err, childTheme) {
                if (!err) {
                    MV.getUserParty(req.models, req.session.id, function (err, Party) {
                        var theme, ifOrganised;
                        if (param.choix.toString() === '2') {
                            // yes theme, no organized
                            theme = req.models.Theme.build({
                                type: 2
                            });
                            ifOrganised = false;
                        } else {
                            // yes theme, yes organized
                            theme = req.models.Theme.build({
                                type: 1
                            });
                            ifOrganised = true;
                        }
                        theme.save().then(function (theme) {
                            theme.setTheme(childTheme);
                            Party.Event.setTheme(theme);
                            Party.Event.update({ifOrganised: ifOrganised});
                        });
                    })

                } else {
                    res.redirect(ROUTES.sur_measure_l1_4);
                }
            });
        }
        res.redirect(ROUTES.sur_measure_l1_5);
    };
    app.post(ROUTES.sur_measure_l1_4, stage4PostTheme);

    // STAGE 5 GET : ifBio
    var stage5GetIfBio = function (req, res) {
        res.render(VIEWS.sur_measure_l1_5, {session: req.session});
    };
    app.get(ROUTES.sur_measure_l1_5, stage5GetIfBio);

    // STAGE 5 POST
    var stage5PostIfBio = function (req, res) {
        var param = req.body;
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            var bio = false;
            if (param.choix.toString() === '1')
                bio = true;
            Party.Event.update({ifBio: bio});
        });
        res.redirect(ROUTES.sur_measure_l1_6);
    };
    app.post(ROUTES.sur_measure_l1_5, stage5PostIfBio);

    // STAGE 6 GET : Complete LEVEL 1
    var stage6GetCompleteLevel1 = function (req, res) {
        MV.getUserParty(req.models, req.session.id, function (err, Party) {
            if (Party.Event.nbPeople != null) // stage 1
                if (Party.Event.hour != null && Party.Event.city != null && Party.Event.postal != null &&
                    Party.Event.duration != null && Party.Event.type != null) // stage 2
                    if (Party.Event.ThemeId != null) // stage 4
                        if (Party.Event.ifBio != null) {
                            MV.getUserParty(req.models, req.session.id, function (err, Party) {
                                Party.update({
                                    stage1: true
                                });
                            });
                            res.render(VIEWS.message, {session: req.session, stage: "stage1complete"});
                        }
                        else
                            res.redirect(ROUTES.sur_measure_l1_5);
                    else
                        res.redirect(ROUTES.sur_measure_l1_4);
                else
                    res.redirect(ROUTES.sur_measure_l1_2);
            else
                res.redirect(ROUTES.sur_measure_l1_1);
        });
    };
    app.get(ROUTES.sur_measure_l1_6, stage6GetCompleteLevel1);

};