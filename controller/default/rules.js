var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var API = require('../../lib/api/engine'),
    api = new API();

module.exports = function(app) {

    // TODO rules not working properly


    // ageConsent
    // TODO add exception for routes starting with /_api
    app.all(ROUTES.prefix.espace, function (req, res, next) {
        if (req.session.ageConsent == undefined) {
            api.findOne(req.models.Session, 'sid', req.sessionID, function (err, session) {
                if (err)
                    throw err;
                if (helper.ifTrue(session, 'ageConsent')) {
                    req.session.ageConsent = true;
                    next();
                } else {
                    req.session.ageConsent = false;
                    res.redirect(ROUTES.validateAge);
                }
            });
        } else {
            if (req.session.ageConsent) {
                next();
            } else {
                if (req.originalUrl == ROUTES.validateAge)
                    next();
                else
                    res.redirect(ROUTES.validateAge);

            }
        }
    });


    // Tutorial Particulier
    app.all(ROUTES.prefix.espace_particulier, function (req, res, next) {
        if (req.session.frequent == undefined) {
            api.findOne(req.models.Session, 'sid', req.sessionID, function (err, session) {
                if (err)
                    throw err;
                if (helper.ifTrue(session, 'frequent')) {
                    req.session.frequent = true;
                    next();
                } else {
                    req.session.frequent = false;
                    res.redirect(ROUTES.tutorial_particulier);
                }
            });
        } else {
            if (req.session.frequent) {
                next();
            } else {
                if (req.originalUrl == ROUTES.tutorial_particulier)
                    next();
                else
                    res.redirect(ROUTES.tutorial_particulier);
            }
        }
    });

};

