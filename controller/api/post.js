var express = require('express');
var router = express.Router();
var VARIABLES = require('../../config/variables');
var Error = require('../../lib/error/error'),
    error = new Error();
var API = require('../../lib/api/engine'),
    engine = new API();
var Paramparser = require('../../lib/utils/param-parser'),
    paramparser = new Paramparser();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();
var _CommandManager = require('../../lib/mrvivien/commandemanager');
var AdminManager = require('../../lib/admin/adminmanager')
var _ = require('underscore')
// ---
// Core

router.post('/core/:model/', function (req, res) {
    var clause = req.body;
    var models = req.models;
    if (models[req.params.model] === undefined)
        res.send(error.APIError(req.path, "model doesn't exist"));
    else {
        engine.findAllCustom(models, models[req.params.model], clause, function (err, r) {
            if (r.s) {
                res.json(r.r);
            } else {
                res.send(error.APIError(req.path, "Internal Error"));
            }
        });
    }
});

router.post('/v/drink/by/category/and/alcool', function (req, res) {
    var params = req.body;
    var alcoholsAllowed;
    if (!params.alcoholsAllowed)
        alcoholsAllowed = null;
    else
        alcoholsAllowed = params.alcoholsAllowed.split(',');

    MV.getDrinksByCategoryAndAlcohol(req.models, parseInt(params.category), alcoholsAllowed, function (err, drinks) {
        res.json(drinks);
    })
});

router.post('/commande/remove/', function (req, res) {
    var CommandManager = new _CommandManager(req.models, MV, req.session.id);
    CommandManager.removeItem(req.body, function (r) {
        res.json(r)
    })
})

router.post('/service/additional/information', function (req, res) {
    var CommandManager = new _CommandManager(req.models, MV, req.session.id);
    CommandManager.getAdditionalServiceInformation(JSON.parse(req.body.data), function (r) {
        res.json(r)
    })
})

// ADMIN
router.post('/edit/drink/', function (req, res) {
    // TODO img property
    var data = {
        alcoholList: _.map(
            _.isArray(req.body['alcohols[]']) ? req.body['alcohols[]'] : [req.body['alcohols[]']],
            function (d) {
                return parseFloat(d)
            })
        ,
        categoryList: _.map(
            _.isArray(req.body['categories[]']) ? req.body['categories[]'] : [req.body['categories[]']],
            function (d) {
                return parseFloat(d)
            })
        ,
        price: parseFloat(req.body.price),
        name: req.body.name,
        description: req.body.description,
        saveur: req.body.saveur,
        id: parseInt(req.body.drinkid)
    }

    if (req.body.action == "edit") {
        AdminManager.editDrink(req.models, data, function (err, r) {
            res.json([err, r])
        })
    } else if (req.body.action == "new") {
        AdminManager.createDrink(req.models, data, function (err, r) {
            res.json([err, r])
        })
    } else if (req.body.action == "delete") {
        AdminManager.deleteDrink(req.models, data, function (err, r) {
            res.json([err, r])
        })
    }
})

router.post('/edit/alcohol/', function (req, res) {
    // TODO img property
    var data = {
        price: parseFloat(req.body.price),
        name: req.body.name,
        type: req.body.type,
        brand: req.body.brand,
        id: parseInt(req.body.alcoholid)
    }

    if (req.body.action == "edit") {
        AdminManager.editAlcohol(req.models, data, function (err, r) {
            res.json([err, r])
        })
    } else if (req.body.action == "new") {
        AdminManager.createAlcohol(req.models, data, function (err, r) {
            res.json([err, r])
        })
    } else if (req.body.action == "delete") {
        AdminManager.deleteAlcohol(req.models, data, function (err, r) {
            res.json([err, r])
        })
    }
})

router.post('/edit/category/', function (req, res) {
    // TODO img property
    var data = {
        price: parseFloat(req.body.price),
        name: req.body.name,
        desc1: req.body.desc1,
        desc2: req.body.desc2,
        drinks: _.map(
            _.isArray(req.body['drinks[]']) ? req.body['drinks[]'] : [req.body['drinks[]']],
            function (d) {
                return parseFloat(d)
            }),
        id: parseInt(req.body.categoryid)
    }

    if (req.body.action == "edit") {
        AdminManager.editCategory(req.models, data, function (err, r) {
            res.json([err, r])
        })
    } else if (req.body.action == "new") {
        AdminManager.createCategory(req.models, data, function (err, r) {
            res.json([err, r])
        })
    } else if (req.body.action == "delete") {
        AdminManager.deleteCategory(req.models, data, function (err, r) {
            res.json([err, r])
        })
    }
})

router.post('/edit/formule/', function (req, res) {
    // TODO img property
    var data = {
        name: req.body.name,
        nbppl: parseInt(req.body.nbppl),
        itemname: req.body.itemname,
        itemdesc: req.body.itemdesc,
        itemprice: parseFloat(req.body.itemprice),
        id: parseInt(req.body.formuleid)
    }

    if (req.body.action == "edit") {
        AdminManager.editFormule(req.models, data, function (err, r) {
            res.json([err, r])
        })
    } else if (req.body.action == "new") {
        AdminManager.createFormule(req.models, data, function (err, r) {
            res.json(r)
        })
    } else if (req.body.action == "delete") {
        AdminManager.deleteFormule(req.models, data, function (err, r) {
            res.json([err, r])
        })
    } else if (req.body.action == "newitem") {
        AdminManager.createFormuleItem(req.models, data, function (err, r) {
            res.json(r)
        })
    } else if (req.body.action == "deleteitem") {
        data = {
            formuleid: parseInt(req.body.formuleid),
            itemid: parseInt(req.body.itemid),
        }
        AdminManager.deleteFormuleItem(req.models, data, function (err, r) {
            res.json([err, r])
        })
    }
})


module.exports = router;
