var express = require('express');
var router = express.Router();
var VARIABLES = require('../../config/variables');
var Error = require('../../lib/error/error'),
    error = new Error();
var API = require('../../lib/api/engine'),
    engine = new API();
var Paramparser = require('../../lib/utils/param-parser'),
    paramparser = new Paramparser();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();
var CommandeGenerator = require('../../lib/mrvivien/commandegenerator');
var _ = require('underscore')
var Promise = require('bluebird')
// ---
// Core

router.get('/core/:model/all', function (req, res) {
    var models = req.models;

    if (models[req.params.model] === undefined)
        res.send(error.APIError(req.path, "model doesn't exist"));
    else {
        engine.findAll(models[req.params.model], function (err, r) {
            res.json(r);
        });
    }
});

router.get('/core/:model/:id', function (req, res) {
    var models = req.models;

    if (models[req.params.model] === undefined)
        res.send(error.APIError(req.path, "model doesn't exist"));
    else {
        engine.findAndCount(models[req.params.model], 'id', paramparser.parse(req.params.id), function (err, r) {
            res.json(r);
        });
    }
});

router.get('/core/expand/:model/:id', function (req, res) {
    var models = req.models;

    if (models[req.params.model] === undefined)
        res.send(error.APIError(req.path, "model doesn't exist"));
    else {
        engine.findAndExpand(
            models, models[req.params.model], 'id', paramparser.parse(req.params.id), function (err, r) {
                res.json(r);
            });
    }
});

router.get('/core/byField/:model/:field/:param', function (req, res) {
    var models = req.models;
    if (models[req.params.model] === undefined)
        res.send(error.APIError(req.path, "model doesn't exist"));
    else {
        engine.find(models[req.params.model], req.params.field, paramparser.parse(req.params.param), function (err, r) {
            res.json(r);
        })
    }
});

router.get('/variable/:name', function (req, res) {
    res.json(VARIABLES[req.params.name]);
});

// ---
// User Oriented

router.get('/user/party', function (req, res) {
    MV.getUserParty(req.models, req.session.id, function (err, party) {
        res.json(party);
    })
});

router.get('/user/macommande/price', function (req, res) {
    var Commande = new CommandeGenerator(req.models, MV, req.session.id);
    Commande.getMacommande(function (err, macommande) {
        res.json(macommande.Total);
    })
});

router.get('/user/macommande/details', function (req, res) {
    var Commande = new CommandeGenerator(req.models, MV, req.session.id);
    Commande.getMacommande(function (err, macommande) {
        res.json(macommande);
    })
});


// ---
// Custom

router.get('/v/alcohol/by/category/:category', function (req, res) {
    MV.getAlcoholByCategory(req.models, req.params.category, function (err, Alcohols) {
        res.json(Alcohols);
    })
});

router.get('/formule/all', function (req, res) {
    req.models.Formule.findAll()
        .then(function (formules) {
            var itemsToBeLoaded = []
            formules.forEach(function (formule) {
                itemsToBeLoaded.push(formule.getItems({include: [req.models.Price]}))
            })
            Promise.all(itemsToBeLoaded)
                .then(function (itemList) {
                    for (var i = 0; i < formules.length; i++) {
                        var items = itemList[i]
                        var totalPrice = 0
                        items.forEach(function (item) {
                            if (!_.isUndefined(item.Price))
                                totalPrice += item.Price.price
                        })
                        formules[i].dataValues.price = totalPrice
                    }
                    res.json(formules)
                })
        })
})


router.get('/formule/id/:id', function (req, res) {
    engine.findOne(req.models.Formule, 'id', paramparser.parse(req.params.id), function (err, formule) {
        formule.getItems({include: [req.models.Price]})
            .then(function (items) {
                var totalPrice = 0
                items.forEach(function (item) {
                    if (!_.isUndefined(item.Price))
                        totalPrice += item.Price.price
                })
                res.json({
                    formule: formule,
                    items: items,
                    totalprice: totalPrice
                });

            })
    });
})

// ADMIN

router.get('/core/:model/all/:iteration', function (req, res) {
    var models = req.models;

    if (models[req.params.model] === undefined)
        res.send(error.APIError(req.path, "model doesn't exist"));
    else {
        engine.findByIteration(models, req.params.iteration, req.params.model, function (err, r) {
            res.json(r);
        });
    }
});

router.get('/v/alcoholandcategories/by/drink/:drinkid', function (req, res) {
    MV.getAlcoholAndCategoryForDrink(req.models, req.params.drinkid, function (err, Alcohols) {
        res.json(Alcohols);
    })
});

router.get('/v/drinks/by/category/:categoryid', function (req, res) {
    MV.getDrinksByCategory(req.models, req.params.categoryid, function (err, Drinks) {
        res.json(Drinks);
    })
});
router.get('/v/formuleitems/:formuleid', function (req, res) {
    MV.getItemsForFormule(req.models, req.params.formuleid, function (err, Items) {
        res.json(Items);
    })
});

router.get('/v/aparty/:partyid', function (req, res) {
    req.models.Party.findOne({
            where: {
                id: parseInt(req.params.partyid)
            },
            include: [
                req.models.Event, req.models.Person
            ]
        })
        .then(function (r) {
            res.json(r);
        });
});

router.get('/v/partyinfo/:partyid', function (req, res) {
    req.models.Party.findOne({
            where: {id: parseInt(req.params.partyid)},
        })
        .then(function (Party) {
            // get commande
            var Commande = new CommandeGenerator(req.models, MV, Party.sid);
            Commande.getMacommande(function (err, macommande) {
                // get Theme
                req.models.Theme.findOne({where: {id: macommande.Theme.o.id}})
                    .then(function (Theme) {
                        res.json({cmd: macommande, theme: Theme})
                    })
            })
        })
})

module.exports = router;
