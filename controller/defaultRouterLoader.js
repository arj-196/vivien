var path = require('path');
var fs = require('fs');

module.exports = function(app) {

    fs
        .readdirSync(path.join(__dirname, '/default'))
        .filter(function(file) {
            return (file.indexOf('.') !== 0);
        })
        .forEach(function(file) {
            if (file.slice(-3) !== '.js') return;
            var filename = file.slice(0, file.length - 3);
            require(path.join(__dirname, 'default/', filename))(app);
        });
};
