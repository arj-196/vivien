var path = require('path');
var fs = require('fs');

module.exports = function (app) {

    /* GET home page. */
    app.get('/_api', function(req, res, next) {
        res.send(["Welcome to the Mr.vivien API"]);
    });

    // loading all routes from folder engine
    fs
        .readdirSync(path.join(__dirname, '/api'))
        .filter(function(file) {
            return (file.indexOf('.') !== 0);
        })
        .forEach(function(file) {
            if (file.slice(-3) !== '.js') return;
            var filename = file.slice(0, file.length - 3);

            app.use('/_api/' + filename, require(path.join(__dirname, 'api/', file)));
        });
};
