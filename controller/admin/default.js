var ROUTES = require('../../config/routes.js');
var VIEWS = require('../../config/views.js');
var helper = require('../../lib/utils/common');
var API = require('../../lib/api/engine'),
    api = new API();
var MrVivien = require('../../lib/mrvivien/mrvivien'),
    MV = new MrVivien();
var _AdminManager = require('../../lib/admin/adminmanager')

module.exports = function (app) {

    // dashboard
    var homepage = function (req, res) {

        res.render(VIEWS.admin_landing)
    }
    app.get(ROUTES.admin_landing, homepage)

    // categories
    var categories = function (req, res) {

        res.render(VIEWS.admin_categories)
    }
    app.get(ROUTES.admin_categories, categories)

    // alcohols
    var alcohols = function (req, res) {

        res.render(VIEWS.admin_alcohols)
    }
    app.get(ROUTES.admin_alcohols, alcohols)

    // drinks
    var drinks = function (req, res) {

        res.render(VIEWS.admin_drinks)
    }
    app.get(ROUTES.admin_drinks, drinks)

    // formules
    var formules = function (req, res) {

        res.render(VIEWS.admin_formules)
    }
    app.get(ROUTES.admin_formules, formules)

    // Parties
    var parties = function (req, res) {

        res.render(VIEWS.admin_parties)
    }
    app.get(ROUTES.admin_parties, parties)



}