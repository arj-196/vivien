'use strict';
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Drink', {
        name: DataTypes.STRING,
        type: DataTypes.STRING,
        description: DataTypes.TEXT,
        img: {
            type: DataTypes.STRING,
            defaultValue: "824bdca281e3e16a6551e245783af755.jpg"
        },
        saveur: DataTypes.STRING,
        list: DataTypes.INTEGER
    }, {
        indexes: [
            {
                fields: ["name"]
            }
        ],
        classMethods: {
            associate: function(models) {
                models.Drink.belongsTo(models.Price);
            }
        }
    });
};
