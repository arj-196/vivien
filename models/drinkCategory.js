'use strict';
module.exports = function (sequelize, DataTypes) {

    return sequelize.define('DrinkCategory', {
    }, {
        classMethods: {
            associate: function (models) {

                models.Drink.belongsToMany(models.Category, {
                    through: models.DrinkCategory
                });

                models.Category.belongsToMany(models.Drink, {
                    through: models.DrinkCategory
                });
            }
        }
    });
};