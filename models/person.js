'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Person', {
        prenom: DataTypes.STRING,
        nom: DataTypes.STRING,
        street: DataTypes.STRING,
        city: DataTypes.STRING,
        country: DataTypes.STRING,
        postal: DataTypes.STRING,
        email: DataTypes.STRING,
        iam: DataTypes.STRING,
        sex: DataTypes.STRING,
        telephone: DataTypes.STRING,
        bday: DataTypes.STRING,
        callme: DataTypes.STRING,
    }, {
        classMethods: {
            associate: function(models) {
            }
        }
    });

};