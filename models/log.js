'use strict';
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Log', {
        source: DataTypes.STRING,
        entry: DataTypes.TEXT
    }, {
        classMethods: {
            associate: function(models) {
            }
        }
    });
};