'use strict';
module.exports = function (sequelize, DataTypes) {

    return sequelize.define('PartyService', {

    }, {
        classMethods: {
            associate: function (models) {

                models.Party.belongsToMany(models.Service, {
                    through: models.PartyService
                });

                models.Service.belongsToMany(models.Party, {
                    through: models.PartyService
                });
            }
        }
    });
};