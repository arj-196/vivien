'use strict';
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Event', {
        nbPeople: DataTypes.INTEGER,
        hour: DataTypes.STRING,
        date: DataTypes.STRING,
        duration: DataTypes.STRING,
        postal: DataTypes.STRING,
        city: DataTypes.STRING,
        country: DataTypes.STRING,
        address: DataTypes.STRING,
        ifBio: DataTypes.BOOLEAN,
        ifOrganised: DataTypes.BOOLEAN,
        type: DataTypes.STRING,
        place: DataTypes.STRING,
        floor: DataTypes.STRING,
        lift: DataTypes.STRING,
        nbBarmen: DataTypes.INTEGER,
        nbCommis: DataTypes.INTEGER,
    }, {
        classMethods: {
            associate: function(models) {
                models.Event.belongsTo(models.Theme);
            }
        }
    });
};