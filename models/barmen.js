'use strict';
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Barmen', {
        barmanId: DataTypes.STRING,
        firstname: DataTypes.STRING,
        lastname: DataTypes.STRING,
        sex: DataTypes.CHAR,
        age: DataTypes.INTEGER
    }, {
        indexes: [
            {
                fields: ["barmanId"]
            }
        ],
        classMethods: {
            associate: function(models) {
                models.Barmen.belongsToMany(models.Party, {
                    through: models.PartyBarmen
                });
            }
        }
    });
};