'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Alcohol', {
        name: DataTypes.STRING,
        img: {
            type: DataTypes.STRING,
            defaultValue: "/content/img/alcooldebase/alcool.png"
        },
        type: DataTypes.STRING,
        brand: DataTypes.STRING
    }, {
        indexes: [
            {
                fields: ["name"]
            }
        ],
        classMethods: {
            associate: function(models) {
                models.Alcohol.belongsTo(models.Price);
            }
        }
    });

};