'use strict';
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Theme', {
        name: DataTypes.STRING,
        list: DataTypes.INTEGER,
        description: DataTypes.TEXT
    }, {
        indexes: [
            {
                fields: ["name"]
            }
        ],
        classMethods: {
            associate: function(models) {
                models.Theme.belongsTo(models.Price);
                models.Theme.belongsTo(models.Theme);
            }
        }
    });
};