'use strict';
module.exports = function (sequelize, DataTypes) {

    return sequelize.define('Transaction', {
        body: {
            type: DataTypes.TEXT,
            get: function () {
                return JSON.parse(this.getDataValue('body'));
            },
            set: function (obj) {
                this.setDataValue('body', JSON.stringify(obj))
            }
        },
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

};