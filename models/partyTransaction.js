'use strict';
module.exports = function (sequelize, DataTypes) {

    return sequelize.define('PartyTransaction', {
    }, {
        classMethods: {
            associate: function (models) {

                models.Party.belongsToMany(models.Transaction, {
                    through: models.PartyTransaction
                });

                models.Transaction.belongsToMany(models.Party, {
                    through: models.PartyTransaction
                });
            }
        }
    });
};