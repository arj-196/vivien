'use strict';
module.exports = function (sequelize, DataTypes) {

    return sequelize.define('Party', {
        ifFormule: DataTypes.BOOLEAN,
        date: DataTypes.DATE,
        barmanPref: DataTypes.STRING,
        rajoutePersonnel: DataTypes.STRING,
        stage1: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        stage2: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        stage3: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        done: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        payement: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        classMethods: {
            associate: function (models) {
                models.Party.belongsTo(models.Session, {
                    foreignKey: {
                        name: 'sid'
                    }
                });
                models.Party.belongsTo(models.Formule);
                models.Party.belongsTo(models.Event);
                models.Party.belongsTo(models.Bar);
                models.Party.belongsToMany(models.Barmen, {
                    through: models.PartyBarmen
                });
                models.Party.belongsTo(models.Person)
            }
        }
    });
};