'use strict';
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Formule', {
        name: DataTypes.STRING,
        nbPpl: DataTypes.INTEGER,
        type: DataTypes.INTEGER,
        start: {
            type: DataTypes.DATE,
            defaultValue: null
        },
        end: {
            type: DataTypes.DATE,
            defaultValue: null
        },
    }, {
        indexes: [
            {
                fields: ["name"]
            }
        ],
        classMethods: {
            associate: function(models) {
            }
        }
    });
};