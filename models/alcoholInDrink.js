'use strict';

module.exports = function(sequelize, DataTypes) {

    return sequelize.define('AlcoholInDrink', {}, {

        classMethods: {
            associate: function (models) {

                models.Drink.belongsToMany(models.Alcohol, {
                    through: models.AlcoholInDrink
                });

                models.Alcohol.belongsToMany(models.Drink, {
                    through: models.AlcoholInDrink
                });

            }
        }
    });
};