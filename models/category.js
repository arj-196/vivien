'use strict';
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Category', {
        name: DataTypes.STRING,
        description: DataTypes.TEXT,
        descriptionLarge: DataTypes.TEXT,
        list: DataTypes.INTEGER
    }, {
        indexes: [
            {
                fields: ["name"]
            }
        ],
        classMethods: {
            associate: function(models) {
                models.Category.belongsTo(models.Drink);
                models.Category.belongsTo(models.Price);
            }
        }
    });
};