'use strict';
module.exports = function (sequelize, DataTypes) {

    return sequelize.define('Item', {
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        options: {
            type: DataTypes.STRING,
            get: function () {
                return JSON.parse(this.getDataValue('options'));
            },
            set: function (obj) {
                this.setDataValue('options', JSON.stringify(obj))
            }
        },

    }, {
        classMethods: {
            associate: function (models) {
                models.Item.belongsTo(models.Price);
            }
        }
    });

};