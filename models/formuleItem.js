'use strict';
module.exports = function (sequelize, DataTypes) {

    return sequelize.define('FormuleItem', {

    }, {
        classMethods: {
            associate: function (models) {

                models.Formule.belongsToMany(models.Item, {
                    through: models.FormuleItem
                });

                models.Item.belongsToMany(models.Formule, {
                    through: models.FormuleItem
                });
            }
        }
    });
};