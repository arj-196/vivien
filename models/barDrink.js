'use strict';
module.exports = function (sequelize, DataTypes) {

    return sequelize.define('BarDrink', {
        quantity: DataTypes.INTEGER,
        upgrade: DataTypes.BOOLEAN,
        category: DataTypes.INTEGER,
        ice: DataTypes.STRING

    }, {
        classMethods: {
            associate: function (models) {

                models.Drink.belongsToMany(models.Bar, {
                    through: models.BarDrink
                });

                models.Bar.belongsToMany(models.Drink, {
                    through: models.BarDrink
                });
            }
        }
    });
};