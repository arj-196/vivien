'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Session', {
        sid: {
            type: DataTypes.STRING,
            primaryKey: true
        }
        , expires: {
            type: DataTypes.DATE,
            allowNull: true
        }
        , data: DataTypes.TEXT
        , ageConsent: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
        , frequent: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
        , tmp: {
            type: DataTypes.STRING,
            get: function () {
                return JSON.parse(this.getDataValue('tmp'));
            },
            set: function (array) {
                this.setDataValue('tmp', JSON.stringify(array))
            }
        }
    }, {
        indexes: [
            {
                fields: ["sid"]
            }
        ],
        classMethods: {
            associate: function (models) {
            }
        }
    });

};