'use strict';
module.exports = function (sequelize, DataTypes) {

    return sequelize.define('Service', {
        type: {
            type: DataTypes.STRING,
            get: function () {
                return JSON.parse(this.getDataValue('type'));
            },
            set: function (obj) {
                this.setDataValue('type', JSON.stringify(obj))
            }
        },
        meta: {
            type: DataTypes.STRING,
            get: function () {
                return JSON.parse(this.getDataValue('meta'));
            },
            set: function (obj) {
                this.setDataValue('meta', JSON.stringify(obj))
            }
        },
        body: {
            type: DataTypes.TEXT,
            get: function () {
                return JSON.parse(this.getDataValue('body'));
            },
            set: function (obj) {
                this.setDataValue('body', JSON.stringify(obj))
            }
        },
        status: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        classMethods: {
            associate: function (models) {
                models.Service.belongsTo(models.Price);
            }
        }
    });

};