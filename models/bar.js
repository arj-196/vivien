'use strict';
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Bar', {
        description: DataTypes.STRING,
        categories: {
            type: DataTypes.STRING,
            get: function(){
                return JSON.parse(this.getDataValue('categories'));
            },
            set: function(array){
                this.setDataValue('categories', JSON.stringify(array))
            }
        }
    }, {
        classMethods: {
            associate: function(models) {
            }
        }
    });

};