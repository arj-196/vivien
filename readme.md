# mr.vivien


# SEQUENCE  


# REST API 

ENDPOINTS :

    
- /get/:model/all
    
        Fetching all of a model 
    
- /get/:model/:id
    
        Fetching a model by id 

- /get/expand/:model/:id

        Fetching a model by id and its associations
